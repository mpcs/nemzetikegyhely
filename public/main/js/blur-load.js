window.onload = function loadStuff() {
  var win, doc, enhancedClass, blurredElements;

  win = window;
  doc = win.document;
  enhancedClass = 'un-blur';

  blurredElements = doc.querySelectorAll('.blur');
  // Quit early if older browser (e.g. IE 8).
  if (!('addEventListener' in window)) {
    if (blurredElements.length > 0) {
      for (var i = 0; i < blurredElements.length; i++) {
        var bigUrl = blurredElements[i].getAttribute('data-big-url');
        if (typeof bigUrl === "undefined") continue;
        blurredElements[i].setAttribute('src', bigUrl);
        blurredElements[i].setAttribute('class', '');
      }
    }
    return;
  }

  function loadImageRemoveBlur(element, bigSrc) {
    var img = new Image();
    img.onload = function () {
      element.src = bigSrc;
      element.style.visibility = 'visible';
      element.className += ' ' + enhancedClass;
    };
    if (bigSrc) img.src = bigSrc;
  }

  for (var j = 0; j < blurredElements.length; j++) {
    var element = blurredElements[j];
    var bigSrc = element.getAttribute('data-big-url');
    loadImageRemoveBlur(element, bigSrc);
  }
};