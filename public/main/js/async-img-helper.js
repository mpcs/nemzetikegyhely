var afterAllDone = [];

function asyncLoadImg(url, intoElement) {
  var img = new Image();
  img.onload = function () {
    intoElement.src = url;
    intoElement.classList.add('loaded');
  };
  img.src = url;
}

function jQueryAsnycLoadImg(url, intoElement) {
  var img = new Image();
  img.onload = function () {
    intoElement.attr('src', url);
    intoElement.addClass('loaded');
  };
  img.src = url;
}

function doBigLoad() {
  console.log('Loading full quality ones');
  $(afterAllDone).each(function () {
    jQueryAsnycLoadImg(this.url, this.element);
  });
}

function JQueryAsnycLoadAllInContainer(container) {
  var all = $('img', container);
  var count = all.length;

  all.each(function () {
    var parsedSelf = $(this);
    var smallUrl = parsedSelf.attr('data-small-src');
    var originalUrl = parsedSelf.attr('data-big-src');
    parsedSelf.addClass('loading');
    afterAllDone.push({ url: originalUrl, element: parsedSelf });
    parsedSelf.on('load', function () {
      count--;
      if (count !== 0) return;
      doBigLoad();
    });
    parsedSelf.attr('src', smallUrl);
  });
}

var imageLoadHandler = {
  everyImage: null,
  parsedWindow: $(window),
  _windowHeight: null,
  windowHeight: function () {
    this._windowHeight = this._windowHeight || this.parsedWindow.height();
    return this._windowHeight;
  },
  scrollHander: function () {
    if (!this.everyImage) alert('No images supplied!');
    var self = this;
    var bottomPosition = self.parsedWindow.scrollTop() + self.windowHeight();

    this.everyImage.each(function () {
      var element = $(this);
      var src = element.attr('src');
      if (bottomPosition < element.offset().top || src) return;
      self.loadOnDisplay(element);
    });
  },
  loadOnDisplay: function (element) {
    var self = this;
    var bigUrl = element.attr('data-big-src');
    var smallUrl = element.attr('data-small-src');

    if (!smallUrl || smallUrl === '') return;
    element.attr('data-small-src', '');
    element.addClass('preloading');
    var newImage = new Image();

    newImage.onload = function () {
      element.attr('src', smallUrl);
      self.jQueryAsnycLoadImg(bigUrl, element);
    };
    newImage.src = smallUrl;
  },

  jQueryAsnycLoadImg: function (url, intoElement) {
    var img = new Image();
    img.onload = function () {
      intoElement.attr('src', url);
      intoElement.removeClass('loading');
      intoElement.removeClass('preloading');
    };
    img.src = url;
  }
};