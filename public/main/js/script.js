(function($){

    "use strict";

    /*------------------------------------------------------*/
    /*Initialize Srollbar       
     /*------------------------------------------------------*/
    $(window).load(function(){
        if ( $().mCustomScrollbar ) {
            $(".scrollbar").mCustomScrollbar({
                axis: "x",
                theme: "light-3",
                scrollbarPosition: "outside",
                scrollInertia: 1200,
                contentTouchScroll: 15,
                advanced: {autoExpandHorizontalScroll: true},
                mouseWheel: {enable: false}
            });
        }
    });


    /*------------------------------------------------------*/
    /*Function to toggle Submenu on Hover        
     /*------------------------------------------------------*/
    if ($(window).width() > 767) {
        $("#site-nav li").on("hover", function () {
            $(this).children("ul").stop(true, true).slideToggle("slow");

        });
    }


    /*------------------------------------------------------*/
    /*Toggle fixed Social Icons for under desktop screens        
     /*------------------------------------------------------*/
    $('.sharing-fixed-toggle').on('click',function(e){
        e.preventDefault();
        $(this).closest('div').toggleClass('social-slide-in');
    });


    /*------------------------------------------------------*/
    /*Initialize Isotope       
     /*------------------------------------------------------*/
    function isotope() {
        if ($().isotope) {
            // init Isotope
            var filterBtns =$('.filter-button-group');
            var $container = $('.isotope-grid').isotope({
                itemSelector: '.item',
                animationEngine: 'best-available'

            });
            // bind filter a click
            filterBtns.on('click', 'a', function () {
                var filterValue = $(this).attr('data-filter');
                $container.isotope({filter: filterValue});
            });
            // change active class on a
            filterBtns.each(function (i, buttonGroup) {
                var $buttonGroup = $(buttonGroup);
                $buttonGroup.on('click', 'a', function () {
                    $buttonGroup.find('.active').removeClass('active');
                    $(this).addClass('active');
                });
            });
        }
    }
    isotope();
    $(window).load(function () {
        isotope();
    });

    /*------------------------------------------------------*/
    /*Prevent Redirection on click on any pagination Link      
     /*------------------------------------------------------*/
    $('.module-pagination a').on('click', function(e){
        e.preventDefault();
        $('.module-pagination a').removeClass('active');
        $(this).addClass('active');
    });


    /*------------------------------------------------------*/
    /*Events Page List and Calendar View
     /*------------------------------------------------------*/
    $('.change-view a').on('click', function(e){
        e.preventDefault();
        $('.change-view a').removeClass('active');
        $(this).addClass('active');
        var viewFilter = $(this).attr('data-filter');
        if(viewFilter == "events-calender-view"){
            $(".events-calender-view").show();
            $(".events-list-view").hide();
        }
        else{
            $(".events-calender-view").hide();
            $(".events-list-view").show();
        }

    });


    /*------------------------------------------------------*/
    /*Initialize wow animations     
     /*------------------------------------------------------*/
    var wow = new WOW(
        {
            boxClass: 'wow',      // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset: 50,          // distance to the element when triggering the animation (default is 0)
            mobile: false,       // trigger animations on mobile devices (default is true)
            live: true       // act on asynchronously loaded content (default is true)
        }
    );
    wow.init();


    /*------------------------------------------------------*/
    /*Popover
     /*------------------------------------------------------*/
    if ( $().popover ) {
        $(function () {
            $('[data-toggle="popover"]').popover({
                html: true,
                container: 'body',
                trigger: 'hover',
                delay: {"show": 100, "hide": 800}
            })
        });
    }


    // /*------------------------------------------------------*/
    // /*Google Maps
    //  /*------------------------------------------------------*/
    // function initializeContactMap() {
    //     var officeLocation = new google.maps.LatLng(-37.817314, 144.955431);
    //     var contactMapOptions = {
    //         zoom: 14,
    //         center: officeLocation,
    //         mapTypeId: google.maps.MapTypeId.ROADMAP,
    //         scrollwheel: false
    //     };
    //     var contactMap = new google.maps.Map(document.getElementById('map-canvas'), contactMapOptions);
    //
    //     var contactMarker = new google.maps.Marker({
    //         position: officeLocation,
    //         map: contactMap
    //     });
    //
    // }
    // if($('#map-canvas').length > 0) {
    //     window.onload = initializeContactMap();
    // }
})(jQuery);