function initMap() {
  var churchLocation = new google.maps.LatLng(47.878380, 22.023988);
  var emanuelHouseLocation = new google.maps.LatLng(47.878147, 22.022440);
  var contactMapOptions = {
    zoom: 17,
    center: churchLocation,
    mapTypeId: google.maps.MapTypeId.SATELLITE,
    scrollwheel: false,
  };
  var contactMap = new google.maps.Map(document.getElementById('map-canvas'), contactMapOptions);

  new google.maps.Marker({
    icon: "http://maps.google.com/mapfiles/markerA.png",
    position: churchLocation,
    map: contactMap,
    animation: google.maps.Animation.DROP
  });

  new google.maps.Marker({
    icon: "http://maps.google.com/mapfiles/markerB.png",
    position: emanuelHouseLocation,
    map: contactMap,
    animation: google.maps.Animation.DROP
  });
}
