(function ($) {
  'use strict';

  function LangaugeSelector() {
    this.langSelector = $('#language_switcher');

    this.route = function (forLanguage) {
      return mainpageRoute.split('/').map(function (value, index, array) {
        return (index === array.length - 1) ? forLanguage : value;
      }).join('/');
    };

    this.init = function () {
      this.attachEventListeners();
    };

    this.attachEventListeners = function () {
      var mainContext = this;
      mainContext.langSelector.on('change', function () {
        var option = $(this).find(':selected');
        window.location.replace(mainContext.route(option.val()));
      });
    };
  };

  var languageSelector = new LangaugeSelector();
  languageSelector.init();
})(jQuery);