<?php

namespace App\Provider;


use App\Entity\Gallery;
use App\Repository\ResourceQueryableRepository;
use Ateszworks\MultilangContentBundle\Provider\ContentProvider;
use Psr\Log\LoggerInterface;

/**
 * Class MPCSContentProvider
 * @package App\Provider
 */
class MPCSContentProvider extends ContentProvider
{

    /* @var LoggerInterface */
    protected $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    /**
     * @param array $entities
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function batchSaveOperation(array $entities): void
    {
        foreach ($entities as $entity) $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }

    /**
     * @param string $slug
     * @return mixed|null
     */
    public function getTranslationWithResourceByParameter(string $slug)
    {
        try {
            $translationRepoRef = $this->getTranslationRepositoryReference();
            if (!$translationRepoRef instanceof ResourceQueryableRepository) return null;
            /* @var $translationRepoRef ResourceQueryableRepository */
            return $translationRepoRef->getResourceByParam($slug);
        } catch (\Exception $e) {
            return null;
        }
    }

}