<?php

namespace App\Resolver;


use App\Entity\ArticleAttachedImage;
use App\Entity\GalleryImage;
use App\Entity\GalleryTranslation;
use App\Entity\Image;
use App\Utils\MpcsUtils;
use Ateszworks\MultilangContentBundle\Provider\LanguageProvider;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class ImageRouteResolver
 * @package App\Transformer
 */
class ImageRouteResolver
{

    /* @var RouterInterface */
    private $router;

    /* @var LanguageProvider */
    private $languageProvider;

    /**
     * ImageRouteResolver constructor.
     * @param RouterInterface $router
     * @param LanguageProvider $languageProvider
     */
    public function __construct(RouterInterface $router, LanguageProvider $languageProvider)
    {
        $this->router = $router;
        $this->languageProvider = $languageProvider;
    }

    /**
     * @param Image $image
     * @return array
     * @throws \Exception
     */
    public function resolveRoute(Image $image): array
    {
        if ($image instanceof ArticleAttachedImage) return $this->getRouteForAttachedImage($image->getImage());
        /* @var $image GalleryImage */
        /* @var $translation GalleryTranslation */
        $currLang = $this->languageProvider->getCurrentLanguage();
        $defLang = $this->languageProvider->getDefaultLanguage();
        $translation = $image->getGallery()->getTranslationForLanguage($currLang)
            ?? $image->getGallery()->getTranslationForLanguage($defLang)
            ?? $image->getGallery()->getTranslations()->first();
        return $this->getRouteForGalleryImage($translation->getUrlSlug(), $image->getImage());
    }

    /**
     * @param string $slug
     * @param string $name
     * @return array
     */
    private function getRouteForGalleryImage(string $slug, string $name): array
    {
        return [
            'small' => $this->getRoute('gallery_image',
                ['url_slug' => $slug, 'image_name' => MpcsUtils::getSmallNameFromNormal($name)]),
            'original' => $this->getRoute('gallery_image',
                ['url_slug' => $slug, 'image_name' => $name])
        ];
    }

    /**
     * @param string $name
     * @return array
     */
    private function getRouteForAttachedImage(string $name): array
    {
        return [
            'small' => $this->getRoute('image',
                ['name' => MpcsUtils::getSmallNameFromNormal($name)]),
            'original' => $this->getRoute('image',
                ['name' => $name])
        ];
    }

    /**
     * @param string $routeName
     * @param array $options
     * @return string
     */
    private function getRoute(string $routeName, array $options = []): string
    {
        return $this->router->generate($routeName, $options, UrlGeneratorInterface::ABSOLUTE_URL);
    }
}