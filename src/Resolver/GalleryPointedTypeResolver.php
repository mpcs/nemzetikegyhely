<?php

namespace App\Resolver;


use App\Entity\GalleryTranslation;
use App\Manager\MPCSGalleryManager;
use Ateszworks\MultilangContentBundle\ContentInterface\PointedTypeRouteResolver;
use Ateszworks\MultilangContentBundle\Provider\LanguageProvider;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class GalleryPointedTypeResolver implements PointedTypeRouteResolver
{

    /* @var MPCSGalleryManager */
    private $galleryManager;

    /* @var LanguageProvider */
    private $languageProvider;

    /* @var RouterInterface */
    private $router;

    /**
     * GalleryPointedTypeResolver constructor.
     * @param MPCSGalleryManager $galleryManager
     * @param LanguageProvider $languageProvider
     * @param RouterInterface $router
     */
    public function __construct(MPCSGalleryManager $galleryManager, LanguageProvider $languageProvider,
                                RouterInterface $router)
    {
        $this->galleryManager = $galleryManager;
        $this->languageProvider = $languageProvider;
        $this->router = $router;
    }

    /**
     * @return string
     */
    public function getPointedType(): string
    {
        return 'aw_gallery';
    }

    /**
     * @param string $id
     * @return string
     * @throws \Exception
     */
    public function getRouteForResource(string $id): string
    {
        /* @var GalleryTranslation $translation */
        /* @var $this ->contentProvider ContentProvider */
        $langCode = $this->languageProvider->getCurrentLanguage()->getCode();
        $translation = $this->galleryManager->getTranslationWithFallbackById((int)$id, $langCode);

        return $this->router->generate('gallery',
            ['_locale' => $langCode, 'url_slug' => $translation->getUrlSlug()],
            UrlGeneratorInterface::ABSOLUTE_PATH);
    }

    /**
     * @return string
     * Returns fully qualified class name of the entity that
     * the class/object implements this managing.
     */
    public function getManagedResourceTranslationClassName(): string
    {
        return GalleryTranslation::class;
    }

    /**
     * @return string
     * Returns the name of the property of the entity the query should look for the
     * queried string in.
     */
    public function getFieldForQuerying(): string
    {
        return 'title';
    }
}