<?php

namespace App\MetaReadable;


use Ateszworks\MultilangContentBundle\UserPresentable\UserPresentable;

/**
 * Interface MetaReadableContentTransformer
 * @package App\MetaReadable
 */
interface MetaReadableContentTransformer
{

    /**
     * @param UserPresentable $entity
     * @return MetaReadableContent
     */
    public function transform(UserPresentable $entity): MetaReadableContent;

    /**
     * Returns the FQCN of meta type it transforms. Used as id, must be unique.
     * @return array
     */
    public function getAssociatedMetaType(): array;

}