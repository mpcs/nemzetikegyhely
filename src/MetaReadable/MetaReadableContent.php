<?php

namespace App\MetaReadable;


interface MetaReadableContent
{

    /**
     * @return string|null
     */
    public function getOgTitle(): ?string;

    /**
     * @return string|null
     */
    public function getOgDescription(): ?string;

    /**
     * @return string|null
     */
    public function getOgImage(): ?string;

    /**
     * @return string|null
     */
    public function getOgUrl(): ?string;

    /**
     * @return string|null
     */
    public function getTwitterTitle(): ?string;

    /**
     * @return string|null
     */
    public function getTwitterDescription(): ?string;

    /**
     * @return string|null
     */
    public function getTwitterImage(): ?string;

    /**
     * @return string|null
     */
    public function getTwitterCard(): ?string;

}