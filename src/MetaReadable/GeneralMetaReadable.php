<?php

namespace App\MetaReadable;


class GeneralMetaReadable implements MetaReadableContent
{

    /* @var string */
    protected $ogTitle;

    /* @var string */
    protected $ogDescription;

    /* @var string */
    protected $ogImage;

    /* @var string */
    protected $ogUrl;

    /* @var string */
    protected $twitterTitle;

    /* @var string */
    protected $twitterDescription;

    /* @var string */
    protected $twitterImage;

    /* @var string */
    protected $twitterCard;

    /**
     * @return string
     */
    public function getOgTitle(): ?string
    {
        return $this->ogTitle;
    }

    /**
     * @return string
     */
    public function getOgDescription(): ?string
    {
        return $this->ogDescription;
    }

    /**
     * @return string
     */
    public function getOgImage(): ?string
    {
        return $this->ogImage;
    }

    /**
     * @return string
     */
    public function getOgUrl(): ?string
    {
        return $this->ogUrl;
    }

    /**
     * @return string
     */
    public function getTwitterTitle(): ?string
    {
        return $this->twitterTitle;
    }

    /**
     * @return string
     */
    public function getTwitterDescription(): ?string
    {
        return $this->twitterDescription;
    }

    /**
     * @return string
     */
    public function getTwitterImage(): ?string
    {
        return $this->twitterImage;
    }

    /**
     * @return string
     */
    public function getTwitterCard(): ?string
    {
        return $this->twitterCard;
    }

    // setters
    /**
     * @param string $ogTitle
     * @return GeneralMetaReadable
     */
    public function setOgTitle(string $ogTitle): GeneralMetaReadable
    {
        $this->ogTitle = $ogTitle;
        return $this;
    }

    /**
     * @param string $ogDescription
     * @return GeneralMetaReadable
     */
    public function setOgDescription(string $ogDescription): GeneralMetaReadable
    {
        $this->ogDescription = $ogDescription;
        return $this;
    }

    /**
     * @param string $ogImage
     * @return GeneralMetaReadable
     */
    public function setOgImage(?string $ogImage): GeneralMetaReadable
    {
        $this->ogImage = $ogImage;
        return $this;
    }

    /**
     * @param string $ogUrl
     * @return GeneralMetaReadable
     */
    public function setOgUrl(?string $ogUrl): GeneralMetaReadable
    {
        $this->ogUrl = $ogUrl;
        return $this;
    }

    /**
     * @param string $twitterTitle
     * @return GeneralMetaReadable
     */
    public function setTwitterTitle(string $twitterTitle): GeneralMetaReadable
    {
        $this->twitterTitle = $twitterTitle;
        return $this;
    }

    /**
     * @param string $twitterDescription
     * @return GeneralMetaReadable
     */
    public function setTwitterDescription(string $twitterDescription): GeneralMetaReadable
    {
        $this->twitterDescription = $twitterDescription;
        return $this;
    }

    /**
     * @param string $twitterImage
     * @return GeneralMetaReadable
     */
    public function setTwitterImage(?string $twitterImage): GeneralMetaReadable
    {
        $this->twitterImage = $twitterImage;
        return $this;
    }

    /**
     * @param string $twitterCard
     * @return GeneralMetaReadable
     */
    public function setTwitterCard(string $twitterCard): GeneralMetaReadable
    {
        $this->twitterCard = $twitterCard;
        return $this;
    }


}