<?php

namespace App\Repository;


use Ateszworks\MultilangContentBundle\Repository\AbstractTranslationQueryableRepository;

class GalleryTranslationRepository extends AbstractTranslationQueryableRepository
    implements ResourceQueryableRepository
{

    /**
     * @param $object
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getResourceByParam($object)
    {
        $qb = $this->createQueryBuilder('translation')
            ->addSelect(['translation', 'gallery'])
            ->join('translation.gallery', 'gallery');

        return $qb->where($qb->expr()->eq('translation.urlSlug', ':url_slug'))
            ->setParameter('url_slug', $object)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();;
    }
}