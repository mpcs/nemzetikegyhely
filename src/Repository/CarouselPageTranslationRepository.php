<?php

namespace App\Repository;


use Ateszworks\MultilangContentBundle\Repository\AbstractTranslationQueryableRepository;

class CarouselPageTranslationRepository extends AbstractTranslationQueryableRepository
{
}