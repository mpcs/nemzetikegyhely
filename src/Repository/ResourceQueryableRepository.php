<?php

namespace App\Repository;


/**
 * Interface ResourceQueryableRepository
 * @package App\Repository
 */
interface ResourceQueryableRepository
{

    /**
     * @param $object
     * @return mixed
     */
    public function getResourceByParam($object);

}