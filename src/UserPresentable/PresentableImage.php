<?php

namespace App\UserPresentable;


use Ateszworks\MultilangContentBundle\UserPresentable\UserPresentable;

/**
 * Class PresentableImage
 * @package App\UserPresentable
 */
class PresentableImage implements UserPresentable
{

    /* @var string */
    private $url = "";

    /* @var string */
    private $smallUrl = "";

    /**
     * @param array $details
     * @return PresentableImage
     */
    public static function initWithArray(array $details): PresentableImage
    {
        if (!$details || !isset($details['small']) || !isset($details['original'])) return new PresentableImage();
        return (new PresentableImage())->setSmallUrl($details['small'])->setUrl($details['original']);
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return PresentableImage
     */
    public function setUrl(string $url): PresentableImage
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getSmallUrl(): string
    {
        return $this->smallUrl;
    }

    /**
     * @param string $smallUrl
     * @return PresentableImage
     */
    public function setSmallUrl(string $smallUrl): PresentableImage
    {
        $this->smallUrl = $smallUrl;
        return $this;
    }

}