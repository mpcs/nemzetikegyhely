<?php

namespace App\UserPresentable;


use Ateszworks\MultilangContentBundle\UserPresentable\UserPresentable;

class PresentableGallery implements UserPresentable
{

    /* @var string */
    private $title;

    /* @var string */
    private $urlSlug;

    /* @var string */
    private $coverImage;

    /* @var string */
    private $smallCoverImage;

    /* @var string */
    private $created;

    /* @var string */
    private $published;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return PresentableGallery
     */
    public function setTitle(string $title): PresentableGallery
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrlSlug(): string
    {
        return $this->urlSlug;
    }

    /**
     * @param string $urlSlug
     * @return PresentableGallery
     */
    public function setUrlSlug(string $urlSlug): PresentableGallery
    {
        $this->urlSlug = $urlSlug;
        return $this;
    }

    /**
     * @return string
     */
    public function getCoverImage(): ?string
    {
        return $this->coverImage;
    }

    /**
     * @param string $coverImage
     * @return PresentableGallery
     */
    public function setCoverImage(string $coverImage): PresentableGallery
    {
        $this->coverImage = $coverImage;
        return $this;
    }

    /**
     * @return string
     */
    public function getSmallCoverImage(): ?string
    {
        return $this->smallCoverImage;
    }

    /**
     * @param string $smallCoverImage
     * @return PresentableGallery
     */
    public function setSmallCoverImage(string $smallCoverImage): PresentableGallery
    {
        $this->smallCoverImage = $smallCoverImage;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreated(): string
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return PresentableGallery
     */
    public function setCreated(string $created): PresentableGallery
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return string
     */
    public function getPublished(): ?string
    {
        return $this->published;
    }

    /**
     * @param string $published
     * @return PresentableGallery
     */
    public function setPublished(string $published): PresentableGallery
    {
        $this->published = $published;
        return $this;
    }

}