<?php

namespace App\UserPresentable;


/**
 * Class PresentableMetaBag
 * @package App\UserPresentable
 */
class PresentableMetaBag
{

    /**
     * @var string
     */
    protected $ogTitle;

    /**
     * @var string
     */
    protected $ogDescription;

    /**
     * @var string
     */
    protected $ogImage;

    /**
     * @var string
     */
    protected $ogUrl;

    /**
     * @var string
     */
    protected $twitterTitle;

    /**
     * @var string
     */
    protected $twitterDescription;

    /**
     * @var string
     */
    protected $twitterImage;

    /**
     * @var string
     */
    protected $twitterCard;

    /**
     * @return string|null
     */
    public function getOgTitle(): ?string
    {
        return $this->ogTitle;
    }

    /**
     * @param string|null $ogTitle
     * @return PresentableMetaBag
     */
    public function setOgTitle($ogTitle): PresentableMetaBag
    {
        $this->ogTitle = $ogTitle;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOgDescription(): ?string
    {
        return $this->ogDescription;
    }

    /**
     * @param string|null $ogDescription
     * @return PresentableMetaBag
     */
    public function setOgDescription($ogDescription): PresentableMetaBag
    {
        $this->ogDescription = $ogDescription;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOgImage(): ?string
    {
        return $this->ogImage;
    }

    /**
     * @param string|null $ogImage
     * @return PresentableMetaBag
     */
    public function setOgImage($ogImage): PresentableMetaBag
    {
        $this->ogImage = $ogImage;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOgUrl(): ?string
    {
        return $this->ogUrl;
    }

    /**
     * @param string|null $ogUrl
     * @return PresentableMetaBag
     */
    public function setOgUrl($ogUrl): PresentableMetaBag
    {
        $this->ogUrl = $ogUrl;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTwitterTitle(): ?string
    {
        return $this->twitterTitle;
    }

    /**
     * @param string|null $twitterTitle
     * @return PresentableMetaBag
     */
    public function setTwitterTitle($twitterTitle): PresentableMetaBag
    {
        $this->twitterTitle = $twitterTitle;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTwitterDescription(): ?string
    {
        return $this->twitterDescription;
    }

    /**
     * @param string|null $twitterDescription
     * @return PresentableMetaBag
     */
    public function setTwitterDescription($twitterDescription): PresentableMetaBag
    {
        $this->twitterDescription = $twitterDescription;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTwitterImage(): ?string
    {
        return $this->twitterImage;
    }

    /**
     * @param string|null $twitterImage
     * @return PresentableMetaBag
     */
    public function setTwitterImage($twitterImage): PresentableMetaBag
    {
        $this->twitterImage = $twitterImage;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTwitterCard(): ?string
    {
        return $this->twitterCard;
    }

    /**
     * @param string|null $twitterCard
     * @return PresentableMetaBag
     */
    public function setTwitterCard($twitterCard): PresentableMetaBag
    {
        $this->twitterCard = $twitterCard;
        return $this;
    }

}