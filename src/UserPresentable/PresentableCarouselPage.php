<?php

namespace App\UserPresentable;


use App\Entity\CarouselPageTranslation;
use Ateszworks\MultilangContentBundle\UserPresentable\UserPresentable;

/**
 * Class PresentableCarouselPage
 * @package App\UserPresentable
 */
class PresentableCarouselPage implements UserPresentable
{

    /* @var string */
    private $imageUrl;

    /* @var string */
    private $smallImage;

    /* @var string */
    private $content;

    /**
     * @return mixed
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * @return string
     */
    public function getSmallImage(): ?string
    {
        return $this->smallImage;
    }

    /**
     * @param string $smallImage
     * @return PresentableCarouselPage
     */
    public function setSmallImage(?string $smallImage): PresentableCarouselPage
    {
        $this->smallImage = $smallImage;
        return $this;
    }

    /**
     * @param mixed $imageUrl
     * @return PresentableCarouselPage
     */
    public function setImageUrl($imageUrl): PresentableCarouselPage
    {
        $this->imageUrl = $imageUrl;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     * @return PresentableCarouselPage
     */
    public function setContent($content): PresentableCarouselPage
    {
        $this->content = $content;
        return $this;
    }

}