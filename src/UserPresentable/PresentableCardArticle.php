<?php

namespace App\UserPresentable;


use Ateszworks\MultilangContentBundle\UserPresentable\UserPresentable;

class PresentableCardArticle implements UserPresentable
{

    /* @var string */
    private $title;

    /* @var string */
    private $coverImage;

    /* @var string */
    private $smallCoverImage;

    /* @var string */
    private $url;

    /* @var string */
    private $introText;

    /* @var string */
    private $author;

    /* @var string */
    private $created;

    /* @var string */
    private $published;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return PresentableCardArticle
     */
    public function setTitle(string $title): PresentableCardArticle
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getCoverImage(): ?string
    {
        return $this->coverImage;
    }

    /**
     * @param string $coverImage
     * @return PresentableCardArticle
     */
    public function setCoverImage(?string $coverImage): PresentableCardArticle
    {
        $this->coverImage = $coverImage;
        return $this;
    }

    /**
     * @return string
     */
    public function getSmallCoverImage(): ?string
    {
        return $this->smallCoverImage;
    }

    /**
     * @param string $smallCoverImage
     * @return PresentableCardArticle
     */
    public function setSmallCoverImage(?string $smallCoverImage): PresentableCardArticle
    {
        $this->smallCoverImage = $smallCoverImage;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     * @return PresentableCardArticle
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getIntroText(): string
    {
        return $this->introText;
    }

    /**
     * @param string $introText
     * @return PresentableCardArticle
     */
    public function setIntroText(string $introText): PresentableCardArticle
    {
        $this->introText = $introText;
        return $this;
    }

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * @param string $author
     * @return PresentableCardArticle
     */
    public function setAuthor(string $author): PresentableCardArticle
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreated(): string
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return PresentableCardArticle
     */
    public function setCreated(string $created): PresentableCardArticle
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return string
     */
    public function getPublished(): string
    {
        return $this->published;
    }

    /**
     * @param string $published
     * @return PresentableCardArticle
     */
    public function setPublished(string $published): PresentableCardArticle
    {
        $this->published = $published;
        return $this;
    }

}