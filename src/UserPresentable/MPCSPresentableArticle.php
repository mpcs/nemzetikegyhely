<?php

namespace App\UserPresentable;


use Ateszworks\MultilangContentBundle\UserPresentable\PresentableArticle;

class MPCSPresentableArticle extends PresentableArticle
{

    public static function createFromPresentableArticle(PresentableArticle $article)
    {
        return (new MPCSPresentableArticle())
            ->setTitle($article->getTitle())
            ->setContents($article->getContents())
            ->setCoverImage($article->getCoverImage())
            ->setCreated($article->getCreated())
            ->setAuthor($article->getAuthor())
            ->setPublished($article->getPublished());
    }

    /* @var string */
    private $smallCoverImage;

    /* @var array */
    private $images = [];

    /**
     * @return string
     */
    public function getSmallCoverImage(): ?string
    {
        return $this->smallCoverImage;
    }

    /**
     * @param string $smallCoverImage
     * @return MPCSPresentableArticle
     */
    public function setSmallCoverImage(?string $smallCoverImage): MPCSPresentableArticle
    {
        $this->smallCoverImage = $smallCoverImage;
        return $this;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        return $this->images;
    }

    /**
     * @param array $images
     * @return MPCSPresentableArticle
     */
    public function setImages(array $images): MPCSPresentableArticle
    {
        $this->images = $images;
        return $this;
    }

}