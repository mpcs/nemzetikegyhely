<?php

namespace App\Utils;


use Error;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class DynamicDataInjector
 * @package App\Transformer
 */
class DynamicDataInjector
{

    /* @var RouterInterface */
    private $router;

    /* @var array[{$key => $value}] */
    private $_cache = [];

    /**
     * DynamicDataInjector constructor.
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param string $key
     * @return String
     */
    public function dataForKey(string $key): String
    {
        if (in_array($key, array_keys($this->_cache))) return $this->_cache[$key];
        if (strstr($key, 'static_files')) return $this->replaceStaticFile($key);
        throw new Error('No data found');
    }

    /**
     * @return array
     */
    public function exposeDictionary(): array
    {
        return $this->_cache;
    }

    /**
     * @param string $staticFileKey
     * @return String
     */
    private function replaceStaticFile(string $staticFileKey): String
    {
        $match = [];
        preg_match('#\((.*?)\)#', $staticFileKey, $match);
        return $this->router->generate('static_files',
            ['file_name' => $match[1]], UrlGeneratorInterface::ABSOLUTE_URL);
    }
}