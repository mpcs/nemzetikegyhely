<?php

namespace App\Utils;

use DateTime;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class MpcsUtils
 * @package App\Utils
 */
class MpcsUtils
{

    /**
     * @param string $string
     * @param int $length
     * @return string
     * Creates a cut string from the original one.
     */
    public static function introText(string $string, int $length = 300): string
    {
        $string = strip_tags($string);
        if (strlen($string) > $length) {
            // truncate string
            $stringCut = substr($string, 0, $length);
            $endPoint = strrpos($stringCut, ' ');

            //if the string doesn't contain any space then it will cut without word basis.
            $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
            $string .= '...';
        }

        return $string;
    }

    /**
     * @param string $fileName
     * @return null|string
     */
    public static function getSmallFileAsB64(string $fileName): ?string
    {
        if (!file_exists($fileName)) return null;

        $type = pathinfo($fileName, PATHINFO_EXTENSION);
        $data = file_get_contents($fileName);
        return 'data:image/' . $type . ';base64,' . base64_encode($data);
    }

    /**
     * @param string $fileName
     * @return string
     */
    public static function getSmallNameFromNormal(string $fileName): string
    {
        $parts = explode('.', $fileName);
        $smallImageName = $parts[count($parts) - 2] . '_small.' . $parts[count($parts) - 1];

        return $smallImageName;
    }

    /**
     * @param UploadedFile $file
     * @return ConstraintViolationListInterface
     */
    public static function validateImage(UploadedFile $file): ConstraintViolationListInterface
    {
        $validator = Validation::createValidator();
        return $validator->validate($file->getRealPath(), new Assert\Image([
            'uploadErrorMessage' => 'admin.asyncup.did_not_get_image'
        ]));
    }

    /**
     * This method is a helper for my view in order to separate elements to two groups.
     * @param $all
     * @return array
     */
    public static function separate(array $all): array
    {
        $result = ['left' => [], 'right' => []];
        for ($i = 0; $i < count($all); ++$i)
            $i % 2 == 0 ?
                $result['left'][] = $all[$i] : $result['right'][] = $all[$i];

        return $result;
    }

    /**
     * @param array $date
     * @return void
     */
    public static function threePartDateChecker(array &$date): void
    {
        $now = new DateTime();
        if (!isset($date['year']) || '' == $date['year']) $date['year'] = $now->format('Y');
        if (!isset($date['month']) || '' == $date['month']) $date['month'] = $now->format('n');
        if (!isset($date['day']) || '' == $date['day']) $date['day'] = $now->format('d');
    }
}