<?php

namespace App\Utils;


use Ateszworks\MultilangContentBundle\ContentInterface\ContentTranslation;
use Ateszworks\MultilangContentBundle\Entity\TranslationFilterableEntity;

class TranslationFilter
{

    /* @var function */
    protected $filter;

    public function __construct()
    {
        $this->filter = function (ContentTranslation $translation) {
            return $translation && strlen($translation->getContent()) > 0;
        };
    }

    /**
     * @param TranslationFilterableEntity $resource
     * @return bool
     */
    public function checkTranslations(TranslationFilterableEntity $resource): bool
    {
        if (!$resource || !$resource->getTranslations()) return false;
        $clone = clone $resource;
        return $clone->getTranslations()->filter($this->filter)->count() > 0;
    }

    /**
     * @param TranslationFilterableEntity $resource
     */
    public function filterTranslations(TranslationFilterableEntity $resource): void
    {
        $resource->setTranslations($resource->getTranslations()->filter($this->filter));
    }

}