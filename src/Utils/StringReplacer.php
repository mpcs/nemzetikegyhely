<?php

namespace App\Utils;


class StringReplacer
{
    /* @var DynamicDataInjector */
    private $dataInjector;

    /**
     * StringReplacer constructor.
     * @param DynamicDataInjector $dataInjector
     */
    public function __construct(DynamicDataInjector $dataInjector)
    {
        $this->dataInjector = $dataInjector;
    }

    /**
     * @param string $toText
     * @param array|null $byDictionary
     * @return String
     */
    public function injectData(string $toText, ?array $byDictionary = []): String
    {
        return preg_replace_callback(
            '/\{inject:[^}]*\}/',
            function (array $m) use ($byDictionary) {
                $cleanMatch = substr(explode(':', $m[0])[1], 0, -1);
                return $byDictionary
                    ? array_key_exists($cleanMatch, $byDictionary) ? $byDictionary[$cleanMatch] : ''
                    : $this->dataInjector->dataForKey($cleanMatch);
            },
            $toText
        );
    }
}