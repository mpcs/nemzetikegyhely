<?php

namespace Ateszworks\AdminBundle\Controller;

use Ateszworks\AdminBundle\Form\LoginType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends Controller
{

    /**
     * @return Response
     * This is a test method, I used it for testing routing.
     */
    public function hello(): Response
    {
        return $this->render('@AteszworksAdmin/security/dummyLogout.html.twig');
    }

    /**
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // errors:
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        $form = $this->createForm(LoginType::class);

        return $this->render('@AteszworksAdmin/security/login.html.twig', [
            'last_username' => $lastUsername,
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * This is the route the user can use to logout.
     *
     * But, this will never be executed. Symfony will intercept this first
     * and handle the logout automatically. See logout in app/config/security.yml
     *
     */
    public function logout(): void
    {
        throw new \Exception('This should never be reached!');
    }

}