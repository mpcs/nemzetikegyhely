<?php

namespace Ateszworks\AdminBundle\Controller;

use Ateszworks\MultilangContentBundle\Entity\MenuEntity;
use Ateszworks\MultilangContentBundle\Manager\MenuManager;
use Ateszworks\MultilangContentBundle\Provider\LanguageProvider;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validation;

/**
 * Class MenuController
 * @package Ateszworks\AdminBundle\Controller
 */
class MenuController extends Controller
{

    /**
     * String representing non-provided value.
     */
    CONST UNPROVIDED_VALUE_STRING = '??'; // todo: move to config.

    /**
     * @var Validation
     */
    private $validator;

    /* @var MenuManager */
    private $menuManager;

    /**
     * MenuController constructor.
     * @param MenuManager $menuManager
     */
    public function __construct(MenuManager $menuManager)
    {
        $this->validator = Validation::createValidator();
        $this->menuManager = $menuManager;
    }

    /**
     * @param MenuManager $menuManager
     * @return Response
     */
    public function listMenu(MenuManager $menuManager): Response
    {
        /** @var $results MenuEntity */
        $results = $menuManager->getMenuTranslations();
        return $this->render('@AteszworksAdmin/menu/list.html.twig', ['menus' => $results]);
    }

    /**
     * @param Request $request
     * @param LanguageProvider $languageProvider
     * @return Response
     * @throws \Exception
     * @throws \Error
     */
    public function editMenu(Request $request, LanguageProvider $languageProvider): Response
    {
        $id = $request->get('resource_id');
        $resource = 'add' == $id ? null : $this->menuManager->getEntityById($id);
        $allLangInSystem = $languageProvider->getAllAvailableLanguages();
        $currentLang = $languageProvider->getCurrentLanguage();

        $menuData = $request->request->get('titles');
        $weight = (integer)$request->request->get('weight');
        $pointedType = $request->request->get('pointed-type-switch')
            ? MenuEntity::RESOURCE_POINTING_TYPE : MenuEntity::LINK_POINTING_TYPE;
        $targetUrl = $request->request->get('target_url', null);
        $targetResourceId = $request->request->get('target_resource_id', null);
        $targetResourceType = $request->request->get('target_resource_type', null);
        $violations = new ConstraintViolationList();

        if ($menuData) {
            $violations = $this->menuManager->validatePostedTranslations($menuData);
            $violations->addAll($this->menuManager->validateNumber($weight));
            $violations->addAll($this->menuManager->validateTarget($pointedType));
            if (0 == count($violations)) { // todo: move saving to manager
                $isNew = null == $resource;
                $resource = $resource ? $resource : new MenuEntity();
                $resource->setWeight($weight);
                $resource->setType($pointedType);
                $resource->setTarget(MenuEntity::RESOURCE_POINTING_TYPE == $pointedType
                    ? $targetResourceType : $targetUrl);
                $resource->setTargetIdentifier($targetResourceId);
                $this->menuManager->mergeUpdateTranslations($resource, $menuData, $isNew);

                if ($this->menuManager->saveMenu($resource)) {
                    $this->addFlash('general.success', 'content_action.menu_successfully_saved');
                    return $this->redirectToRoute('ateszworks_admin_menu');
                }
            }
        }

        return $this->render('@AteszworksAdmin/menu/form.html.twig', [
            'languages' => $allLangInSystem,
            'currentLanguage' => $currentLang,
            'menu' => $resource,
            'unprovidedValuePlaceholder' => $this::UNPROVIDED_VALUE_STRING,
            'errors' => $violations
        ]);
    }

    // AJAX

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function requestPointableResources(Request $request): JsonResponse
    {
        $queryString = $request->attributes->get('name_fraction');
        $suggestions = $this->menuManager->getResourceSuggestions($queryString);

        return new JsonResponse($suggestions);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function removeMenu(Request $request): JsonResponse
    {
        $menuId = $request->attributes->get('resource_id');
        try {
            $resource = $this->menuManager->getEntityById($menuId);
            if (!$resource) throw new NotFoundHttpException('Nem található tartalom', null, 404);
            $this->menuManager->removeMenu($resource);
        } catch (NotFoundHttpException $exception) {
            return new JsonResponse('Not found.', 404);
        }

        return new JsonResponse('Successful removal.');
    }

}