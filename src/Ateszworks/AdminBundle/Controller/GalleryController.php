<?php

namespace Ateszworks\AdminBundle\Controller;

use App\Entity\GalleryImage;
use Ateszworks\AdminBundle\Form\GalleryImageType;
use App\Entity\Gallery;
use App\Manager\MPCSGalleryImageManager;
use App\Manager\MPCSGalleryManager;
use App\Utils\MpcsUtils;
use Ateszworks\AdminBundle\Form\GalleryType;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class GalleryController
 * @package Ateszworks\AdminBundle\Controller
 */
class GalleryController extends ContentManagerController
{

    /* @var LoggerInterface */
    private $logger;

    /* @var MPCSGalleryManager */
    protected $manager;

    /* @var MPCSGalleryImageManager */
    protected $imageManager;

    /**
     * GalleryController constructor.
     * @param MPCSGalleryManager $manager
     * @param LoggerInterface $logger
     * @param MPCSGalleryImageManager $imageManager
     */
    public function __construct(MPCSGalleryManager $manager, LoggerInterface $logger,
                                MPCSGalleryImageManager $imageManager)
    {
        parent::__construct($manager);
        $this->logger = $logger;
        $this->imageManager = $imageManager;
    }

    /**
     * @param MPCSGalleryManager $galleryManager
     * @return Response
     */
    public function listAll(MPCSGalleryManager $galleryManager): Response
    {
        $results = $galleryManager->adminListableWithTranslations();
        return $this->render('@AteszworksAdmin/gallery/list.html.twig', ['galleries' => $results]);
    }

    /**
     * @param Request $request
     * @param MPCSGalleryManager $manager
     * @return Response
     * @throws Exception
     */
    public function edit(Request $request, MPCSGalleryManager $manager): Response
    {
        $resourceId = $request->attributes->get('resource_id');
        $galleryEntity = $resourceId ? $manager->getById($resourceId) ?? new Gallery() : new Gallery();

        $form = $this->createForm(GalleryType::class, $galleryEntity);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if (!$manager->checkTranslations($galleryEntity))
                $form->addError(new FormError('form.gallery.errors.at_least_one_translation'));

            if ($form->isValid()) {
                try {
                    $manager->save($galleryEntity);
                    $this->addFlash('general.success', 'gallery.action.page_successfully_saved');
                    return $this->redirectToRoute('ateszworks_admin_gallery');
                } catch (Exception $e) {
                    if ($e instanceof OptimisticLockException || $e instanceof ORMException) {
                        $form->addError(new FormError('form.errors.unsuccessful_save'));
                    } else {
                        throw $e;
                    }
                }
            }
        }

        return $this->render('@AteszworksAdmin/gallery/form.html.twig', [
            'form' => $form->createView(),
            'formErrors' => $form->getErrors()
        ]);
    }

    /**
     * @param Request $request
     * @param MPCSGalleryManager $manager
     * @param MPCSGalleryImageManager $imageManager
     * @return Response
     */
    public function manageImages(Request $request, MPCSGalleryManager $manager,
                                 MPCSGalleryImageManager $imageManager): Response
    {
        $gallery = $manager->getEntityById($request->attributes->get('resource_id'));
        if (!$gallery) throw new NotFoundHttpException();

        $form = $this->createForm(GalleryImageType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $images = $form['images']->getData();

            try {
                $uploadedImages = $imageManager->setGallery($gallery)->handleUpload($images);
                $manager->attachImages($gallery, $uploadedImages);
            } catch (Exception $e) {
                $this->logger->error('Unable to save some images.', ['msg' => $e->getMessage(),
                    'trace' => $e->getTrace()]);
                $this->addFlash('general.error', 'content_action.image.some_not_saved');
            }

            $this->addFlash('general.success', 'form.gallery.manage_images.successful_save');
            return $this->redirectToRoute('ateszworks_admin_gallery_images',
                ['resource_id' => $gallery->getId()]);
        }

        return $this->render('@AteszworksAdmin/gallery/manage_images.html.twig', [
            'form' => $form->createView(),
            'errors' => $form->getErrors(),
            'gallery_id' => $gallery->getId(),
            'images' => $imageManager->getImages($gallery)
        ]);
    }

    /**
     * @param Request $request
     * @param TranslatorInterface $translator
     * @return JsonResponse
     */
    public function addImage(Request $request, TranslatorInterface $translator): JsonResponse
    {
        /* @var UploadedFile $image */
        $image = $request->files->get('image');
        $galleryId = $request->attributes->get('gallery_id');

        if (!$image || !$galleryId) return new JsonResponse(['message' => 'Missing params.'], 400);

        $violations = MpcsUtils::validateImage($image);
        if (count($violations) > 0)
            return new JsonResponse($translator->trans($violations->get(0)->getMessage()),
                400);

        $originalImageName = $image->getClientOriginalName();
        try {
            $storedName = array_merge($this->manager->uploadImage($galleryId, $image),
                ['originalName' => $originalImageName]);
        } catch (Exception $e) {
            return new JsonResponse(['message' =>
                $translator->trans('gallery.error.image_upload', ['%img%' => $originalImageName])], 500);
        }

        // todo: make it nicer... move this logic to managers!!

        $gallery = $this->manager->getById($galleryId);
        $galleryImage = new GalleryImage();
        $galleryImage->setGallery($gallery);
        $galleryImage->setOriginalName($image->getClientOriginalName());
        $galleryImage->setStoredName($storedName['normal']);

        try {
            $this->imageManager->attachToGallery($gallery, $galleryImage);
        } catch (Exception $e) {
            return new JsonResponse('Unable to save. :(', 500);
        }

        return new JsonResponse($storedName);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function removeImage(Request $request): JsonResponse
    {
        $galleryId = $request->attributes->get('gallery_id');
        $storedName = $request->attributes->get('resource_id');
        $this->imageManager->removeById($galleryId, $storedName);

        return new JsonResponse();
    }

    /**
     * @param int $gallery_id
     * @param string $resource_name
     * @return BinaryFileResponse
     */
    public function viewImage(int $gallery_id, string $resource_name): BinaryFileResponse
    {
        return $this->manager->getImageAsBinaryResponse($gallery_id, $resource_name);
    }

}