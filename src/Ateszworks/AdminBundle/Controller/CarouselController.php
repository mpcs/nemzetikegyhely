<?php

namespace Ateszworks\AdminBundle\Controller;

use App\Entity\CarouselPage;
use App\Manager\MPCSCarouselManager;
use Ateszworks\AdminBundle\Form\CarouselType;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CarouselController
 * @package Ateszworks\AdminBundle\Controller
 */
class CarouselController extends ContentManagerController
{

    /**
     * CarouselController constructor.
     * @param MPCSCarouselManager $manager
     */
    public function __construct(MPCSCarouselManager $manager)
    {
        parent::__construct($manager);
    }

    /**
     * @param MPCSCarouselManager $carouselManager
     * @return Response
     */
    public function listPages(MPCSCarouselManager $carouselManager): Response
    {
        $pages = $carouselManager->getAll();
        return $this->render('@AteszworksAdmin/carousel/list.html.twig', ['carousels' => $pages]);
    }

    /**
     * @param Request $request
     * @param MPCSCarouselManager $carouselManager
     * @return Response
     * (re) @throws Exception
     * @throws Exception
     */
    public function managePage(Request $request, MPCSCarouselManager $carouselManager): Response
    {
        $resourceId = $request->attributes->get('resource_id');
        $carouselPageEntity = $resourceId ? $carouselManager->getById($resourceId) : new CarouselPage();

        $form = $this->createForm(CarouselType::class, $carouselPageEntity ?? new CarouselPage());
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $image = $form['carousel_image']->getData();
            if (!$image && !$carouselPageEntity->getImage())
                $form->addError(new FormError('form.carousel.errors.missing_image'));

            if (!$carouselManager->checkTranslations($carouselPageEntity))
                $form->addError(new FormError('form.carousel.errors.at_least_one_translation'));

            if ($form->isValid()) {
                try {
                    $image = $form['carousel_image']->getData();
                    $carouselManager->save($carouselPageEntity, $image);
                    $this->addFlash('general.success', 'carousel.action.page_successfully_saved');
                    return $this->redirectToRoute('ateszworks_admin_carousel');
                } catch (Exception $e) {
                    if ($e instanceof OptimisticLockException || $e instanceof ORMException) {
                        $form->addError(new FormError('form.errors.unsuccessful_save'));
                    } else {
                        throw $e;
                    }
                }
            }
        }

        return $this->render('@AteszworksAdmin/carousel/form.html.twig', [
            'form' => $form->createView(),
            'formErrors' => $form->getErrors()
        ]);
    }

}