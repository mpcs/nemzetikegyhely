<?php

namespace Ateszworks\AdminBundle\Controller;

use Ateszworks\MultilangContentBundle\Manager\RemoveCapableManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

abstract class ContentManagerController extends Controller
{

    // Inject the appropriate one from subclass!
    /* @var RemoveCapableManager */
    protected $manager;

    /**
     * ContentManagerController constructor.
     * @param RemoveCapableManager $manager
     */
    public function __construct(RemoveCapableManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function remove(Request $request): JSONResponse
    {
        $resouceId = $request->attributes->get('resource_id');
        try {
            $this->manager->removeById($resouceId);
        } catch (NotFoundHttpException $exception) {
            return new JsonResponse('Not found.', 404);
        }

        return new JsonResponse('Successful removal.');
    }
}