<?php

namespace Ateszworks\AdminBundle\Controller;

use App\Manager\MPCSArticleManager;
use App\Manager\MPCSAttachedImageManager;
use Ateszworks\AdminBundle\Utils\SessionFileBag;
use Ateszworks\AdminBundle\Form\ArticleTranslationType;
use Ateszworks\MultilangContentBundle\Entity\ArticleTranslationEntity;
use Ateszworks\MultilangContentBundle\Manager\ArticleManager;
use Ateszworks\MultilangContentBundle\Entity\ArticleEntity;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ArticleController
 * @package Ateszworks\AdminBundle\Controller
 */
class ArticleController extends Controller
{

    /**
     * @param ArticleManager $articleManager
     * @return Response
     */
    public function listArticles(ArticleManager $articleManager): Response
    {
        $results = $articleManager->adminListableArticlesWithTranslations();
        return $this->render('@AteszworksAdmin/article/article.html.twig', ['articles' => $results]);
    }

    /**
     * @param Request $request
     * @param MPCSArticleManager $articleManager
     * @param LoggerInterface $logger
     * @param MPCSAttachedImageManager $attachedImageManager
     * @param SessionFileBag $fileBag
     * @return Response
     */
    public function addArticle(Request $request, MPCSArticleManager $articleManager, LoggerInterface $logger,
                               MPCSAttachedImageManager $attachedImageManager, SessionFileBag $fileBag): Response
    {
        $editingId = $request->attributes->get('resource_id');
        $bagId = $request->request->get('filebag_id', md5(uniqid()));
        $editMode = $editingId ? true : false;
        /* @var $article ArticleEntity */
        $articleTranslation = new ArticleTranslationEntity();

        if ($editMode) {
            $article = $articleManager->getEntityById($editingId);
            if (!$article) throw new NotFoundHttpException('Content not found.', null, 404);
            $awArticle = $request->request->get('aw_article_translation');
            $currentTranslationLangCode = isset($awArticle['language_selector'])
                ? $awArticle['language_selector'] : null;
            $articleTranslation =
                $articleManager->getTranslationOfEntityByIdAndLang($editingId, $currentTranslationLangCode)
                ?? (new ArticleTranslationEntity())->setArticle($article);

            try {
                $attachedImageManager->attachImagesToArticle($article, $fileBag->returnAsFiles($bagId));
            } catch (Exception $e) {
                $logger->error('Could not attach images from session file bag to article.',
                    ['article' => $article->getId()]);
            }
        }

        $form = $this->createForm(ArticleTranslationType::class, $articleTranslation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $returnedTranslation = $form->getData();

            $coverImage = $form['article']['article_image']->getData();
            $attachedImages = $form['article']['attached_images']->getData();

            try {
                $attachedImages = array_merge($attachedImages, $fileBag->returnAsFiles($bagId));
                $attachedImageManager->attachImagesToArticle($returnedTranslation->getArticle(), $attachedImages);
            } catch (\Exception $e) {
                $logger->error('Unable to save some images.', ['msg' => $e->getMessage(),
                    'trace' => $e->getTrace()]);
                $this->addFlash('general.error', 'content_action.image.some_not_saved');
            }
            $articleManager->handleImageUpload($returnedTranslation, $coverImage);
            $articleManager->save($returnedTranslation);
            $this->addFlash('general.success', 'content_action.article_successfully_saved');
            return $this->redirectToRoute('ateszworks_admin_article');
        }

        $attachedImages = $articleManager->getAttachedImages($articleTranslation);

        return $this->render('@AteszworksAdmin/article/add_article.html.twig', [
            'form' => $form->createView(),
            'viewMode' => 'add',
            'editMode' => $editMode,
            'articleTranslation' => $articleTranslation,
            'attachedImages' => $attachedImages,
            'uniqId' => $bagId,
            'errors' => $form->getErrors()
        ]);
    }

    /**
     * AJAX METHODS
     * @param Request $request
     * @param ArticleManager $articleManager
     * @return JsonResponse
     */
    public function loadArticleById(Request $request, ArticleManager $articleManager): JsonResponse
    {
        $response = ['title' => null, 'isListed' => null, 'content' => null];
        $articleId = $request->attributes->get('resource_id');
        $languageCode = $request->attributes->get('lang_code');
        try {
            $articleTranslation = $articleManager->getTranslationOfEntityByIdAndLang($articleId, $languageCode);
        } catch (Exception $e) {
            return new JsonResponse('Hiba történt!', 500);
        }


        if ($articleTranslation) {
            $response = [
                'title' => $articleTranslation->getTitle(),
                'isListed' => $articleTranslation->getArticle()->isListed(),
                'content' => $articleTranslation->getContent()
            ];
        }

        return new JsonResponse($response);
    }

    /**
     * @param Request $request
     * @param ArticleManager $articleManager
     * @param MPCSAttachedImageManager $attachedImageManager
     * @return JsonResponse
     */
    public function removeArticle(Request $request, ArticleManager $articleManager,
                                  MPCSAttachedImageManager $attachedImageManager): JsonResponse
    {
        $articleId = $request->attributes->get('resource_id');
        try {
            $article = $articleManager->getEntityById($articleId);
            $attachedImageManager->removeImagesOf($article);
            $articleManager->remove($article);
        } catch (NotFoundHttpException $exception) {
            return new JsonResponse('Not found.', 404);
        }

        return new JsonResponse('Successful removal.');
    }

}