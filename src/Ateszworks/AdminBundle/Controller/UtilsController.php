<?php

namespace Ateszworks\AdminBundle\Controller;


use App\Manager\MPCSArticleManager;
use App\Manager\MPCSAttachedImageManager;
use App\Manager\MPCSFileManager;
use Ateszworks\AdminBundle\Utils\SessionFileBag;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class UtilsController
 * @package Ateszworks\AdminBundle\Controller
 */
class UtilsController extends Controller
{

    /* @var TranslatorInterface */
    private $translator;

    /* @var MPCSAttachedImageManager */
    private $attachedImageManager;

    /* @var MPCSFileManager */
    private $fileManager;

    /* @var MPCSArticleManager */
    private $articleManager;

    /* @var RouterInterface $router */
    private $router;

    /**
     * UtilsController constructor.
     * @param TranslatorInterface $translator
     * @param MPCSAttachedImageManager $manager
     * @param MPCSFileManager $fileManager
     * @param MPCSArticleManager $articleManager
     */
    public function __construct(TranslatorInterface $translator,
                                MPCSAttachedImageManager $manager,
                                MPCSFileManager $fileManager, RouterInterface $router,
                                MPCSArticleManager $articleManager)
    {
        $this->translator = $translator;
        $this->attachedImageManager = $manager;
        $this->fileManager = $fileManager;
        $this->articleManager = $articleManager;
        $this->router = $router;
    }

    /**
     * @param Request $request
     * @param SessionFileBag $fileBag
     * @param LoggerInterface $logger
     * @param MPCSFileManager $fileManager
     * @param ValidatorInterface $validator
     * @return JsonResponse
     */
    public function acceptIncomingImage(Request $request, SessionFileBag $fileBag, LoggerInterface $logger,
                                        MPCSFileManager $fileManager,
                                        ValidatorInterface $validator): JsonResponse
    {
        try {
            $iterator = $request->files->getIterator();
            $bagId = $request->request->get('filebagId');
            $url = ['small' => null, 'original' => ''];

            while ($iterator->valid()) {
                $image = $iterator->current();
                /* @var $concreteImage UploadedFile */
                $concreteImage = $image[0];
                /* @var $violations ConstraintViolationList */
                // todo: use MpcsUtils
                $violations = $validator->validate($concreteImage->getRealPath(), new Assert\Image([
                    'uploadErrorMessage' => 'admin.asyncup.did_not_get_image'
                ]));

                if (count($violations) > 0)
                    return new JsonResponse($this->translator->trans($violations->get(0)->getMessage()),
                        400);

                $storedName = $fileManager->uploadImage($concreteImage);
                $fileBag->addToBag($bagId, $storedName);
                $url['original'] = $this->urlForImage($storedName);
                $url['small'] = $this->urlForImage($fileManager->convertNameToSmall($storedName));
                $iterator->next();
            }

            return new JsonResponse($url);
        } catch (\Exception $e) {
            $logger->error('System was unable to complete upload.', [
                'message' => $e->getMessage(),
                'trace' => $e->getTrace(),
                'code' => $e->getCode()
            ]);
            return new JsonResponse($this->translator->trans('admin.general.unable_to_upload'), 500);
        }
    }

    /**
     * @param Request $request
     * @param SessionFileBag $fileBag
     * @param TranslatorInterface $translator
     * @param LoggerInterface $logger
     * @return JsonResponse
     */
    public function removeImageFromBag(Request $request, SessionFileBag $fileBag,
                                       TranslatorInterface $translator, LoggerInterface $logger): JsonResponse
    {
        $fileUrl = $request->attributes->get('name');
        try {
            $fileBag->removeFromBag($fileUrl);
            $this->attachedImageManager->removeImageByUrl($fileUrl);
        } catch (Exception $e) {
            $logger->error('Could not delete attached img.', [
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
            return new JsonResponse($translator->trans('general.error'), 500);
        }

        return new JsonResponse();
    }

    /**
     * @param string $name
     * @param MPCSFileManager $fileManager
     * @return BinaryFileResponse
     */
    public function showImage(string $name, MPCSFileManager $fileManager): BinaryFileResponse
    {
        $image = $fileManager->getFullFileURL($name);
        return new BinaryFileResponse($image);
    }

    /**
     * @param string $imageName
     * @return string
     */
    private function urlForImage(string $imageName): string
    {
        return $this->router->generate('ateszworks_admin_async_image', ['name' => $imageName],
            UrlGeneratorInterface::ABSOLUTE_URL);
    }

}