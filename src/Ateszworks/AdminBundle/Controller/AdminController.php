<?php

namespace Ateszworks\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AdminController
 * @package Ateszworks\AdminBundle\Controller
 */
class AdminController extends Controller
{

    /**
     * @return Response
     */
    public function welcome(): Response
    {
        return $this->render('@AteszworksAdmin/admin/welcome.html.twig');
    }

}