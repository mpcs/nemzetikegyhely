# Admin Bundle

## About this bundle
This is going to be more and more reusable through stages of development.

I have many, and huge plans on improving it, but yet it should stay simple, since I need it.

## Usage
Basically right now, It is limited for menu and content editing,
but leaves a window open by requiring a content storage layer to be
injected, which requires to have a managed content type, which makes it
very very portable. This is only the beginning.

## Interfaces folder
You should check that out, that contains the only dependency yet.