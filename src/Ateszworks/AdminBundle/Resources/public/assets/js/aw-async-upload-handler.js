var wontWorkProperlyText = 'Hibás config, az async feltöltés nem fog jól működni.';
if (!removeRoute) alert(wontWorkProperlyText);
if (!addRoute) alert(wontWorkProperlyText);
if (!('FormData' in window && 'FileReader' in window)) alert('A böngésző nem képes async képfeltöltésre, fallback.');

var AsyncUploadHandler = function (upRoute, inputElementId, responderObject) {
  this.input = document.getElementById(inputElementId);
  this.upUrl = upRoute;
  this.formData = new FormData();
  this.totalFiles = 0;
  this.doneFiles = 0;

  this.initialize = function () {
    var self = this;
    this.input.onchange = function (e) {
      e.preventDefault();
      var files = self.input.files;
      self.totalFiles = files.length;
      self.doneFiles = 0;
      responderObject.statusHandler(files.length, 0);
      self.sendFiles(files);
      self.input.value = "";
    };
  };

  this.sendFiles = function (files) {
    var self = this;

    $.each(files, function (i, file) {
      var ajaxData = new FormData();
      ajaxData.append('image', file);
      $.ajax({
        url: self.upUrl,
        type: 'POST',
        data: ajaxData,
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        success: function (data, textStatus, jqXHR) {
          ++self.doneFiles;
          responderObject.successHandler(data, textStatus, jqXHR);
          responderObject.statusHandler(self.totalFiles, self.doneFiles);
        },
        statusCode: responderObject.statusCodeResponseHandler,
        error: responderObject.errorHandler
      });
    });
  };
};