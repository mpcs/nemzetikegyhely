// This image loading flow control is excellent.
// Makes page loading so much faster, and nicer, it is hard to even explain.

var loadingState = 'loading';
var readyState = 'ready';

var languageSelector;
var titleBar;
var textArea;
var state = readyState;

var selectorPrefix = 'aw_article_translation';
var loadAfterAllSmall = [];

$(function () {
  languageSelector = $('#' + selectorPrefix + '_language_selector');
  titleBar = $('#' + selectorPrefix + '_article_title');
  textArea = $('#' + selectorPrefix + '_article_content');
  var imageBox = $('#actual-images');

  var updateRouteArray = updateRoute.split('/');

  if ('edit' === mode && languageSelector.length > 0) {
    languageSelector.on('change', function () {
      toggleLoadingState();
      updateRouteArray[updateRouteArray.length - 1] = this.value;

      $.ajax({
        url: updateRouteArray.join('/'),
        method: 'GET',
        success: processSuccess,
        statusCode: {
          404: handleNotFoundError
        },
        error: requestErrorHandler
      });
    });
  }

  // async image load for all images
  var allImages = $('li', imageBox);
  var allCount = allImages.length;

  allImages.each(function () {
    var a = $('a', this).eq(0);
    var bigUrl = a.attr('href');
    var imgTag = $('img', a).eq(0);
    var smallUrl = imgTag.attr('data-small-src');
    loadAfterAllSmall.push({url: bigUrl, tag: imgTag});

    imgTag.on('load', function() {
      allCount--;
      if(allCount == 0) loadBigOnes();
    });
    imgTag.attr('src', smallUrl);
  });
});

function loadBigOnes() {
  $(loadAfterAllSmall).each(function() {
    jQueryAsnycLoadImg(this.url, this.tag);
  });
}

function processSuccess(data, textStatus, jqXHR) {
  titleBar.attr('value', data.title);
  textArea.summernote("code", data.content);
  toggleLoadingState();
}

function toggleLoadingState() {
  state = state === readyState ? loadingState : readyState;

  switch (state) {
    case loadingState:
      languageSelector.attr('disabled', 'disabled');
      titleBar.attr('disabled', 'disabled');
      textArea.attr('disabled', 'disabled');
      break;
    default:
      languageSelector.removeAttr('disabled');
      titleBar.removeAttr('disabled');
      textArea.removeAttr('disabled');
  }
}
