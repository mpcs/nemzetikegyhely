var exampleRow = $('#sample-row');
var table = $('table.table');
var noContentSign = $('#no-content');
var submitButton = $('#gallery_image_submit_button');
var inputElement = $('#gallery_image_images');
var statusLabel = $('#status-label');

var Responder = function (asyncLoader) {
  this.exampleRowContents = undefined;

  this.successHandler = function (data) {
    this.exampleRowContents = this.exampleRowContents || exampleRow.html();
    var newContent = '<tr>' + this.exampleRowContents
      .replace(new RegExp('_image_stored_name_', 'g'), data.normal)
      .replace(new RegExp('_replace_this_class_', 'g'), 'image under-load')
      .replace(new RegExp('_image_original_name_', 'g'), data.originalName) + '</tr>';
    var newElement = $(newContent);
    newElement.appendTo('table.table > tbody');

    asyncLoader.init();
    attachDatatableRemoveListeners();

    if ((noContentSign || []).length === 0) return;
    noContentSign.hide();
  };

  this.statusCodeResponseHandler = { 404: requestErrorHandler };
  this.errorHandler =function(jqXHR) {
    requestErrorHandler(jqXHR.responseJSON.message);
  };
  this.statusHandler = function (total, done) {
    statusLabel.html(done + ' / ' + total);
    var disabled = inputElement.attr('disabled');
    if (done < total) {
      if (undefined !== disabled && false !== disabled) return;
      inputElement.attr('disabled', 'disabled');
      submitButton.attr('disabled', 'disabled');
      return;
    }

    inputElement.removeAttr('disabled');
    submitButton.removeAttr('disabled');
  };
};

var asnycConfig = {
  baseUrl: viewRoute,
  imgTagClass: 'image',
  wildcard: 'replace_me',
  willStartLoading: function (resource) {
    $(resource).siblings('.loading').show();
  },
  willDisplayImage: function (holderTag) {
    var parsedHolder = $(holderTag);
    parsedHolder.siblings('.loading').hide();
    parsedHolder.css('visibility', 'visible');
    parsedHolder.removeClass('under-load');
  }
};

var imageLoader = new AsyncImageLoader(asnycConfig);
var responder = new Responder(imageLoader);
var uploadHandler = new AsyncUploadHandler(addRoute, 'gallery_image_images', responder).initialize();

$(function () {
  imageLoader.init();
});