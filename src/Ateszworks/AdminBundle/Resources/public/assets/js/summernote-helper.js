$(function () {

  if('undefined' === typeof config) alert('No config has been provided for summernote!');
  var summernote = 'undefined' !== typeof summernoteTextareaID ? $(summernoteTextareaID) : $('.summernote');
  if (summernote.length > 0) summernote.summernote(config);
});