window.onload = setRemoveEventListeners;

// todo: if u have time remove JQuery as a dependency. Not so important btw.

var removeElementClass = 'attached_image_remove';
var $fileBagId = $('#filebag_id').val();
var $articleId = $('#article_id').val();
var $noImageLabel = document.getElementById('no-image-text');

var canAdvancedUpload = function () {
  var div = document.createElement('div');
  return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
}();

function getResourceUrl(resourceId) {
  return removeUrl.replace('replace_me', resourceId);
}

var removeEventHandler = function (e) {
  e.preventDefault();
  var element = $(this);
  var imageUrl = $(this).attr('data-resource-id');
  $.ajax({
    url: getResourceUrl(imageUrl),
    type: 'DELETE',
    dataType: 'json',
    success: function () {
      var parent = element.parent();
      parent.hide('slow', function () {
        parent.remove();
        var numOfImages = $('li', $actualImagesContainer).length;
        updateLabels(numOfImages > 0);
      });
    },
    error: function (errorResponse) {
      requestErrorHandler(errorResponse.responseJSON.toString());
      $errorLabel.show();
      $loading.hide();
      $uploadingLabel.hide();
    }
  });
};

function setRemoveEventListeners() {
  $('.' + removeElementClass).on('click', removeEventHandler);
}


if (canAdvancedUpload) {
  $('.should-hide').parent().hide();
  $('.should-display').show();

  var $submitButton = $('.submit-button') || $('#aw_article_translation_submit_button');
  var $wrapper = $('.upload-area');
  var $input = $('.upbox_file');
  var $uploadingLabel = $('.upbox_uploading');
  var $doneLabel = $('.upbox_done');
  var $errorLabel = $('.upbox_error');
  var $loading = $('.loading');
  var $actualImagesContainer = document.getElementById('actual-images'); // todo: stop mixin' vanilla and jq
  var $imageInput = document.getElementById('upload_file');
  var $currentLabel = $('.upabox_current');
  var $totalLabel = $('.upbox_total');
  var $statusBox = $('.upbox_status');
  var droppedFiles = undefined;
  var _disabler = function (e) {
    e.preventDefault();
    e.stopPropagation();
  };

  $wrapper.on('drag dragstart dragend dragover dragenter dragleave drop', function (e) {
    e.preventDefault();
    e.stopPropagation();
  })
    .on('dragover dragenter', function () {
      $wrapper.addClass('is-dragover');
    })
    .on('dragleave dragend drop', function () {
      $wrapper.removeClass('is-dragover');
    })
    .on('drop', function (e) {
      droppedFiles = e.originalEvent.dataTransfer.files;
      handleUpload(droppedFiles);
    });

  $input.on('change', function (e) { // when drag & drop is NOT supported
    e.preventDefault();
    handleUpload($imageInput.files);
  });
}

function handleUpload(elements) {
  if (!elements) return;

  var count = elements.length;
  var done = 0;
  $totalLabel.html(count);
  $submitButton.removeClass('btn-success');
  $submitButton.addClass('btn-danger');
  $submitButton.on('click', _disabler);
  $wrapper.addClass('is-uploading');
  $doneLabel.hide();
  $errorLabel.hide();
  $loading.show();
  $uploadingLabel.show();
  $statusBox.show();

  $.each(elements, function (i, file) {
    var ajaxData = new FormData();
    ajaxData.append($input.attr('name'), file);
    ajaxData.append('filebagId', $fileBagId);

    $.ajax({
      url: asyncImageUrl,
      type: 'POST',
      data: ajaxData,
      dataType: 'json',
      cache: false,
      contentType: false,
      processData: false,
      success: function (data) {
        --count;
        updateCountLabel(++done);

        var parts = data.original.split('/');
        var newLi = document.createElement('LI');
        var newA = document.createElement('A');
        var removeA = document.createElement('A');
        var newImg = document.createElement('IMG');
        var removeIcon = document.createElement('I');

        removeIcon.classList.add('fa');
        removeIcon.classList.add('fa-eraser');
        removeA.setAttribute('data-resource-id', parts[parts.length - 1]);
        removeA.setAttribute('href', '#');
        removeA.classList.add(removeElementClass);
        removeA.classList.add('remove-icon');
        removeA.appendChild(removeIcon);
        newA.setAttribute('href', data.original);
        newA.setAttribute('target', '_blank');
        newA.classList.add('image-wrapper');
        newLi.appendChild(newA);
        newLi.appendChild(removeA);
        newA.appendChild(newImg);
        $actualImagesContainer.appendChild(newLi);
        newImg.src = data.small;

        setRemoveEventListeners();
        asyncLoadImg(data.original, newImg);
        updateLabels(true);

        if (count !== 0) return;
        $doneLabel.show();
        $loading.hide();
        $uploadingLabel.hide();
        $imageInput.value = "";
        $submitButton.removeClass('btn-danger');
        $submitButton.addClass('btn-success');
        $submitButton.off('click', _disabler);
      },
      statusCode: {
        404: handleNotFoundError
      },
      error: function (errorResponse) {
        requestErrorHandler((errorResponse.responseJSON || errorResponse.responseText).toString());
        $errorLabel.show();
        $loading.hide();
        $uploadingLabel.hide();
      }
    });
  });

  function updateCountLabel(done) {
    $currentLabel.html(done);
  }
}

function updateLabels(status) {
  if (status) {
    $actualImagesContainer.classList.remove('forced-hidden');
    $noImageLabel.classList.add('hidden');
  } else {
    $actualImagesContainer.classList.add('forced-hidden');
    $noImageLabel.classList.remove('hidden');
  }
}

