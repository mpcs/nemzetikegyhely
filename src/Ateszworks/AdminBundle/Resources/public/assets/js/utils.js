function asyncLoadImg(url, intoElement) {
  var img = new Image();
  img.onload = function () {
    intoElement.src = url;
    intoElement.classList.add('loaded');
  };
  img.src = url;
}

function jQueryAsnycLoadImg(url, intoElement) {
  var img = new Image();
  img.onload = function () {
    intoElement.attr('src', url);
    intoElement.addClass('loaded');
  };
  img.src = url;
}