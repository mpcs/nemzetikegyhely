// Okay enough! Let's create a reusable helper finally.
// yet dependent on jquery
// input:
// {
//   baseUrl: 'baseUrl',
//     imgTagClass: 'imgtagClass',
//   wildcard: 'wildcard',
//   willStartLoading: handler(ofResource),
//   didLoadImage: handler(holderImgTag), // after displayed image
//   willDisplayImage: handler(holderTag), // before displaying img
// }
var AsyncImageLoader = function (config) {
  this.wildCard = config.wildcard || 'resource_url';
  this.baseUrl = config.baseUrl;
  this.tagClass = config.imgTagClass; // class of img tag to load
  this.elements = [];
  this.init = function () { // This you should call.
    this.elements = $('.' + this.tagClass);
    this.startLoading();
  };
  this.startLoading = function () {
    var context = this;
    this.elements.each(function () {
      context.loadOne(this);
    });
  };
  this.loadOne = function (element) {
    var context = this;
    var imageURL = element.getAttribute('data-src');
    if (!imageURL) return false;
    element.setAttribute('data-src', ''); // prevents loading twice
    context.worker(imageURL, element);
  };
  this.worker = function (imageUrl, element) {
    var loadUrl = this.createImageUrl(imageUrl);
    var img = new Image();
    img.onload = function () {
      if (typeof config.willDisplayImage === 'function') config.willDisplayImage(element);
      element.src = loadUrl;
      if (typeof config.didLoadImage === 'function') config.didLoadImage(element);
    };
    if (typeof config.willStartLoading === 'function') config.willStartLoading(element);
    img.src = loadUrl;
  };
  this.createImageUrl = function (resource) {
    return this.baseUrl.replace(this.wildCard, resource);
  };
};