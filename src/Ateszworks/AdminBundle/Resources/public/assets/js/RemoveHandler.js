function RemoveHandler() {
    /**
     * @type {number}
     */
    this.resourceIdToRemove = -1;
    /** @type {string} */
    this.requestUrlStub = "";
    /** @type {string} */
    this.requestUrl = "";
    /** @type {boolean} */
    this.isRouteProcessed = false;
    /** @type {string} */
    this.stubToReplace = "replace_me";
    /** @type {boolean} */
    this.pullBreaks = false;

    this.setRequestUrl = function (url) {
        this.requestUrlStub = url;
        this.writeLog('Request URL stub has been set to: ' + this.requestUrlStub);
        this.createRequestUrl();
    };

    this.processRequestURL = function () {
        if (this.isRouteProcessed) return;
        if (!this.requestUrlStub) {
            this.pullBreaks = true;
            alert('No request URL has been specified!');
            return;
        }
        this.requestUrlStub = this.requestUrlStub.replace(this.stubToReplace, "");
        this.writeLog('Request URL stub: ' + this.requestUrlStub);
    };

    this.createRequestUrl = function () {
        this.processRequestURL();
        this.requestUrl = this.requestUrlStub + this.resourceIdToRemove;
        this.writeLog('Request URL: ' + this.requestUrl);
    };

    this.setResourceIdToRemove = function (id) {
        if (!id) {
            alert('No id was given for removing!');
            this.pullBreaks = true;
            return;
        }

        this.resourceIdToRemove = id;
        this.writeLog('Resource ID has been set to: ' + id);
    };

    this.triggerRequest = function () {
        if (this.pullBreaks) {
            this.pullBreaks = false;
            this.writeLog('Request prevented.');
            return;
        }

        $.ajax({
            url: this.requestUrl,
            method: 'DELETE',
            success: this.successfulResponseHandler,
            statusCode: {
                200: this.handleSuccessfulResponse,
                404: this.handleNotFoundError
            },
            error: this.requestErrorHandler
        });
    };

    // Should be used as a callback for success from outside.
    this.successfulResponseHandler = function (data, textStatus, jqXHR) {
        // todo: find a solution for being able to call writeLog ! jquery messes things up
        console.log('Successful AJAX request!');
        console.log(data);
        console.log(textStatus);
        console.log(jqXHR);
    };
    this.handleNotFoundError = function () {
        this.writeLog('Response code: 404.');
    };
    this.handleSuccessfulResponse = function () {
        this.writeLog('Response code: 200');
    };
    this.requestErrorHandler = function (jqXHR, textStatus, errorThrown) {
        // todo: create a function for error handling by sending an ajax request to a logger service
        console.log('Error while making AJAX request!');
        console.log(jqXHR);
        console.log(textStatus);
        console.log(errorThrown);
    };

    this.writeLog = function (msg) {
        console.log('[RemoveHandler] ' + msg);
    };
}

function createRemoveHandler() {
    return new RemoveHandler();
}