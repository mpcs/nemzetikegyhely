var removeHandler = createRemoveHandler();
var removeConfirmationDiv = $('#remove-confirmation');
var clickedElement = undefined;

removeHandler.handleSuccessfulResponse = function () {
  removeConfirmationDiv.modal('hide');
  removeConfirmationDiv.on('hidden.bs.modal', function () {
    $('#success-modal').modal('show');
    clickedElement.closest('tr').remove();
  });
};

removeHandler.handleNotFoundError = function () {
  removeConfirmationDiv.modal('hide');
  removeConfirmationDiv.on('hidden.bs.modal', function () {
    $('#404modal').modal('show');
  });
};

removeHandler.requestErrorHandler = function () {
  removeConfirmationDiv.modal('hide');
  removeConfirmationDiv.on('hidden.bs.modal', function () {
    $('#error-modal').modal('show');
  });
};

function attachDatatableRemoveListeners() {
  $('.aw-remove-button').on('click', function () {
    clickedElement = $(this);
    var resourceId = clickedElement.attr('data-resource-id');
    removeHandler.setResourceIdToRemove(resourceId);
    removeHandler.setRequestUrl(removeRoute);
  });
}

attachDatatableRemoveListeners();

removeConfirmationDiv.on('click', '.btn-danger', function () {
  removeHandler.triggerRequest();
});

