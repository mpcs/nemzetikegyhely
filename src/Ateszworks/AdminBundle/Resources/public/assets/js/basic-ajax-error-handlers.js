// todo: wrap in object

function handleSuccessfulResponse() {
  $('#success-modal').modal('show');
}

function handleNotFoundError() {
  $('#404modal').modal('show');
}

function requestErrorHandler(message) {
  var errorModal = $('#error-modal');
  if (message) $('.modal-body', errorModal).html(message);
  errorModal.modal('show');
}

function processSuccessfulRequest(data, textStatus, jqXHR) {
  console.log(data);
  console.log(textStatus);
  console.log(jqXHR);
}