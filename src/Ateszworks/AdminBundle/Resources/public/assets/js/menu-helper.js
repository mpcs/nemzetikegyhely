Element.prototype.vanillaAddClass = function (newClass) {
  if (this.className.indexOf(newClass) !== -1) return;
  var classes = this.className.split(' ');
  classes.push(newClass);
  this.className = classes.join(' ').trim();
};

Element.prototype.vanillaRemoveClass = function(existingClass) {
	if(this.className.indexOf(existingClass) === -1) return;
	var classes = this.className.split(' ');
	var index = classes.indexOf(existingClass);
	classes.splice(index, 1);
	this.className = classes.join(' ').trim();
};

$(function () {

  var toggleButton = $('#pointed_type_switcher');
  var urlFormGroup = $('div.menu-form-url-target-group');
  var urlLabel = $('.switch-text-url');
  var resourceLabel = $('.switch-text-resource');
  var resourceFormGroup = $('div.menu-form-resource-target-group');
  var resourceTextField = $('#target_resource-selector');
  var resourceResoultsUL = document.getElementById('target_resource-values'); // Gettin' Vanillaaa
  var noContentInfo = document.getElementById('target_resource-hidden_text');
  var selectedResourceTypeField = $('#target_resource_type');
  var selectedResourceIdField = $('#target_resource_id');
  var resultDiv = $('.results').eq(0);

  var effectSpeed = 300;
  var ajaxUrl = function (queryString) {
    return _ajaxUrl.replace('name_fraction', queryString);
  };

  // Functionality
  setSwitchState();

  // Event listeners
  toggleButton.on('change', setSwitchState);
  resourceTextField.on('keyup', function () {
    resultDiv.show('slow');
    var queryString = resourceTextField.val();
    if (queryString.length < 1) return;

    $.ajax({
      url: ajaxUrl(queryString),
      method: 'GET',
      success: processResponse,
      statusCode: {
        404: handleNotFoundError
      },
      error: requestErrorHandler
    });
  });

  // Animations
  function handleFormGroupAnimation(forSwitchState) {
    forSwitchState ? urlFormGroup.hide(effectSpeed) : urlFormGroup.show(effectSpeed);
    !forSwitchState ? resourceFormGroup.hide(effectSpeed) : resourceFormGroup.show(effectSpeed);
  }

  function handleLabelAnimation(forSwitchState) {
    var elementToHide = forSwitchState ? urlLabel : resourceLabel;
    var elementToShow = forSwitchState ? resourceLabel : urlLabel;

    elementToHide.fadeOut(effectSpeed, function () {
      elementToShow.fadeIn(effectSpeed);
    });
  }

  // Work
  function setSwitchState() {
    var isOn = toggleButton.is(':checked');
    handleFormGroupAnimation(isOn);
    handleLabelAnimation(isOn);
  }

  function processResponse(JSONdata, textStatus, jqXHR) {
    removeCurrentList();

    if (JSONdata.length === 0) {
      resourceResoultsUL.vanillaAddClass('hidden');
      noContentInfo.vanillaRemoveClass('hidden');
      return;
    }

    resourceResoultsUL.vanillaRemoveClass('hidden');
    noContentInfo.vanillaAddClass('hidden');

    for (var key in JSONdata) {
      if (!JSONdata.hasOwnProperty(key)) return;

      var alreadySelectedResourceType = selectedResourceTypeField.val();
      var alreadySelectedResourceId = selectedResourceIdField.val();
      var resources = JSONdata[key];

      for (var index = 0; index < resources.length; ++index) {
        var resourceObject = resources[index];
        var liElement = document.createElement('LI');
        liElement.setAttribute('class', 'resource-list-clickable');
        liElement.setAttribute('data-resource-type', key);
        liElement.setAttribute('data-resource-id', resourceObject['id']);
        liElement.innerHTML = resourceObject['string'];
        resourceResoultsUL.appendChild(liElement);

        if (!alreadySelectedResourceId) continue;
        if (key === alreadySelectedResourceType &&
          parseInt(resourceObject['id']) === parseInt(alreadySelectedResourceId))
          liElement.classList.add('selected');
      }
    }

    setEventListeners();
  }

  function removeCurrentList() {
    while (resourceResoultsUL.firstChild) {
      resourceResoultsUL.removeChild(resourceResoultsUL.firstChild);
    }
  }

  function setEventListeners() {
    var resourceClickableLis = $('.resource-list-clickable');
    resourceClickableLis.on('click', function () {
      var clickedElement = $(this);
      var resourceType = clickedElement.attr('data-resource-type');
      var resourceId = clickedElement.attr('data-resource-id');
      clickedElement.addClass('selected');

      selectedResourceTypeField.val(resourceType);
      selectedResourceIdField.val(resourceId);

      resourceClickableLis.each(function () {
        var parsedElement = $(this);
        if (!parsedElement.is(clickedElement)) parsedElement.removeClass('selected');

        if (parsedElement.hasClass('selected')) return;
        parsedElement.hide('slow');
      });
    });
  }
});