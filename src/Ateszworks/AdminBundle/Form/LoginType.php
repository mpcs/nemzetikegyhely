<?php

namespace Ateszworks\AdminBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class LoginType
 * @package Ateszworks\AdminBundle\Form
 */
class LoginType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'constraints' => new NotBlank(),
                'label' => 'login.username',
                'attr' => [
                    'class' => 'form-control m-input',
                    'style' => 'margin-bottom: 20px;',
                ]
            ])
            ->add('password', PasswordType::class, [
                'constraints' => new NotBlank(),
                'label' => 'login.password',
                'attr' => ['class' => 'form-control m-input'],
            ])
            ->add('login', SubmitType::class, [
                'label' => 'login.login',
                'attr' => [
                    'class' => 'btn btn-success',
                    'style' => 'margin: 20px auto;'
                ]
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        // gyász
        $resolver->setDefaults([
            'csrf_protection' => true,
            'csrf_field_name' => '_csrf_token'
        ]);
    }

}