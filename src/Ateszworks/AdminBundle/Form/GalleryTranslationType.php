<?php

namespace Ateszworks\AdminBundle\Form;


use App\Entity\GalleryTranslation;
use Ateszworks\MultilangContentBundle\ContentInterface\LanguageProviderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class GalleryTranslationType
 * @package Ateszworks\AdminBundle\Form
 */
class GalleryTranslationType extends AbstractType
{

    /* @var LanguageProviderInterface */
    private $languageProvider;

    /**
     * GalleryTranslationType constructor.
     * @param LanguageProviderInterface $languageProvider
     */
    public function __construct(LanguageProviderInterface $languageProvider)
    {
        $this->languageProvider = $languageProvider;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $this->formInitializer($event);
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => GalleryTranslation::class]);
    }

    /**
     * @param FormEvent $event
     */
    private function formInitializer(FormEvent $event): void
    {
        /* @var $translation GalleryTranslation */
        $form = $event->getForm();
        $translation = $event->getData();
        $translation = $translation->getLang()
            ->getTranslationForLanguage($this->languageProvider->getCurrentLanguage());

        $form->add('content', TextType::class, [
            'label' => $translation->getLangName(),
            'required' => false,
            'property_path' => 'title',
            'attr' => [
                'placeholder' => 'form.gallery.provide_title',
                'class' => 'form-control m-input'
            ]
        ]);
    }

}