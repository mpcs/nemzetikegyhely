<?php

namespace Ateszworks\AdminBundle\Form;


use App\Entity\CarouselPageTranslation;
use Ateszworks\AdminBundle\Form\DataTransformer\LangcodeToEntityTransformer;
use Ateszworks\MultilangContentBundle\ContentInterface\LanguageProviderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CarouselTranslationType
 * @package App\Ateszworks\AdminBundle\Form
 */
class CarouselTranslationType extends AbstractType
{

    /**
     * @var LangcodeToEntityTransformer
     */
    protected $transformer;

    /* @var LanguageProviderInterface */
    private $languageProvider;

    /**
     * CarouselTranslationType constructor.
     * @param LanguageProviderInterface $languageProvider
     */
    public function __construct(LanguageProviderInterface $languageProvider)
    {
        $this->languageProvider = $languageProvider;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $this->formInitializer($event);
        });
    }

    /**
     * @param FormEvent $event
     */
    private function formInitializer(FormEvent $event): void
    {
        /* @var $data CarouselPageTranslation */
        $form = $event->getForm();
        $data = $event->getData();
        $translation = $data->getLang()->getTranslationForLanguage($this->languageProvider->getCurrentLanguage());
        $form->add('content', TextareaType::class, [
            'label' => $translation->getLangName(),
            'required' => false,
            'property_path' => 'content',
            'attr' => [
                'placeholder' => 'form.please_provide_content',
                'class' => 'summernote'
            ]
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => CarouselPageTranslation::class]);
    }


}