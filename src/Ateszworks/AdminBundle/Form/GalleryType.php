<?php

namespace Ateszworks\AdminBundle\Form;


use App\Entity\Gallery;
use App\Entity\GalleryTranslation;
use App\Utils\MpcsUtils;
use Ateszworks\MultilangContentBundle\ContentInterface\LanguageProviderInterface;
use Ateszworks\MultilangContentBundle\Entity\LangEntity;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class GalleryType
 * @package App\Ateszworks\AdminBundle\Form
 */
class GalleryType extends AbstractType
{

    /* @var LanguageProviderInterface */
    private $languageProvider;

    /**
     * GalleryType constructor.
     * @param LanguageProviderInterface $languageProvider
     */
    public function __construct(LanguageProviderInterface $languageProvider)
    {
        $this->languageProvider = $languageProvider;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $now = new DateTime();
        $builder
            ->add('translations', CollectionType::class, [
                'label' => 'form.gallery.texts',
                'entry_type' => GalleryTranslationType::class,
                'entry_options' => ['label' => false],
                'property_path' => 'translations',
                'constraints' => [new Assert\Valid()]
            ])
            ->add('published', DateType::class, [
                'label' => 'form.gallery.published',
                'required' => false,
                'property_path' => 'published',
                'widget' => 'single_text',
                'attr' => ['class' => 'flatpicker']
            ])
            ->add('submit_button', SubmitType::class, [
                'label' => 'form.submit',
                'attr' => [
                    'class' => 'btn btn-success',
                    'style' => 'margin: 20px auto;'
                ]
            ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $this->formInitializer($event);
        });
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $this->preSubmitEventListener($event);
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Gallery::class]);
    }

    /**
     * @param FormEvent $event
     */
    private function formInitializer(FormEvent $event): void
    {
        /* @var $gallery Gallery */
        $gallery = $event->getData();
        $languages = $this->languageProvider->getAllAvailableLanguages();

        foreach ($languages as $lang) {
            if ($gallery->getTranslations() &&
                $gallery->getTranslationForLanguage($lang)) continue;

            $currentTranslations = $gallery->getTranslations()
                ?? $gallery->setTranslations(new ArrayCollection())->getTranslations();
            $currentTranslations->add((new GalleryTranslation())->setLang($lang));
        }
    }

    /**
     * @param FormEvent $event
     */
    private function preSubmitEventListener(FormEvent $event): void
    {
        $data = $event->getData();
        $data['published'] = isset($data['published']) && $data['published']
            ? $data['published'] : (new DateTime)->format('Y-m-d');
        $event->setData($data);
    }

}