<?php

namespace Ateszworks\AdminBundle\Form;

use Ateszworks\AdminBundle\Controller\MenuController;
use Ateszworks\MultilangContentBundle\ContentInterface\LanguageProviderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotEqualTo;

/**
 * Class MenuFormType
 * @package Ateszworks\AdminBundle\Form
 * todo: use it later, right now there is no time for new things to learn!
 */
class MenuFormType extends AbstractType
{

    /**
     * @var $langProvider LanguageProviderInterface
     */
    private $langProvider;

    /**
     * MenuFormType constructor.
     * @param LanguageProviderInterface $languageProvider
     */
    public function __construct(LanguageProviderInterface $languageProvider)
    {
        $this->langProvider = $languageProvider;
    }


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titles', CollectionType::class, [
                'entry_type' => TextType::class,
                'entry_options' => [
                    'constraints' => [
                        new NotBlank(),
                        new NotEqualTo(MenuController::UNPROVIDED_VALUE_STRING)
                    ]
                ]
            ]);

        // todo: finish implementation
//        $langs = $this->langProvider->getAllAvailableLanguages();
//        foreach ($langs as $lang) {
//            $builder->add();
//        }
            $builder->add('save', SubmitType::class);
    }

}