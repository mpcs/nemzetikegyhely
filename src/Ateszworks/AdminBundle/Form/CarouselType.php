<?php

namespace Ateszworks\AdminBundle\Form;


use App\Entity\CarouselPage;
use App\Entity\CarouselPageTranslation;
use Ateszworks\MultilangContentBundle\ContentInterface\LanguageProviderInterface;
use Ateszworks\MultilangContentBundle\Entity\LangEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class CarouselType
 * @package Ateszworks\AdminBundle\Form
 */
class CarouselType extends AbstractType
{

    /* @var LanguageProviderInterface */
    private $languageProvider;

    /**
     * CarouselType constructor.
     * @param LanguageProviderInterface $languageProvider
     */
    public function __construct(LanguageProviderInterface $languageProvider)
    {
        $this->languageProvider = $languageProvider;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('POST')
            ->add('carousel_image', FileType::class, [
                'required' => false,
                'label' => 'form.carousel.image',
                'property_path' => 'image',
                'mapped' => false
            ])
            ->add('translations', CollectionType::class, [
                'label' => 'form.carousel.texts',
                'entry_type' => CarouselTranslationType::class,
                'entry_options' => ['label' => false],
                'property_path' => 'translations',
                'constraints' => [new Assert\Valid()]
            ])
            ->add('weight', IntegerType::class, [
                'property_path' => 'weight',
                'constraints' => [new Assert\Type("integer")]
            ])
            ->add('submit_button', SubmitType::class, [
                'label' => 'form.submit',
                'attr' => [
                    'class' => 'btn btn-success',
                    'style' => 'margin: 20px auto;'
                ]
            ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $this->formInitializer($event);
        });
    }

    /**
     * @param FormEvent $event
     */
    private function formInitializer(FormEvent $event): void
    {
        /* @var $allLanguages array */
        /* @var $carouselPageEntity CarouselPage */
        /* @var $carouselTranslations Collection */
        $carouselPageEntity = $event->getData();
        $carouselTranslations = $carouselPageEntity->getTranslations();
        $allLanguages = $this->languageProvider->getAllAvailableLanguages();

        if (!$carouselTranslations || count($carouselTranslations->toArray()) < count($allLanguages)) {
            $missingLanguages = array_filter($allLanguages,
                function (LangEntity $langEntity) use ($carouselPageEntity) {
                    return !$carouselPageEntity->getTranslations() ||
                        !$carouselPageEntity->getTranslationForLanguage($langEntity);
                });

            $currentTranslations = $carouselPageEntity->getTranslations()
                ?? $carouselPageEntity->setTranslations(new ArrayCollection())->getTranslations();
            foreach ($missingLanguages as $language) {
                $currentTranslations->add((new CarouselPageTranslation())->setLang($language));
            }
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => CarouselPage::class]);
    }

}