<?php

namespace Ateszworks\AdminBundle\Form;


use App\Entity\GalleryImage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class GalleryImageType
 * @package App\Ateszworks\AdminBundle\Form
 */
class GalleryImageType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('images', FileType::class, [
                'multiple' => true,
                'label' => 'form.gallery.manage_images.upload',
                'mapped' => false,
                'required' => false
            ])
            ->add('submit_button', SubmitType::class, [
                'label' => 'form.submit',
                'attr' => [
                    'class' => 'btn btn-success',
                    'style' => 'margin: 20px auto;'
                ]
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => GalleryImage::class]);
    }

}