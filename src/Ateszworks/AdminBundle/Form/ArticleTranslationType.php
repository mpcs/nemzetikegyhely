<?php

namespace Ateszworks\AdminBundle\Form;

use Ateszworks\AdminBundle\Form\DataTransformer\LangcodeToEntityTransformer;
use Ateszworks\MultilangContentBundle\ContentInterface\LanguageProviderInterface;
use Ateszworks\MultilangContentBundle\Entity\ArticleTranslationEntity;
use Error;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class ArticleTranslationType
 * @package Ateszworks\AdminBundle\Form
 */
class ArticleTranslationType extends AbstractType
{

    /* @var LanguageProviderInterface */
    private $languageProvider;

    /* @var LangcodeToEntityTransformer */
    private $transformer;

    /**
     * ArticleTranslationType constructor.
     * @param LanguageProviderInterface $languageProvider
     * @param LangcodeToEntityTransformer $entityTransformer
     */
    public function __construct(LanguageProviderInterface $languageProvider, LangcodeToEntityTransformer $entityTransformer)
    {
        $this->languageProvider = $languageProvider;
        $this->transformer = $entityTransformer;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // TODO: Refactor
        $langEntities = $this->languageProvider->getAllAvailableLanguages();
        if (!$langEntities) throw new Error('No languages provided.', 500);
        $currentLanguage = $this->languageProvider->getCurrentLanguage();

        $choiceArray = [];
        foreach ($langEntities as $langEntity) {
            $langName = $langEntity->getTranslationForLanguage($currentLanguage)->getLangName();
            $choiceArray[$langName] = $langEntity->getCode();
        }

        $builder
            ->setMethod('POST')
            ->add('language_selector', ChoiceType::class, [
                'choices' => $choiceArray,
                'label' => 'form.article.select_language',
                'constraints' => [new NotBlank()],
                'attr' => ['class' => 'form-control'],
                'property_path' => 'formLang'
            ])
            ->add('article_title', TextType::class, [
                'label' => 'form.article.title',
                'attr' => [
                    'placeholder' => 'form.please_provide_title',
                    'class' => 'form-control m-input'
                ],
                'constraints' => [
                    new NotBlank(), // todo: solve uniqueness problem
//                    new UniqueEntity([
//                        'fields' => ['title'],
//                        'entityClass' => ArticleTranslationEntity::class,
//                        'message' => 'validaton.already_taken'
//                    ])
                ],
                'property_path' => 'title'
            ])
            ->add('article', ArticleType::class)
            ->add('article_content', TextareaType::class, [
                'label' => 'form.article.content',
                'attr' => [
                    'placeholder' => 'form.please_provide_content',
                    'id' => 'summernote'
                ],
                'constraints' => [new NotBlank()],
                'property_path' => 'content'
            ])
            ->add('submit_button', SubmitType::class, [
                'label' => 'form.submit',
                'attr' => [
                    'class' => 'btn btn-success',
                    'style' => 'margin: 20px auto;'
                ]
            ]);

        $builder->get('language_selector')
            ->addModelTransformer($this->transformer);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => ArticleTranslationEntity::class]);
    }

    /**
     * @return null|string
     */
    public function getBlockPrefix()
    {
        return 'aw_article_translation';
    }
}
