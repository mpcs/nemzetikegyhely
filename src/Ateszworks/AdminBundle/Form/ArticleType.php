<?php

namespace Ateszworks\AdminBundle\Form;


use App\Entity\ArticleAttachedImage;
use App\Utils\MpcsUtils;
use Ateszworks\MultilangContentBundle\Entity\ArticleEntity;
use DateTime;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ArticleType
 * @package Ateszworks\AdminBundle\Form
 */
class ArticleType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('listable', CheckboxType::class, [ // todo: embed in in a normal article form!
                'label' => 'form.article.is_listed',
                'mapped' => true,
                'required' => false,
                'property_path' => 'isListed'
            ])
            ->add('published', DateType::class, [
                'label' => 'form.article.published',
                'mapped' => true,
                'required' => false,
                'property_path' => 'published',
                'widget' => 'single_text',
                'attr' => ['class' => 'flatpicker']
            ])
            ->add('article_image', FileType::class, [
                'required' => false,
                'label' => 'form.article.image',
                'mapped' => false
            ])
            ->add('attached_images', FileType::class, [
                'multiple' => true,
                'label' => 'form.article.attached_images',
                'data_class' => ArticleAttachedImage::class,
                'mapped' => false,
                'required' => false
            ]);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $this->submitEventListener($event);
        });
    }

    /**
     * @return null|string
     */
    public function getBlockPrefix()
    {
        return 'aw_article';
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => ArticleEntity::class]);
    }

    // MARK: - Private
    /**
     * @param FormEvent $event
     */
    private function submitEventListener(FormEvent $event): void
    {
        $data = $event->getData();
        $data['published'] = isset($data['published']) && $data['published']
            ? $data['published'] : (new DateTime)->format('Y-m-d');
        $event->setData($data);
    }
}