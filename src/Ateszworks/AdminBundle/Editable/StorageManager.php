<?php

namespace Ateszworks\AdminBundle\Editable;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\Entity;

class StorageManager implements StorageLayerAdapter
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var Entity
     */
    private $entity;

    /**
     * @var EntityRepository
     */
    private $entityRepository;

    /**
     * StorageManager constructor.
     * @param EntityManager $entityManager
     * @param Entity $entity
     * @param EntityRepository $entityRepository
     */
    public function __construct(EntityManager $entityManager, Entity $entity, EntityRepository $entityRepository)
    {
        $this->entityManager = $entityManager;
        $this->entity = $entity;
        $this->entityRepository = $entityRepository;
    }


    /**
     * @param int $id
     * @return mixed
     */
    public function getById(integer $id)
    {
        return $this->entityManager->getRepository($this->entity)->findOneBy(['id' => $id]);
    }

    /**
     * Get contents in packs.
     * @param int $from
     * @param int $amount
     * @param array $criteria
     * @param array $orderBy
     * @param string $order
     * @return mixed
     */
    public function getPack(integer $from, integer $amount, array $criteria = array(), array $orderBy, string $order = 'DESC')
    {
        return $this->entityManager->getRepository($this->entity)->findBy($criteria, $orderBy, $amount, $from);
    }

    /**
     * @param $content
     * @return bool
     */
    public function saveContent(AdminRepresentable $content)
    {
        // TODO: Implement saveContent() method.
    }

    /**
     * @param AdminRepresentable $content
     * @return mixed
     */
    public function updateContent(AdminRepresentable $content)
    {
        // TODO: Implement updateContent() method.
    }

    /**
     * @param AdminRepresentable $content
     * @return mixed
     */
    public function removeContent(AdminRepresentable $content)
    {
        // TODO: Implement removeContent() method.
    }
}