<?php

namespace Ateszworks\AdminBundle\Editable;

use Doctrine\ORM\Mapping\Entity;

/**
 * Interface AdminRepresentable
 * @package Ateszworks\AdminBundle\Editable
 */
interface AdminRepresentable
{
    /**
     * @return Entity
     */
    public function getEntity();

    /**
     * @return StorageLayerAdapter
     */
    public function getStorageLayer();

}