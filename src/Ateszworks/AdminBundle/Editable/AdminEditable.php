<?php

namespace Ateszworks\AdminBundle\Editable;


use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Validator\ValidatorBuilder;

interface AdminEditable extends AdminRepresentable
{
    /**
     * @return FormBuilder
     */
    public function getFormBuilder();

    /**
     * @return ValidatorBuilder
     */
    public function getValidator();
}