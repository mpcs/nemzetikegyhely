<?php

namespace Ateszworks\AdminBundle\Editable;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Entity;

class AbstractAdminRepresentable implements AdminRepresentable
{
    /**
     * @var Entity
     */
    private $entity;

    /**
     * @var EntityManager
     */
    private $storageLayer;

    /**
     * @return Entity
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @return StorageLayerAdapter
     */
    public function getStorageLayer()
    {
        // TODO: Implement getStorageLayer() method.
    }
}