<?php

namespace Ateszworks\AdminBundle\Editable;


/**
 * Interface StorageLayerAdapter
 * @package Ateszworks\AdminBundle\Editable
 */
/**
 * Interface StorageLayerAdapter
 * @package Ateszworks\AdminBundle\Editable
 */
interface StorageLayerAdapter
{

    /**
     * @param int $id
     * @return mixed
     */
    public function getById(integer $id);

    /**
     * Get contents in packs.
     * @param int $from
     * @param int $amount
     * @param array $criteria
     * @param array $orderBy
     * @param string $order
     * @return mixed
     */
    public function getPack(integer $from, integer $amount, array $criteria, array $orderBy, string $order);

    /**
     * @param $content
     * @return bool
     */
    public function saveContent(AdminRepresentable $content);

    /**
     * @param AdminRepresentable $content
     * @return mixed
     */
    public function updateContent(AdminRepresentable $content);

    /**
     * @param AdminRepresentable $content
     * @return mixed
     */
    public function removeContent(AdminRepresentable $content);

}