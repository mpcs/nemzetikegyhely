<?php

namespace Ateszworks\AdminBundle\Utils;

use App\Manager\MPCSFileManager;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class SessionFileBag
 */
class SessionFileBag
{

    /**
     * Name used for storing the bag in session.
     */
    CONST SESSION_BAG_NAME = 'mpcs_file_bag';

    /* @var Session */
    private $session;

    /* @var MPCSFileManager */
    private $fileManager;

    /* @var array */
    private $files = [];

    /**
     * MPCSSessionFileBag constructor.
     * @param Session $session
     * @param MPCSFileManager $fileManager
     */
    public function __construct(Session $session, MPCSFileManager $fileManager)
    {
        $this->session = $session;
        $this->fileManager = $fileManager;
        $this->restoreStateFromSession();
    }

    /**
     * @param string $bagId
     * @param string $url
     */
    public function addToBag(string $bagId, string $url): void
    {
        if (!array_key_exists($bagId, $this->files)) $this->files[$bagId] = [];
        $this->files[$bagId][] = $url;
        $this->updateSession();
    }

    /**
     * @param string $bagId
     * @return array
     */
    public function returnAsFiles(string $bagId): array
    {
        if (!isset($this->files[$bagId]) || count($this->files[$bagId]) == 0) return [];

        $result = array_map(function (string $name) {
            $file = new File($this->fileManager->getFullFileURL($name), true);
            return new UploadedFile($file->getRealPath(), $file->getFilename(), $file->getMimeType(),
                $file->getSize(), null, true);
        }, isset($this->files[$bagId]) ? $this->files[$bagId] : []);

        $this->emptyPartialBag($bagId);
        return $result;
    }

    // Events

    /**
     * @param Event $_
     */
    public function onFilebagShouldClear(Event $_): void
    {
        $this->emptyBag();
    }

    /**
     * Nukes the bag, and temp files.
     */
    public function emptyBag(): void
    {
        foreach ($this->files as $bag) foreach ($bag as $file) $this->fileManager->removeFromTemp($file);
        $this->files = [];
        $this->updateSession();
    }

    /**
     * @param string $id
     */
    public function emptyPartialBag(string $id): void
    {
        unset($this->files[$id]);
        $this->updateSession();
    }

    /**
     * @param string $imageUrl
     * @param string $bagId
     * @return bool
     */
    public function has(string $imageUrl, string $bagId): bool
    {
        if (!isset($this->files[$bagId])) return false;
        return in_array($imageUrl, $this->files[$bagId]);
    }

    /**
     * @param string $imageUrl
     * @param string $bagId
     */
    public function removeFromBag(string $imageUrl, string $bagId = null): void
    {
        if ($bagId) {
            for ($i = 0; $i < count($this->files[$bagId]); $i++) {
                if (!$this->files[$bagId][$i] == $imageUrl) continue;

                unset($this->files[$bagId][$i]);
                break;
            }
        } else {
            foreach ($this->files as &$bag) {
                foreach($bag as $key => $url) {
                    if ($url != $imageUrl) continue;
                    unset($bag[$key]);
                    break;
                }
            }
        }

        $this->fileManager->removeFromTemp($imageUrl);
        $this->updateSession();
    }

    /**
     * Restores previous state from session.
     */
    private function restoreStateFromSession(): void
    {
        $this->files = $this->session->get(self::SESSION_BAG_NAME, []);
    }

    /**
     * Updates bag in session.
     */
    private function updateSession(): void
    {
        $this->session->set(self::SESSION_BAG_NAME, $this->files);
    }
}