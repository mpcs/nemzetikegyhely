<?php

namespace Ateszworks\AdminBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class ShouldClearFileBagEvent extends Event
{
    CONST NAME = "filebag.should.clear";
}