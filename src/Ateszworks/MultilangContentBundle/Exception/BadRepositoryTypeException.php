<?php

namespace Ateszworks\MultilangContentBundle\Exception;

/**
 * Class BadRepositoryTypeException
 * @package Ateszworks\MultilangContentBundle\Exception
 */
class BadRepositoryTypeException extends \Exception
{

}