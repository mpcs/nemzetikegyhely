<?php

namespace Ateszworks\MultilangContentBundle\Provider;

use Ateszworks\MultilangContentBundle\ContentInterface\ContentProviderInterface;
use Ateszworks\MultilangContentBundle\ContentInterface\ContentTranslation;
use Ateszworks\MultilangContentBundle\ContentInterface\LanguageProviderInterface;
use Ateszworks\MultilangContentBundle\ContentInterface\Translatable;
use Ateszworks\MultilangContentBundle\Entity\LangEntity;
use Ateszworks\MultilangContentBundle\Entity\TranslationFilterableEntity;
use Ateszworks\MultilangContentBundle\Exception\BadRepositoryTypeException;
use Ateszworks\MultilangContentBundle\Repository\TranslatableResourceRepository;
use Ateszworks\MultilangContentBundle\Repository\TranslationQueryableRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ContentProvider
 * It is strongly dependent on Doctrine.
 * @package Ateszworks\MultilangContentBundle\Provider
 * This service should be able to serve you appropriate content by id or
 * limits even with implicit language settings.
 */
class ContentProvider implements ContentProviderInterface
{

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var array
     */
    protected $queryCriteria = [];

    /**
     * Works as default ordering direction if not overridden.
     * @var array
     */
    protected $order = ['id' => 'DESC'];

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var \stdClass Some kinda Entity
     */
    protected $managedContentType;

    /**
     * @var \stdClass entity
     */
    protected $managedContentTranslationType;

    /**
     * @var TranslatableResourceRepository
     */
    protected $repositoryReference;

    /**
     * @var TranslationQueryableRepository
     */
    protected $translationRepostoryReference;

    /**
     * @var $currentLang LangEntity
     */
    protected $currentLang;

    /**
     * @var $fallbackLang LangEntity
     */
    protected $fallbackLang;

    /**
     * @var LanguageProviderInterface $languageProvider
     */
    protected $languageProvider;

    /**
     * ContentProvider constructor.
     * @param Session $session
     * @param EntityManager $entityManager
     * @param LanguageProviderInterface $languageProvider
     */
    public function __construct(Session $session, EntityManager $entityManager, LanguageProviderInterface $languageProvider)
    {
        $this->session = $session;
        $this->entityManager = $entityManager;
        $this->languageProvider = $languageProvider; // todo: consider decoupling langprov. and this, use managers as a bridge
        $this->setupLangs();
    }

    /**
     * @param $entity
     */
    public function setManagedContentType($entity): void
    {
        $this->managedContentType = $entity;
        $this->repositoryReference = $this->entityManager->getRepository($this->managedContentType);
    }

    /**
     * @param $entity
     * @throws BadRepositoryTypeException
     */
    public function setManagedContentTranslationType($entity): void
    {
        $this->managedContentTranslationType = $entity;
        $this->translationRepostoryReference = $this->entityManager->getRepository($this->managedContentTranslationType);
        $this->checkTranslationRepositoryType();
        $this->translationRepostoryReference->setOrder($this->order);
    }

    /**
     * @param array $array
     */
    public function setCriteriaArray(array $array = array()): void
    {
        $this->queryCriteria = $array;
    }

    /**
     * @param array $order
     */
    public function setOrderingForPackedRequest(array $order): void
    {
        $this->order = $order;
    }

    /**
     * @param int $id
     * @return mixed|null|object
     */
    public function getEntityById(int $id)
    {
        return $this->repositoryReference->findOneBy(['id' => $id]);
    }

    /**
     * @param array|null $order
     * @return array
     */
    public function getAll(array $order = null): array
    {
        return $this->repositoryReference->findBy($this->queryCriteria, $order ? $order : $this->order);
    }

    /**
     * @param int $from
     * @param int $amount
     * @param array $order
     * @return array|mixed
     */
    public function getEntitiesInPack(int $from, int $amount, array $order)
    {
        return $this->repositoryReference->findBy($this->queryCriteria, $order ? $order : $this->order, $amount, $from);
    }

    /**
     * @param $entity
     * @return void
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveByEntity($entity): void
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }

    /**
     * @param $entity
     * @return void
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateByEntity($entity): void
    {
        $this->saveByEntity($entity);
    }

    /**
     * @param $entity
     * @return void
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeByEntity($entity): void
    {
        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }

    /**
     * @return TranslatableResourceRepository
     * @throws \Exception
     */
    public function getRepositoryReference(): TranslatableResourceRepository
    {
        if (!$this->managedContentType) throw new \Exception('No managed content type is given!');

        return $this->repositoryReference; // thats an entity repository tho...
    }

    /**
     * @param int $id
     * @return ContentTranslation
     */
    public function getTranslationEntityForId(int $id): ContentTranslation
    {
        /**
         * @var $entity Translatable
         */
        $entity = $this->getEntityById($id);
        return $entity->getTranslations()->first();
    }

    /**
     * @param int $from
     * @param int $amount
     * @param string|null $optionalLang
     * @return array
     */
    public function getTranslationEntitiesInPack(?int $from, ?int $amount, ?string $optionalLang = null): array
    {
        /* @var $repo TranslationQueryableRepository */
        $repo = $this->translationRepostoryReference;
        return $repo->getTranslationsInPacksByLang($from, $amount, $optionalLang);
    }

    // Private

    /**
     * @throws BadRepositoryTypeException
     */
    private function checkTranslationRepositoryType()
    {
        if ($this->translationRepostoryReference && !$this->translationRepostoryReference instanceof TranslationQueryableRepository) throw new BadRepositoryTypeException();
    }

    /**
     * @return TranslationQueryableRepository
     * @throws \Exception
     */
    public function getTranslationRepositoryReference(): TranslationQueryableRepository
    {
        if (!$this->translationRepostoryReference) throw new \Exception('No translation repository provided.');

        return $this->translationRepostoryReference;
    }

    /**
     * @return Translatable
     */
    public function getTranslationForCurrentLang(): Translatable
    {
        return $this->translationRepostoryReference->getTranslationForCurrentLang();
    }

    /**
     * @param int $entityId
     * @param string $langCode
     * @return ContentTranslation
     */
    public function getTranslationForEntityByLangCode(int $entityId, string $langCode): ?ContentTranslation
    {
        /** @var $entity TranslationFilterableEntity */
        $entity = $this->getEntityById($entityId);
        if (!$entity) throw new NotFoundHttpException('Resource not found.', null, 404);
        $language = $this->languageProvider->getLangByCode($langCode);
        if (!$language) throw new BadRequestHttpException('Unkown language!');

        return $entity->getTranslationForLanguage($language);
    }

    /**
     * @param int $entityId
     * @param string $langCode
     * @return ContentTranslation|null
     */
    public function getTranslationForEntityByLangCodeWithFallback(int $entityId, string $langCode): ?ContentTranslation
    {
        $translation = $this->getTranslationForEntityByLangCode($entityId, $langCode);
        if (!$translation) {
            $entity = $this->getEntityById($entityId);
            $fallbackTranslation = $entity->getTranslationForLanguage($this->languageProvider->getDefaultLanguage());
            if (!$fallbackTranslation) throw new NotFoundHttpException('Resource not found.', null, 404);
            return $fallbackTranslation;
        }

        return $translation;
    }

    /**
     * @return Translatable
     */
    public function getEntityWithAppropriateTranslation(): Translatable
    {
        return $this->repositoryReference->getEntityAndTranslations();
    }

    /**
     * @param array $criteria
     * @return array of Translation entities
     */
    public function getEntitiesWithAppropriateTranslation(array $criteria = null): array // of Translatable
    {
        $this->repositoryReference->setOrder($this->order);
        $this->repositoryReference->setCriteria($criteria ?? $this->queryCriteria);
        return $this->repositoryReference->getEntitiesWithTranslationByLang($this->getUsableLang());
    }
    // Private

    /**
     * Sets up languages.
     */
    private function setupLangs()
    {
        $this->currentLang = $this->languageProvider->getCurrentLanguage();
        $this->fallbackLang = $this->languageProvider->getDefaultLanguage();
    }

    /**
     * Instead of having to rewrite following lines 1000 times, it returns the usable lang.
     * @return LangEntity
     */
    private function getUsableLang()
    {
        return $this->currentLang ? $this->currentLang : $this->fallbackLang;
    }
}