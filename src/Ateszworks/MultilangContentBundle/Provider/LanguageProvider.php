<?php

namespace Ateszworks\MultilangContentBundle\Provider;

use Ateszworks\MultilangContentBundle\ContentInterface\LanguageProviderInterface;
use Ateszworks\MultilangContentBundle\Entity\LangEntity;
use Ateszworks\MultilangContentBundle\UserPresentable\PresentableLang;
use Ateszworks\MultilangContentBundle\UserPresentable\UserPresentable;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class LanguageProvider
 * @package App\Ateszworks\MultilangContentBundle\Provider
 */
class LanguageProvider implements LanguageProviderInterface, EventSubscriberInterface
{

    /**
     * @var bool
     * Indicates if setup has happened.
     */
    private $isSetUp = false;

    /**
     * @var array $languageCache
     */
    private $languageCache = null;

    /**
     * @var string
     * For performance issues, only storing
     * locale code, and returning a complete LangEntity
     * only when it is explicitly requested.
     */
    private $localeCode = 'hu';

    /**
     * @var $session Session
     */
    private $session;

    /**
     * @var $defaultLang LangEntity
     */
    private $defaultLang;

    /**
     * @var $currentLang LangEntity
     */
    private $currentLang;

    /**
     * @var bool
     */
    private $shouldStoreLangInSession;

    /**
     * @var string
     */
    private $defaultLangCode;

    /**
     * @var EntityManager $entityManager
     */
    private $entityManager;


    /**
     * LanguageProvider constructor.
     * @param bool $shouldStoreLangInSession
     * @param string $defaultLocale
     * @throws \Exception
     */
    public function __construct(bool $shouldStoreLangInSession, string $defaultLocale)
    {
        $this->shouldStoreLangInSession = $shouldStoreLangInSession;
        $this->defaultLangCode = $defaultLocale;
    }

    /**
     * @param Session $session
     * Setter injection!
     */
    public function setSession(Session $session)
    {
        $this->session = $session;
    }

    /**
     * @param EntityManager $entityManager
     */
    public function setStorage(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return LangEntity
     * @throws \Exception
     */
    public function getLanguageFromSession(): LangEntity
    {
        // TODO: Implement getLanguageFromSession() method.
        throw new \Exception("Not implemented.", 501);
    }

    /**
     * @return LangEntity
     * @throws \Exception
     */
    public function getDefaultLanguage(): LangEntity
    {
        $this->setupSelf();
        return $this->defaultLang;
    }

    /**
     * @return LangEntity
     * @throws \Exception
     */
    public function getCurrentLanguage(): LangEntity
    {
        $this->setupSelf();
        return $this->currentLang;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [KernelEvents::REQUEST => [['onKernelRequest', 20]]];
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        // TODO: Fix the following line: only return when
        // session saving is active, and langs are the same!
        // if (!$request->hasPreviousSession()) return;

        $this->localeCode = $request->attributes->get('_locale') ?? $this->defaultLangCode;
        $this->setSessionLang($request);
    }

    /**
     * @return array|LangEntity[]
     */
    public function getAllAvailableLanguages(): array
    {
        if ($this->languageCache) return $this->languageCache;
        return ($this->languageCache = $this->entityManager->getRepository(LangEntity::class)->findAll());
    }

    /**
     * @param string $code
     * @return LangEntity|null
     * @throws \Exception
     */
    public function getLangByCode(string $code): ?LangEntity
    {
        $languages = $this->getAllAvailableLanguages();
        foreach ($languages as $langEntity) {
            if ($langEntity->getCode() == $code) return $langEntity;
        }

        return null;
    }

    /**
     * @param int $id
     * @return LangEntity|null
     */
    public function getLangById(int $id): ?LangEntity
    {
        $languages = $this->getAllAvailableLanguages();
        foreach ($languages as $langEntity) {
            if ($langEntity->getId() == $id) return $langEntity;
        }

        return null;
    }

    /**
     * @return array|null
     */
    public function getLanguagesForSwitcher(): ?array
    {
        return array_map(function ($langEntity) {
            return $this->transform($langEntity);
        }, $this->getAllAvailableLanguages());
    }

    // MARK: - Private

    /**
     * @param Request $request
     */
    private function setSessionLang(Request $request): void
    {
        if (!$this->shouldStoreLangInSession) return;

        if ($locale = $request->attributes->get('_locale')) {
            $request->getSession()->set('_locale', $locale);
        } else {
            // if no explicit locale has been set on this request, use one from the session
            $request->setLocale($request->getSession()->get('_locale', $this->defaultLang));
        }
    }

    /**
     * This should setup locales.
     * @throws \Exception
     */
    private function setupSelf(): void
    {
        if ($this->isSetUp) return;

        // possible upgrade option I should not spend time with yet:
        // create another provider object that encapsulates handling language getting by code
        $qb = $this->entityManager->getRepository(LangEntity::class)->createQueryBuilder('lang');
        $results = $qb->orWhere('lang.code = :currentLangCode')
            ->orWhere('lang.code = :defaultLangCode')
            ->setParameter('currentLangCode', $this->localeCode)
            ->setParameter('defaultLangCode', $this->defaultLangCode)
            ->getQuery()
            ->getResult();

        if (count($results) == 1 && $this->localeCode == $this->defaultLangCode) {
            $this->currentLang = $results[0];
            $this->defaultLang = $results[0];
        } else if (count($results) == 2 && $this->localeCode != $this->defaultLangCode) {
            /**
             * @var LangEntity $firstResult
             * @var LangEntity $secondResult
             */
            $firstResult = $results[0];
            $secondResult = $results[1];
            $this->currentLang = $firstResult->getCode() == $this->localeCode ? $firstResult : $secondResult;
            $this->defaultLang = $firstResult->getCode() == $this->defaultLangCode ? $firstResult : $secondResult;
        } else {
            throw new \Exception("Cannot determine actual lang in service.", 500);
        }

        $this->isSetUp = true;
    }

    /**
     * @param LangEntity $langEntity
     * @return UserPresentable
     * @throws \Exception
     */
    private function transform(LangEntity $langEntity): UserPresentable
    {

        $transLationForCurrentLang = $langEntity->getTranslationForLanguage($langEntity);
        $translation = $transLationForCurrentLang ? $transLationForCurrentLang : $langEntity->getTranslations()->first();

        return ((new PresentableLang())->setCode($langEntity->getCode())->setLangName($translation->getLangName()));
    }
}