<?php

namespace Ateszworks\MultilangContentBundle\UserPresentable;


/**
 * Class PresentableLang
 * @package Ateszworks\MultilangContentBundle\UserPresentable
 */
class PresentableLang implements UserPresentable
{

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $langName;

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return PresentableLang
     */
    public function setCode(string $code): PresentableLang
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getLangName(): string
    {
        return $this->langName;
    }

    /**
     * @param string $langName
     * @return PresentableLang
     */
    public function setLangName(string $langName): PresentableLang
    {
        $this->langName = $langName;
        return $this;
    }

}