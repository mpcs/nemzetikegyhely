<?php

namespace Ateszworks\MultilangContentBundle\UserPresentable;


/**
 * Class PresentableArticle
 * @package Ateszworks\MultilangContentBundle\UserPresentable
 */
class PresentableArticle implements UserPresentable
{

    /* @var string */
    private $title;

    /* @var string */
    private $coverImage;

    /* @var string */
    private $contents;

    /* @var string */
    private $author;

    /* @var string */
    private $created;

    /* @var string */
    private $published;

    /**
     * @param mixed $title
     * @return PresentableArticle
     */
    public function setTitle($title): PresentableArticle
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getCoverImage(): ?string
    {
        return $this->coverImage;
    }

    /**
     * @param string $coverImage
     * @return PresentableArticle
     */
    public function setCoverImage(?string $coverImage): PresentableArticle
    {
        $this->coverImage = $coverImage;
        return $this;
    }

    /**
     * @param mixed $contents
     * @return PresentableArticle
     */
    public function setContents($contents): PresentableArticle
    {
        $this->contents = $contents;
        return $this;
    }

    /**
     * @param mixed $author
     * @return PresentableArticle
     */
    public function setAuthor($author): PresentableArticle
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @param mixed $created
     * @return PresentableArticle
     */
    public function setCreated($created): PresentableArticle
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @param string $published
     * @return PresentableArticle
     */
    public function setPublished(string $published): PresentableArticle
    {
        $this->published = $published;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getContents()
    {
        return $this->contents;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return string
     */
    public function getPublished(): ?string
    {
        return $this->published;
    }

}