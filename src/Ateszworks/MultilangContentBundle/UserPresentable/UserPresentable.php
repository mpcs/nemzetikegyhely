<?php

namespace Ateszworks\MultilangContentBundle\UserPresentable;


/**
 * Interface UserPresentable
 * @package Ateszworks\MultilangContentBundle\Transformer
 * A convenience interface to represent a transformers work.
 */
interface UserPresentable { }