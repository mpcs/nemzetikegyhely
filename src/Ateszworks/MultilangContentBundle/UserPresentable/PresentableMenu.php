<?php

namespace Ateszworks\MultilangContentBundle\UserPresentable;


/**
 * Class PresentableMenu
 * @package Ateszworks\MultilangContentBundle\UserPresentable
 */
class PresentableMenu implements UserPresentable
{

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $url;

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }


}