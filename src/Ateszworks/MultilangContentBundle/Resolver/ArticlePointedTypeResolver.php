<?php

namespace Ateszworks\MultilangContentBundle\Resolver;


use Ateszworks\MultilangContentBundle\ContentInterface\ContentProviderInterface;
use Ateszworks\MultilangContentBundle\ContentInterface\LanguageProviderInterface;
use Ateszworks\MultilangContentBundle\ContentInterface\PointedTypeRouteResolver;
use Ateszworks\MultilangContentBundle\Entity\ArticleEntity;
use Ateszworks\MultilangContentBundle\Entity\ArticleTranslationEntity;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class ArticlePointedTypeResolver implements PointedTypeRouteResolver
{
    /* @var RouterInterface */
    private $router;

    /* @var ContentProviderInterface */
    private $contentProvider;

    /* @var LanguageProviderInterface */
    private $languageProvider;

    /* @var string */
    private $routeName;

    /**
     * ArticlePointedTypeResolver constructor.
     * @param RouterInterface $router
     * @param ContentProviderInterface $contentProvider
     * @param LanguageProviderInterface $languageProvider
     * @param string $routeName
     */
    public function __construct(RouterInterface $router, ContentProviderInterface $contentProvider, LanguageProviderInterface $languageProvider, string $routeName)
    {
        $this->router = $router;
        $this->contentProvider = $contentProvider;
        $this->languageProvider = $languageProvider;
        $this->routeName = $routeName;
    }


    /**
     * @return string
     */
    public function getPointedType(): string
    {
        return 'aw_article';
    }

    /**
     * @param string $id
     * @return string
     */
    public function getRouteForResource(string $id): string
    {
        $this->setContext();
        $langCode = $this->languageProvider->getCurrentLanguage()->getCode();
        /* @var ArticleTranslationEntity $translation */
        /* @var $this->contentProvider ContentProvider */
        $translation = $this->contentProvider->getTranslationForEntityByLangCodeWithFallback((int)$id, $langCode);

        return $this->router->generate($this->routeName,
            ['_locale' => $this->languageProvider->getCurrentLanguage()->getCode(),
                'article_slug' => $translation->getUrlSlug()], UrlGeneratorInterface::ABSOLUTE_PATH);
    }

    /**
     * @return string
     * Returns fully qualified class name of the entity that
     * the class/object implements this managing.
     */
    public function getManagedResourceTranslationClassName(): string
    {
        return ArticleTranslationEntity::class;
    }

    /**
     * @return string
     * Returns the name of the property of the entity the query should look for the
     * queried string in.
     */
    public function getFieldForQuerying(): string
    {
        return ArticleTranslationEntity::getFilterableFieldName();
    }

    //MARK: CP ... Move it there!
    private function setContext(): void
    {
        $this->contentProvider->setManagedContentType(ArticleEntity::class);
        $this->contentProvider->setManagedContentTranslationType(ArticleTranslationEntity::class);
    }
}