<?php

namespace Ateszworks\MultilangContentBundle\Resolver;


use Ateszworks\MultilangContentBundle\ContentInterface\LanguageProviderInterface;
use Ateszworks\MultilangContentBundle\ContentInterface\PointedTypeRouteResolver;
use Ateszworks\MultilangContentBundle\Manager\PointableResourceManager;
use Doctrine\ORM\EntityManager;

/**
 * Class TypeBasedRouteResolverService
 * @package Ateszworks\MultilangContentBundle\Resolver
 */
class TypeBasedRouteResolverService
{

    /* @var array */
    private $cache = array();

    /* @var EntityManager */
    private $entityManager;

    /* @var LanguageProviderInterface */
    private $languageProvider;

    /* @var PointableResourceManager $pointableResourceManager */
    private $pointableResourceManager = null;

    /**
     * TypeBasedRouteResolverService constructor.
     * @param iterable $taggedServices
     */
    public function __construct(iterable $taggedServices)
    {
        $this->buildCache($taggedServices);
    }

    /**
     * @param EntityManager $entityManager
     */
    public function setEntityManager(EntityManager $entityManager): void
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param LanguageProviderInterface $languageProvider
     */
    public function setLanguageProvider(LanguageProviderInterface $languageProvider): void
    {
        $this->languageProvider = $languageProvider;
    }

    /**
     * @param string $withString
     * @return array
     */
    public function queryForResources(string $withString): array
    {
        if (!$this->pointableResourceManager) $this->createPointableResourceManager();
        return $this->pointableResourceManager->buildQuery($this->cache, $withString);
    }

    /**
     * @param string $resourceTypeString
     * @return PointedTypeRouteResolver|null
     */
    public function getResourceManagerForResourceType(string $resourceTypeString): ?PointedTypeRouteResolver
    {
        return in_array($resourceTypeString, array_keys($this->cache)) ? $this->cache[$resourceTypeString] : null;
    }

    // Private

    /**
     * @param iterable $services
     */
    private function buildCache(iterable $services): void
    {
        /* @var $service PointedTypeRouteResolver */
        foreach ($services as $service) {
            $this->cache[$service->getPointedType()] = $service;
        }
    }

    /**
     * Creates necessary object for querying. Since I want to inject that object nowhere else,
     * I use class as a factory for PointableResourceManager.
     */
    private function createPointableResourceManager(): void
    {
        $this->pointableResourceManager = PointableResourceManager::create($this->entityManager, $this->languageProvider);
    }

}