<?php

namespace Ateszworks\MultilangContentBundle\Manager;

/**
 * Interface RemoveCapableManager
 * @package Ateszworks\MultilangContentBundle\Manager
 */
interface RemoveCapableManager
{
    /**
     * @param $entity
     */
    public function remove($entity): void;

    /**
     * @param int $id
     */
    public function removeById(int $id): void;
}