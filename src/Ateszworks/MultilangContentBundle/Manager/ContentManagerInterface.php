<?php

namespace Ateszworks\MultilangContentBundle\Manager;

use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Interface ContentManagerInterface
 * @package Ateszworks\MultilangContentBundle\Manager
 */
interface ContentManagerInterface extends RemoveCapableManager
{

    /**
     * @return Session
     */
    public function getSession(): Session;

    /**
     * @param int $id
     * @return mixed|null|object
     */
    public function getEntityById(int $id);

    /**
     * @return array|null
     */
    public function getEntites(): ?array;

    /**
     * @return array|null
     */
    public function getEntityTranslations(): ?array;

    /**
     * @param $entity
     */
    public function save($entity): void;

    /**
     * @param int $id
     */
    public function removeById(int $id): void;

}