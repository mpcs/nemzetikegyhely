<?php

namespace Ateszworks\MultilangContentBundle\Manager;


/**
 * Class PointableResourceManager
 * @package Ateszworks\MultilangContentBundle\Manager
 * This manager has to somehow collect all the pointable resource types ->
 */

use Ateszworks\MultilangContentBundle\ContentInterface\LanguageProviderInterface;
use Ateszworks\MultilangContentBundle\ContentInterface\PointedTypeRouteResolver;
use Doctrine\ORM\EntityManager;

/**
 * Class PointableResourceManager
 * @package Ateszworks\MultilangContentBundle\Manager
 */
class PointableResourceManager
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var LanguageProviderInterface
     */
    private $languageProvider;

    /**
     * PointableResourceManager constructor.
     * @param EntityManager $entityManager
     * @param LanguageProviderInterface $languageProvider
     */
    private function __construct(EntityManager $entityManager, LanguageProviderInterface $languageProvider)
    {
        $this->entityManager = $entityManager;
        $this->languageProvider = $languageProvider;
    }

    /**
     * @param EntityManager $entityManager
     * @param LanguageProviderInterface $languageProvider
     * @return PointableResourceManager
     */
    public static function create(EntityManager $entityManager, LanguageProviderInterface $languageProvider)
    {
        return new PointableResourceManager($entityManager, $languageProvider);
    }

    /**
     * @param $services |PointedTypeRouteResolver[]
     * @param $queryString
     * @return array
     */
    public function buildQuery(array $services, string $queryString): array // eg: ['aw_article' => [PointedTypeTranslationInterface]]
    {
        $results = [];
        $queryResults = [];

        foreach ($services as $service)
            $queryResults = array_merge($queryResults, $this->doQuery($service, $queryString));

        foreach ($queryResults as $object) {
            foreach ($services as $service) {
                if ($service->getManagedResourceTranslationClassName() != get_class($object)) continue;
                $results[$service->getPointedType()][] = $object;
            }
        }

        return $results;
    }

    // MARK: - Private

    /**
     * @param PointedTypeRouteResolver $service
     * @param string $queryString
     * @return array
     */
    private function doQuery(PointedTypeRouteResolver $service, string $queryString): array
    {
        $managedEntityClassName = $service->getManagedResourceTranslationClassName();
        $qb = $this->entityManager->createQueryBuilder()->setMaxResults(5); // todo: make it configurable

        return $qb->addSelect('tableAlias')
            ->from($managedEntityClassName, 'tableAlias')// todo: consider if joining the parent worth it.
            ->orWhere($qb->expr()->like('tableAlias.' . $service->getFieldForQuerying(), ':param'))
            ->setParameter('param', '%' . $queryString . '%')
            ->getQuery()->getResult();
    }
}