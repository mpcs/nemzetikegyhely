<?php

namespace Ateszworks\MultilangContentBundle\Manager;

use Ateszworks\MultilangContentBundle\ContentInterface\UserPresentableProvier;
use Ateszworks\MultilangContentBundle\ContentInterface\ContentProviderInterface;
use Ateszworks\MultilangContentBundle\ContentInterface\ContentTranslation;
use Ateszworks\MultilangContentBundle\ContentInterface\LanguageProviderInterface;
use Ateszworks\MultilangContentBundle\ContentInterface\Translatable;
use Ateszworks\MultilangContentBundle\Entity\ArticleEntity;
use Ateszworks\MultilangContentBundle\Entity\ArticleTranslationEntity;
use Ateszworks\MultilangContentBundle\Entity\LangEntity;
use Ateszworks\MultilangContentBundle\Entity\TranslationFilterableEntity;
use Ateszworks\MultilangContentBundle\Transformer\ArticleTransformer;
use Ateszworks\MultilangContentBundle\UserPresentable\UserPresentable;
use Ateszworks\MultilangContentBundle\Utils\UrlUtils;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class ArticleManager
 * @package Ateszworks\MultilangContentBundle\Manager
 */
class ArticleManager extends AbstractContentManager implements UserPresentableProvier
{

    /* @var string */
    protected $associatedRouteName;

    /* @var TranslatorInterface */
    protected $translator;

    /* @var FileManager */
    protected $fileManager;

    /* @var ArticleTransformer */
    protected $transformer;

    /**
     * ArticleManager constructor.
     * @param ContentProviderInterface $contentProvider
     * @param LoggerInterface $logger
     * @param SessionInterface $session
     * @throws \Ateszworks\MultilangContentBundle\Exception\BadRepositoryTypeException
     */
    public function __construct(ContentProviderInterface $contentProvider,
                                LoggerInterface $logger, SessionInterface $session)
    {
        parent::__construct($contentProvider, $logger, $session);
        $this->contentProvider->setManagedContentType(ArticleEntity::class);
        $this->contentProvider->setManagedContentTranslationType(ArticleTranslationEntity::class);
        $this->contentProvider->setOrderingForPackedRequest(['published' => 'DESC', 'created' => 'DESC']);
    }

    /**
     * @param ArticleTransformer $transformer
     */
    public function setTransformer(ArticleTransformer $transformer): void
    {
        $this->transformer = $transformer;
    }

    /**
     * @param FileManager $fileManager
     */
    public function setFileManager(FileManager $fileManager): void
    {
        $this->fileManager = $fileManager;
    }

    /**
     * @param LanguageProviderInterface $languageProvider
     */
    public function setLanguageProvider(LanguageProviderInterface $languageProvider): void
    {
        parent::setLanguageProvider($languageProvider);
    }

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param int $articleId
     * @param string $langCode
     * @return ContentTranslation|Translatable|ArticleTranslationEntity
     */
    public function getTranslationOfEntityByIdAndLang(int $articleId, string $langCode = null): ?ArticleTranslationEntity
    {
        /** @var ArticleTranslationEntity $translation */
        $usableLangCode = $langCode ?? $this->languageProvider->getCurrentLanguage()->getCode();
        $translation = $this->contentProvider->getTranslationForEntityByLangCode($articleId, $usableLangCode);

        return $translation;
    }

    /**
     * @return null|Translatable[]
     */
    public function getArticleTranslations(): ?array
    {
        return $this->contentProvider->getEntitiesWithAppropriateTranslation();
    }

    /**
     * @return array|null
     */
    public function getEntityTranslations(): ?array
    {
        return $this->getArticleTranslations();
    }

    /**
     * @return array|null
     */
    public function adminListableArticlesWithTranslations(): ?array
    {
        $pack = $this->getArticleTranslations();
        $currentLang = $this->languageProvider->getCurrentLanguage();

        /* @var $item TranslationFilterableEntity */
        foreach ($pack as &$item) {
            $hunTranslation = $item->getTranslationForLanguage($currentLang);
            if ($hunTranslation) {
                $translations = new ArrayCollection();
                $translations->add($hunTranslation);
                $item->setTranslations($translations); // todo: rethink if this is right
            }
        }

        return $pack;
    }

    /**
     * @param string $languageCode
     * @param string $title
     * @param string $content
     * @param ArticleEntity|null $existingArticle
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveArticle(string $languageCode, string $title, string $content, ?ArticleEntity $existingArticle = null)
    {
        $articleLanguage = $this->languageProvider->getLangByCode($languageCode);
        $article = null;

        if (!$existingArticle) {
            $article = new ArticleEntity();
            $newTranslation = new ArticleTranslationEntity();
            $newTranslation->setArticle($article);
            $translations = $article->getTranslations();
            $translations->add($newTranslation);
            $article->setTranslations($translations);
            $newTranslation->setLang($articleLanguage);
            $newTranslation->setTitle($title);
            $newTranslation->setContent($content);
        }

        $this->contentProvider->saveByEntity($article);
    }

    /**
     * @param ArticleTranslationEntity $forEntity
     * @param UploadedFile $withImage
     */
    public function handleImageUpload(?ArticleTranslationEntity $forEntity, ?UploadedFile $withImage): void
    {
        if (!$withImage || !$forEntity) return;

        $article = $forEntity->getArticle();
        if ($article->getImageUrl()) $this->fileManager->removeFile($article->getImageUrl());
        $storedFile = $this->fileManager->uploadImage($withImage);
        $article->setImageUrl($storedFile);
    }

    /**
     * @param $entity
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save($entity): void
    {
        /* @var ArticleTranslationEntity $entity */
        $articleEntity = $entity->getArticle();
        if (!$articleEntity) {
            $articleEntity = new ArticleEntity();
            $entity->setArticle($articleEntity);
        }

        $slug = UrlUtils::getCleanFormOfString($entity->getTitle());
        $entity->setUrlSlug($slug);
        $translations = $articleEntity->getTranslations();
        $translations->add($entity);

        $this->contentProvider->saveByEntity($articleEntity);
    }

    /**
     * @param int $id
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeArticleById(int $id)
    {
        $entity = $this->contentProvider->getEntityById($id);
        $this->contentProvider->removeByEntity($entity);
    }

    /**
     * @param ArticleEntity $articleEntity
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove($articleEntity): void
    {
        $this->contentProvider->removeByEntity($articleEntity);
    }

    /**
     * @param $slug
     * @return UserPresentable
     * @throws \Exception
     */
    public function getArticleBySlug($slug): UserPresentable
    {
        $translation = $this->getTranslationBySlug($slug);
        if (!$translation) throw
        new NotFoundHttpException($this->translator->trans('user_messages.content_not_found'));

        return $this->getPresentable($translation, $this->languageProvider->getCurrentLanguage());
    }

    // PRIVATE & PROTECTED

    /**
     * @param string $slug
     * @return ArticleTranslationEntity|null
     * @throws \Exception
     */
    protected function getTranslationBySlug(string $slug): ?ArticleTranslationEntity
    {
        /* @var EntityRepository $translationRepoReference */
        /* @var ArticleTranslationEntity $translation */
        $translationRepoReference = $this->contentProvider->getTranslationRepositoryReference();
        return $translationRepoReference->findOneBy(['urlSlug' => $slug]);
    }


    // UserPresentableProvider

    /**
     * @param TranslationFilterableEntity $fromEntity
     * @param LangEntity $inLang
     * @return UserPresentable|null
     */
    public function getPresentable(TranslationFilterableEntity $fromEntity, LangEntity $inLang): ?UserPresentable
    {
        return $this->transformer->transform($fromEntity, $inLang);
    }

    /**
     * @param array $entities
     * @param LangEntity $inLang
     * @return array|null
     */
    public function getPresentables(array $entities, LangEntity $inLang): ?array
    {
        return array_map(function ($translation) use ($inLang) {
            return $this->getPresentable($translation, $inLang);
        }, $entities);
    }

    /**
     * @param int $from
     * @param int $amount
     * @param LangEntity $inLang
     * @return array|null
     */
    public function getPresentablesPaged(int $from, int $amount, LangEntity $inLang): ?array
    {
        // TODO: Implement getPresentablesPaged() method.
        return null;
    }
}
