<?php

namespace Ateszworks\MultilangContentBundle\Manager;

use Ateszworks\MultilangContentBundle\ContentInterface\UserPresentableProvier;
use Ateszworks\AdminBundle\Controller\MenuController;
use Ateszworks\MultilangContentBundle\ContentInterface\ContentProviderInterface;
use Ateszworks\MultilangContentBundle\ContentInterface\PointedTypeResource;
use Ateszworks\MultilangContentBundle\ContentInterface\PointedTypeTranslationInterface;
use Ateszworks\MultilangContentBundle\Entity\LangEntity;
use Ateszworks\MultilangContentBundle\Entity\MenuEntity;
use Ateszworks\MultilangContentBundle\Entity\MenuTranslationEntity;
use Ateszworks\MultilangContentBundle\Entity\TranslationFilterableEntity;
use Ateszworks\MultilangContentBundle\Resolver\TypeBasedRouteResolverService;
use Ateszworks\MultilangContentBundle\Transformer\MenuTransformer;
use Ateszworks\MultilangContentBundle\UserPresentable\UserPresentable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\ORMException;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class MenuManager
 * @package Ateszworks\MultilangContentBundle\Manager
 */
class MenuManager extends AbstractContentManager implements UserPresentableProvier
{

    /* @var Validation */
    private $validator;

    /* @var TypeBasedRouteResolverService */
    private $typeBasedRouteResolver;

    /**
     * MenuManager constructor.
     * @param ContentProviderInterface $contentProvider
     * @param Logger $logger
     * @param Session $session
     * @param TypeBasedRouteResolverService $typeBasedRouteResolver
     * @param MenuTransformer $menuTransformer
     */
    public function __construct(ContentProviderInterface $contentProvider,
                                Logger $logger,
                                Session $session,
                                TypeBasedRouteResolverService $typeBasedRouteResolver,
                                MenuTransformer $menuTransformer)
    {
        parent::__construct($contentProvider, $logger, $session);
        $this->validator = Validation::createValidator();
        $this->typeBasedRouteResolver = $typeBasedRouteResolver;
        $this->transformer = $menuTransformer;

        $this->contentProvider->setManagedContentType(MenuEntity::class);
        $this->contentProvider->setManagedContentTranslationType(MenuTranslationEntity::class);
    }

    /**
     * @param int $id
     * @return MenuEntity
     */
    public function getEntityById(int $id): MenuEntity
    {
        $resource = $this->contentProvider->getEntityById($id);
        if (!$resource) throw new NotFoundResourceException('A kért tartalom nem található.', 404);

        return $resource;
    }

    /**
     * @return null|Translatable[]
     */
    public function getMenuTranslations(): ?array
    {
        $this->contentProvider->setOrderingForPackedRequest(['weight' => 'ASC']);
        return $this->contentProvider->getEntitiesWithAppropriateTranslation();
    }

    /**
     * @return array|null
     */
    public function getAll()
    {
        $menuTranslations = $this->getMenuTranslations();
        return $this->getPresentables($menuTranslations, $this->languageProvider->getCurrentLanguage());
    }

    /**
     * @return array|null
     */
    public function getEntityTranslations(): ?array
    {
        return $this->getMenuTranslations();
    }

    /**
     * @param MenuEntity $menuEntity
     * @param array $withAssocArray
     * @param bool $isNewEntity
     * @throws \Error
     */
    public function mergeUpdateTranslations(MenuEntity $menuEntity, array $withAssocArray,
                                            bool $isNewEntity = false): void
    {
        $currentTranslations = !$isNewEntity ? $menuEntity->getTranslations() : new ArrayCollection();
        foreach ($withAssocArray as $langCode => $title) {
            $translationEntity = $this->findTranslation($currentTranslations, $langCode);

            if ($translationEntity) {
                $translationEntity->setTitle($title);
                continue;
            }

            $lang = $this->languageProvider->getLangByCode($langCode);
            if (!$lang) throw new \Error('Invalid language code!');

            /* @var $newTranslation MenuTranslationEntity */
            $newTranslation = new MenuTranslationEntity();
            $newTranslation->setTitle($title);
            $newTranslation->setLang($lang);
            $newTranslation->setMenu($menuEntity);
            $currentTranslations->add($newTranslation);
            $menuEntity->setTranslations($currentTranslations);
        }
    }

    /**
     * @param MenuEntity $menuEntity
     * @return bool
     */
    public function saveMenu(MenuEntity $menuEntity): bool
    {
        try {
            $this->contentProvider->saveByEntity($menuEntity); // throws!
            return true;
        } catch (ORMException $exception) {
            $this->logger->error('Could not save menu entity. Info attached.', [
                'code' => $exception->getCode(),
                'message' => $exception->getMessage(),
                'file @ line' => $exception->getFile() . ' @ ' . $exception->getLine(),
                'trace' => $exception->getTraceAsString()
            ]);

            return false;
        }
    }

    /**
     * @param MenuEntity $menuEntity
     */
    public function removeMenu(MenuEntity $menuEntity)
    {
        $this->contentProvider->removeByEntity($menuEntity);
    }

    // MARK: Form helper part

    /**
     * @param array $formData
     * @return null|ConstraintViolationList
     */
    public function validatePostedTranslations(array $formData): ?ConstraintViolationList
    {
        $allLangInSystem = $this->languageProvider->getAllAvailableLanguages();
        $assertArray = ['fields' => []];
        foreach ($allLangInSystem as $langEntity) {
            $assertArray['fields'][$langEntity->getCode()] = [
                new Assert\NotBlank(['message' => 'form_errors.should_not_be_empty']),
                new Assert\Length([
                    'min' => 1,
                    'minMessage' => 'form_errors.too_short_string'
                ]),
                new Assert\NotEqualTo([
                    'value' => MenuController::UNPROVIDED_VALUE_STRING,
                    'message' => 'form_errors.should_not_be_equal_to_specific_text'
                ])
            ];
        }

        return $this->validator->validate($formData, new Assert\Collection($assertArray));
    }

    /**
     * @param int $number
     * @return \Symfony\Component\Validator\ConstraintViolationListInterface
     */
    public function validateNumber(int $number) // todo: outsource all validation methods!!
    {
        return $this->validator->validate($number, [
            new Assert\NotNull(),
            new Assert\GreaterThanOrEqual(['value' => 0, 'message' => 'form_errors.should_be_greater_than']),
            new Assert\LessThanOrEqual(['value' => 99, 'message' => 'form_errors.should_be_less_than'])
        ]);
    }

    // MARK: Private

    /**
     * @param Collection $translations todo: investigate if getTranslationForLangugae could be used
     * @param string $byCode
     * @return MenuTranslationEntity|null
     */
    private function findTranslation(Collection $translations, string $byCode): ?MenuTranslationEntity
    {
        /* @var $translation MenuTranslationEntity */
        foreach ($translations as $translation) {
            if ($translation->getLang()->getCode() == $byCode) return $translation;
        }

        return null;
    }

    /**
     * @param $pointedType
     * @return \Symfony\Component\Validator\ConstraintViolationListInterface
     */
    public function validateTarget($pointedType)
    {
        return $this->validator->validate($pointedType, [
            new Assert\Choice([
                'choices' => [MenuEntity::LINK_POINTING_TYPE, MenuEntity::RESOURCE_POINTING_TYPE],
                'message' => 'general.monkey_business'
            ]),
            new Assert\NotNull()
        ]);
    }

    /**
     * @param string $forNameFraction
     * @return array
     */
    public function getResourceSuggestions(string $forNameFraction): array
    {
        $resourceResults = $this->typeBasedRouteResolver->queryForResources($forNameFraction);
        $results = [];

        foreach ($resourceResults as $resourceIdentifier => $resourceTranslationArray) {
            foreach ($resourceTranslationArray as $translation) {
                /* @var $translation PointedTypeTranslationInterface, Translatable */
                /* @var $resourceEntity PointedTypeResource */
                $resourceEntity = $translation->getTranslationOf();
                // MUST use this structure, because otherwise it would merge results by id!!
                $results[$resourceIdentifier][] = [
                    'id' => $resourceEntity->getId(),
                    'string' => $translation->getFilterableFieldContents()
                ];
            }
        }

        return $results;
    }

    // UserPresentableProvider

    /**
     * @param TranslationFilterableEntity $fromEntity
     * @param LangEntity $inLang
     * @return UserPresentable|null
     */
    public function getPresentable(TranslationFilterableEntity $fromEntity, LangEntity $inLang): ?UserPresentable
    {
        return $this->transformer->transform($fromEntity, $inLang);
    }

    /**
     * @param array $entities
     * @param LangEntity $inLang
     * @return array|null
     */
    public function getPresentables(array $entities, LangEntity $inLang): ?array
    {
        return array_map(function ($translation) {
            return $this->getPresentable($translation, $this->languageProvider->getCurrentLanguage());
        }, $entities);
    }


    /**
     * @param int $from
     * @param int $amount
     * @param LangEntity $inLang
     * @return array|null
     */
    public function getPresentablesPaged(int $from, int $amount, LangEntity $inLang): ?array
    {
        // TODO: Implement getPresentablesPaged() method.
        return null;
    }
}