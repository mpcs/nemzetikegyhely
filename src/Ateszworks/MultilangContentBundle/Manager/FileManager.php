<?php

namespace Ateszworks\MultilangContentBundle\Manager;


use App\Ateszworks\MultilangContentBundle\Utils\AWMCBUtils;
use Monolog\Logger;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class FileManager
 * @package Ateszworks\MultilangContentBundle\Manager
 */
class FileManager
{

    /* Concrete name of JPEG. */
    CONST JPEG = 'jpeg';

    /* Concrete name of PNG. */
    CONST PNG = 'png';

    /* Concrete name of BMP. */
    CONST BMP = 'bmp';

    /* Concrete name of GIF. */
    CONST GIF = 'gif';

    /* @var array */
    private static $supporedImageMimeTypes = [
        self::JPEG => ['image/jpeg'],
        self::PNG => ['image/png'],
        self::BMP => ['image/bmp'],
        self::GIF => ['image/gif']
    ];

    /* @var string */
    protected $workingDirectory;

    /* @var bool */
    protected $processImgOnUpload = false;

    /* @var Filesystem */
    protected $fileSystem;

    /* @var Logger */
    protected $logger;

    /**
     * FileManager constructor.
     * @param string $workingDirectory
     * @param bool $processImgOnUpload
     */
    public function __construct(string $workingDirectory,
                                bool $processImgOnUpload = false)
    {
        $this->workingDirectory = $workingDirectory;
        $this->processImgOnUpload = $processImgOnUpload;
        $this->fileSystem = new Filesystem();
    }

    /**
     * @param Logger $logger
     */
    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function upload(UploadedFile $file): string
    {
        $this->checkDir();
        $extension = $file->guessExtension() ?? '.bin';
        $uniqueName = $this->generateUniqueName();
        $fileName = $uniqueName . '.' . $extension;
        $file->move($this->workingDirectory, $fileName);

        return $fileName;
    }

    /**
     * @param UploadedFile $image
     * @return string
     */
    public function uploadImage(UploadedFile $image): string
    {
        if ($this->processImgOnUpload && $image->getMimeType()) $this->handleImageRotation($image);
        $bigImage = $this->upload($image);

        AWMCBUtils::smartResizeImage($this->workingDirectory . '/' . $bigImage, null,
            40, 0,
            true, $this->workingDirectory . '/' . $this->convertNameToSmall($bigImage),
            false, false,
            100, false);

        return $bigImage;
    }

    /**
     * @param string $fileName
     * @return string
     */
    public function getFullFileURL(string $fileName): string
    {
        $path = $this->workingDirectory . '/' . $fileName;
        if (!$this->fileSystem->exists($path)) throw new NotFoundHttpException($path);

        return $path;
    }

    /**
     * @param string $fileName
     * @return void
     */
    public function removeFile(string $fileName): void
    {
        $path = $this->workingDirectory . '/' . $fileName;
        if (!$this->fileSystem->exists($path)) return;

        try {
            $this->fileSystem->remove($path);
            $this->logger->info('File removed.', ['path' => $path]);
        } catch (IOException $e) {
            $this->logger->warn('Could not remove file.', ['path' => $path]);
        }
    }

    /**
     * @param string $fileName
     */
    public function removeImage(string $fileName): void
    {
        $this->removeFile($fileName);
        $this->removeFile($this->convertNameToSmall($fileName));
    }

    /**
     * @param string $bigName
     * @return string
     */
    public function convertNameToSmall(string $bigName): string
    {
        $smallNameParts = explode('.', $bigName);
        $smallNameParts[count($smallNameParts) - 2] = $smallNameParts[count($smallNameParts) - 2] . '_small';
        return implode('.', $smallNameParts);
    }

    /**
     * @return string
     */
    protected function generateUniqueName(): string
    {
        return md5(uniqid());
    }

    /**
     * Has to check if target directory exists.
     */
    private function checkDir(): void
    {
        if ($this->fileSystem->exists($this->workingDirectory)) return;
        $this->fileSystem->mkdir($this->workingDirectory);
    }

    /**
     * Handles image rotation and EXIF removal, should be used just before saving img.
     * @param UploadedFile $uploadedImage
     */
    private function handleImageRotation(UploadedFile $uploadedImage): void
    {
        $image = null;
        $concreteMimeType = $this->determineImageType($uploadedImage);
        switch ($concreteMimeType) {
            case self::JPEG:
                $image = imagecreatefromjpeg($uploadedImage->getPathname());
                break;
            case self::PNG:
                $image = imagecreatefrompng($uploadedImage->getPathname());
                break;
            case self::BMP:
                $image = imagecreatefrombmp($uploadedImage->getPathname());
                break;
            case self::GIF:
                $image = imagecreatefromgif($uploadedImage->getPathname());
                break;
            default:
                $image = null;
                break;
        }

        if (!$image) return;
        try {
            $exif = exif_read_data($uploadedImage->getPathname());
        } catch (\Exception $e) {
            if ($e->getCode() == 0) return;

            $this->logger->error('Could not read exif data.',
                ['message' => $e->getMessage(), 'code' => $e->getCode()]);

            return;
        }


        if (empty($exif['Orientation'])) return;

        $newImage = null;
        switch ($exif['Orientation']) {
            case 3:
                $newImage = imagerotate($image, 180, 0);
                break;
            case 6:
                $newImage = imagerotate($image, -90, 0);
                break;
            case 8:
                $newImage = imagerotate($image, 90, 0);
                break;
            default:
                return; // unsupported orientation setting
        }

        switch ($concreteMimeType) {
            case self::JPEG:
                imagejpeg($newImage, $uploadedImage->getPathname());
                break;
            case self::PNG:
                imagepng($newImage, $uploadedImage->getPathname());
                break;
            case self::BMP:
                imagebmp($newImage, $uploadedImage->getPathname());
                break;
            case self::GIF:
                imagegif($newImage, $uploadedImage->getPathname());
                break;
            default:
                imagedestroy($newImage);
                imagedestroy($image);
                throw new \Error('Image could not be saved');
                break;
        }

        imagedestroy($newImage);
        imagedestroy($image);
    }

    /**
     * @param UploadedFile $image
     * @return string
     */
    private function determineImageType(UploadedFile $image): ?string
    {
        foreach (self::$supporedImageMimeTypes as $key => $types) {
            foreach ($types as $type) if ($image->getMimeType() == $type) return $key;
        }

        return null;
    }
}