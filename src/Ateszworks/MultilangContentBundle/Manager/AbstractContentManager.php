<?php

namespace Ateszworks\MultilangContentBundle\Manager;

use Ateszworks\MultilangContentBundle\ContentInterface\ContentProviderInterface;
use Ateszworks\MultilangContentBundle\ContentInterface\LanguageProviderInterface;
use Ateszworks\MultilangContentBundle\Entity\TranslationFilterableEntity;
use Ateszworks\MultilangContentBundle\Provider\ContentProvider;
use Ateszworks\MultilangContentBundle\Transformer\Transformer;
use Doctrine\Common\Collections\ArrayCollection;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class AbstractContentManager
 * @package Ateszworks\MultilangContentBundle\Manager
 */
abstract class AbstractContentManager implements ContentManagerInterface
{

    /* @var ContentProvider */
    protected $contentProvider;

    /* @var LanguageProviderInterface */
    protected $languageProvider;

    /* @var $logger Logger */
    protected $logger;

    /* @var $session Session */
    protected $session;

    /* @var Transformer */
    protected $transformer;

    /**
     * AbstractContentManager constructor.
     * @param ContentProviderInterface $contentProvider
     * @param LoggerInterface $logger
     * @param SessionInterface $session
     */
    public function __construct(ContentProviderInterface $contentProvider, LoggerInterface $logger,
                                SessionInterface $session)
    {
        $this->contentProvider = $contentProvider;
        $this->logger = $logger;
        $this->session = $session;
    }

    /**
     * @param LanguageProviderInterface $languageProvider
     */
    public function setLanguageProvider(LanguageProviderInterface $languageProvider): void
    {
        $this->languageProvider = $languageProvider;
    }

    /**
     * @return Session
     */
    public function getSession(): Session
    {
        return $this->session;
    }

    /**
     * @param int $id
     * @return mixed|null|object
     */
    public function getEntityById(int $id)
    {
        return $this->contentProvider->getEntityById($id);
    }

    /**
     * @return array|null
     */
    public function getEntites(): ?array
    {
        return $this->contentProvider->getAll();
    }

    /**
     * @return array
     */
    public function getContentTranslations(): ?array
    {
        // Translations: Entites with appropriate translations...
        $entities = $this->getEntites();
        $currentLang = $this->languageProvider->getCurrentLanguage();

        /* @var $item TranslationFilterableEntity */
        foreach ($entities as &$entity) {
            $hunTranslation = $entity->getTranslationForLanguage($currentLang);
            if (!$hunTranslation) continue;

            $translations = new ArrayCollection();
            $translations->add($hunTranslation);
            $entity->setTranslations($translations);
        }

        return $entities;
    }

    /**
     * @param $entity
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save($entity): void
    {
        $this->contentProvider->saveByEntity($entity);
    }

    /**
     * @param $entity
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove($entity): void
    {
        $this->contentProvider->removeByEntity($entity);
    }

    /**
     * @param int $id
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeById(int $id): void
    {
        $entity = $this->contentProvider->getEntityById($id);
        if (!$entity)
            throw new NotFoundHttpException('Nem található tartalom', null, 404);

        $this->remove($entity);
    }
}