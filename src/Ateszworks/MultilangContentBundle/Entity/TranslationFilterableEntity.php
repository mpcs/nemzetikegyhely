<?php

namespace Ateszworks\MultilangContentBundle\Entity;

use Ateszworks\MultilangContentBundle\ContentInterface\ContentTranslation;
use Ateszworks\MultilangContentBundle\ContentInterface\Translatable;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\PersistentCollection;

/**
 * Class TranslationFilterableEntity
 * @package Ateszworks\MultilangContentBundle\Entity
 */
abstract class TranslationFilterableEntity implements Translatable
{

    /* @var PersistentCollection */
    protected $translations;

    /**
     * @return Collection|null
     */
    public function getTranslations(): ?Collection
    {
        return $this->translations;
    }

    /**
     * @param Collection $translations
     */
    public function setTranslations(Collection $translations)
    {
        $this->translations = $translations;
    }

    /**
     * @param LangEntity $langEntity
     * @return ContentTranslation
     */
    public function getTranslationForLanguage(LangEntity $langEntity): ?ContentTranslation
    {
        if (!$langEntity) return null;

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('lang', $langEntity));

        return $this->translations->matching($criteria)->first() ? $this->translations->matching($criteria)->first() : null;
    }

}