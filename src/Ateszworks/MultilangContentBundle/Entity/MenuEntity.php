<?php

namespace Ateszworks\MultilangContentBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * Class MenuEntity
 * @package Ateszworks\MultilangContentBundle\Entity
 * @ORM\Entity(repositoryClass="Ateszworks\MultilangContentBundle\Repository\MenuEntityRepository")
 * @ORM\Table(name="menu")
 * @ORM\HasLifecycleCallbacks()
 */
class MenuEntity extends TranslationFilterableEntity
{
    CONST LINK_POINTING_TYPE = 0;
    CONST RESOURCE_POINTING_TYPE = 1;

    /**
     * @var integer
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     * The number it gets its order by.
     */
    private $weight = 1;

    /**
     * @var PersistentCollection
     * @ORM\OneToMany(targetEntity="MenuTranslationEntity", mappedBy="menu", cascade={"persist", "remove"})
     */
    protected $translations;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(type="string")
     * Type of target to point to.
     */
    private $target;

    /**
     * @var string
     * @ORM\Column(type="string")
     * If type is RESOURCE_POINTER,
     * then it should be an id the resource can be identified by.
     */
    private $targetIdentifier;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return MenuEntity
     */
    public function setId(int $id): MenuEntity
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     * @return MenuEntity
     */
    public function setWeight(int $weight): MenuEntity
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * @param Collection $translations
     * @return MenuEntity
     */
    public function setTranslations(Collection $translations): ?MenuEntity
    {
        $this->translations = $translations;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreated(): DateTime
    {
        return $this->created;
    }

    /**
     * @param DateTime $created
     */
    public function setCreated(DateTime $created): void
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return MenuEntity
     */
    public function setType(int $type): MenuEntity
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getTarget(): string
    {
        return $this->target;
    }

    /**
     * @param string $target
     * @return MenuEntity
     */
    public function setTarget(string $target): MenuEntity
    {
        $this->target = $target;
        return $this;
    }

    /**
     * @return string
     */
    public function getTargetIdentifier(): string
    {
        return $this->targetIdentifier;
    }

    /**
     * @param string $targetIdentifier
     * @return MenuEntity
     */
    public function setTargetIdentifier(string $targetIdentifier): MenuEntity
    {
        $this->targetIdentifier = $targetIdentifier;
        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersistSetType() {
        $this->type = $this->type ? $this->type : $this::LINK_POINTING_TYPE;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersistSetCreated()
    {
        $this->created = new \DateTime();
    }

}