<?php

namespace Ateszworks\MultilangContentBundle\Entity;

use Ateszworks\MultilangContentBundle\ContentInterface\ContentTranslation;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * Class LangEntity
 * @package Ateszworks\MultilangContentBundle\Entity
 * @ORM\Table(name="languages")
 * @ORM\Entity
 */
class LangEntity // NOTE: Do not make it TranslationFilterable!! No LANGs are mapped!
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=2)
     * Short code like: hu, en, pl...
     */
    private $code;

    /**
     * @var PersistentCollection
     * @ORM\OneToMany(targetEntity="LangTranslationEntity", mappedBy="langEntity")
     */
    protected $translations;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return LangEntity
     */
    public function setId(int $id): LangEntity
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return LangEntity
     */
    public function setCode(string $code): LangEntity
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getTranslations(): ?Collection
    {
        return $this->translations;
    }

    /**
     * @param Collection $translations
     * @return LangEntity
     */
    public function setTranslations(Collection $translations): LangEntity
    {
        $this->translations = $translations;
        return $this;
    }

    /**
     * @param LangEntity $langEntity
     * @return LangTranslationEntity
     */
    public function getTranslationForLanguage(LangEntity $langEntity): ?LangTranslationEntity
    {
        if (!$langEntity) return null;

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('lang', $langEntity->getCode()));

        return $this->translations->matching($criteria)->first() ? $this->translations->matching($criteria)->first() : null;
    }

}