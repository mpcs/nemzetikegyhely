<?php

namespace Ateszworks\MultilangContentBundle\Entity;

use Ateszworks\MultilangContentBundle\ContentInterface\ContentTranslation;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class MenuTranslationEntity
 * @package Ateszworks\MultilangContentBundle\Entity
 * @ORM\Entity(repositoryClass="Ateszworks\MultilangContentBundle\Repository\MenuTranslationEntityRepository")
 * @ORM\Table(name="menu_translations")
 */
class MenuTranslationEntity implements ContentTranslation
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var LangEntity
     * @ORM\ManyToOne(targetEntity="LangEntity")
     */
    private $lang;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @var MenuEntity
     * @ORM\ManyToOne(targetEntity="MenuEntity", inversedBy="translations")
     */
    private $menu;

    /**
     * @return LangEntity
     */
    public function getLang(): LangEntity
    {
        return $this->lang;
    }

    /**
     * @param LangEntity $lang
     * @return MenuTranslationEntity
     */
    public function setLang(LangEntity $lang): MenuTranslationEntity
    {
        $this->lang = $lang;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return MenuTranslationEntity
     */
    public function setTitle($title): MenuTranslationEntity
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return MenuEntity
     */
    public function getMenu(): MenuEntity
    {
        return $this->menu;
    }

    /**
     * @param MenuEntity $menu
     * @return MenuTranslationEntity
     */
    public function setMenu(MenuEntity $menu): MenuTranslationEntity
    {
        $this->menu = $menu;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    // ContentTranslation
    public function getContent(): ?string
    {
        return $this->getTitle();
    }

    // Magic
    public function __toString(): string
    {
        return "id: " . $this->id . ", title: " . $this->title . ", parent menu id: " . $this->menu->getId();
    }


}