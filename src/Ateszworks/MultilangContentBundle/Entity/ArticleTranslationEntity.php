<?php

namespace Ateszworks\MultilangContentBundle\Entity;

use Ateszworks\MultilangContentBundle\ContentInterface\ContentTranslation;
use Ateszworks\MultilangContentBundle\ContentInterface\PointedTypeTranslationInterface;
use Doctrine\ORM\Mapping as ORM;
use Error;

/**
 * @ORM\Entity(repositoryClass="Ateszworks\MultilangContentBundle\Repository\ArticleTranslationEntityRepository")
 * @ORM\Table(name="article_translations")
 */
class ArticleTranslationEntity extends TranslationFilterableEntity
    implements ContentTranslation, PointedTypeTranslationInterface
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var LangEntity
     * @ORM\ManyToOne(targetEntity="LangEntity")
     */
    protected $lang;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $title;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $urlSlug;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $content;

    /**
     * @var ArticleEntity
     * @ORM\ManyToOne(targetEntity="ArticleEntity", inversedBy="translations")
     */
    protected $article;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUrlSlug(): string
    {
        return $this->urlSlug;
    }

    /**
     * @param string $urlSlug
     */
    public function setUrlSlug(string $urlSlug): void
    {
        $this->urlSlug = $urlSlug;
    }

    /**
     * @param mixed $id
     * @return ArticleTranslationEntity
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return ArticleTranslationEntity
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return ArticleTranslationEntity
     */
    public function setContent(string $content): ArticleTranslationEntity
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return ArticleEntity
     */
    public function getArticle(): ?ArticleEntity
    {
        return $this->article;
    }

    /**
     * @param ArticleEntity $article
     * @return ArticleTranslationEntity
     */
    public function setArticle(ArticleEntity $article): ArticleTranslationEntity
    {
        $this->article = $article;
        return $this;
    }

    /**
     * @return LangEntity
     */
    public function getLang(): LangEntity
    {
        return $this->lang;
    }

    /**
     * @param LangEntity $lang
     * @return ArticleTranslationEntity
     */
    public function setLang(LangEntity $lang): ArticleTranslationEntity
    {
        $this->lang = $lang;
        return $this;
    }

    // PointedTypeTransationInterface

    /**
     * @return string
     * Returns the name of the property the query should look for.
     */
    public static function getFilterableFieldName(): string
    {
        return 'title';
    }

    /**
     * @return TranslationFilterableEntity
     */
    public function getTranslationOf(): TranslationFilterableEntity
    {
        return $this->getArticle();
    }

    /**
     * @return string
     * Returns content of the field that the query was triggered for.
     */
    public function getFilterableFieldContents(): string
    {
        return $this->getTitle();
    }

    // hacky - hacky way for solving form helper conformance & being a ContentTranslation:
    // todo: must solve it, it hurts SOLID principles in many ways :/ (Symfony made me do it!)
    public function __call($name, $arguments)
    {
        switch ($name) {
            case 'getFormLang':
                return $this->lang ? $this->lang : null;
            case 'setFormLang':
                if (!isset($arguments[0]) || !($arguments[0] instanceof LangEntity))
                    throw new Error('Wrong parameter in setter call!');
                $this->setLang($arguments[0]);
                break;
            default:
                throw new Error("Should not access non-existing properties!");
        }
    }
}