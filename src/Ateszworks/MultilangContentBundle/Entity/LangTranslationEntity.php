<?php

namespace Ateszworks\MultilangContentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class LangTranslationEntity
 * @package Ateszworks\MultilangContentBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name="language_translations")
 */
class LangTranslationEntity
{

    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     * Name of the language in the language concreted by $lang.
     */
    private $langName;

    /**
     * @var LangEntity
     * @ORM\ManyToOne(targetEntity="LangEntity", inversedBy="translations")
     */
    private $langEntity;

    /**
     * @var string
     * @ORM\Column(type="string",length=2)
     */
    private $lang;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLangName(): string
    {
        return $this->langName;
    }

    /**
     * @param string $langName
     * @return LangTranslationEntity
     */
    public function setLangName(string $langName): LangTranslationEntity
    {
        $this->langName = $langName;
        return $this;
    }

    /**
     * @return LangEntity
     */
    public function getLangEntity(): LangEntity
    {
        return $this->langEntity;
    }

    /**
     * @param LangEntity $langEntity
     */
    public function setLangEntity(LangEntity $langEntity): void
    {
        $this->langEntity = $langEntity;
    }

    /**
     * @return string
     */
    public function getLang(): string
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     * @return LangTranslationEntity
     */
    public function setLang(string $lang)
    {
        $this->lang = $lang;
        return $this;
    }
}