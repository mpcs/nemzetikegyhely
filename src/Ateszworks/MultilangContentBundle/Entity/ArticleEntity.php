<?php

namespace Ateszworks\MultilangContentBundle\Entity;

use Ateszworks\MultilangContentBundle\ContentInterface\PointedTypeResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ArticleEntity
 * @package Ateszworks\MultilangContentBundle\Entity
 * @ORM\Entity(repositoryClass="Ateszworks\MultilangContentBundle\Repository\ArticleEntityRepository")
 * @ORM\Table(name="article")
 * @ORM\HasLifecycleCallbacks()
 */
class ArticleEntity extends TranslationFilterableEntity implements PointedTypeResource
{

    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $isListed = true;

    /**
     * @var
     * @ORM\Column(type="string")
     */
    private $publisher = "admin";

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="ArticleTranslationEntity", mappedBy="article", cascade={"persist", "remove"})
     */
    protected $translations;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $published;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * Since it is a very common requirement, this bundle
     * supports attaching images by default.
     */
    private $imageUrl = null;

    /**
     * ArticleEntity constructor.
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return ArticleEntity
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return bool
     */
    public function isListed(): bool
    {
        return $this->isListed;
    }

    /**
     * @param bool $isListed
     * @return ArticleEntity
     */
    public function setIsListed(bool $isListed): ArticleEntity
    {
        $this->isListed = $isListed;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getPublisher(): string
    {
        return $this->publisher;
    }

    /**
     * @param mixed $publisher
     * @return ArticleEntity
     */
    public function setPublisher($publisher)
    {
        $this->publisher = $publisher;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getTranslations(): ?Collection
    {
        return $this->translations;
    }

    /**
     * @param Collection $translations
     * @return ArticleEntity
     */
    public function setTranslations(Collection $translations): ArticleEntity
    {
        $this->translations = $translations;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created): void
    {
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    /**
     * @param string $imageUrl
     */
    public function setImageUrl(string $imageUrl): void
    {
        $this->imageUrl = $imageUrl;
    }

    /**
     * @return \DateTime
     */
    public function getPublished(): ?\DateTime
    {
        return $this->published;
    }

    /**
     * @param \DateTime $published
     * @return ArticleEntity
     */
    public function setPublished(\DateTime $published): ArticleEntity
    {
        $this->published = $published;
        return $this;
    }

    // MARK: - Lifecycle
    /**
     * @ORM\PrePersist
     */
    public function onPrePersistSetCreated()
    {
        $this->created = new \DateTime();
        $this->published = $this->published ?? new \DateTime();
    }

}