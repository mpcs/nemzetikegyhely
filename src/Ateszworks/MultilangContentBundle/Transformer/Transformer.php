<?php

namespace Ateszworks\MultilangContentBundle\Transformer;

use Ateszworks\MultilangContentBundle\Entity\LangEntity;
use Ateszworks\MultilangContentBundle\Entity\TranslationFilterableEntity;
use Ateszworks\MultilangContentBundle\UserPresentable\UserPresentable;

/**
 * Interface Transformer
 * @package Ateszworks\MultilangContentBundle\Transformer
 * A transformer is an object that transforms a complex underlying object into an easily printable form.
 */
interface Transformer
{

    /**
     * @param TranslationFilterableEntity $entity
     * @param LangEntity $toLang
     * @return UserPresentable
     */
    function transform(TranslationFilterableEntity $entity, LangEntity $toLang): ?UserPresentable;

}