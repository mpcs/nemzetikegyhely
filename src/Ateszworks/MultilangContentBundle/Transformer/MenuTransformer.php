<?php

namespace Ateszworks\MultilangContentBundle\Transformer;

use Ateszworks\MultilangContentBundle\Entity\LangEntity;
use Ateszworks\MultilangContentBundle\Entity\MenuEntity;
use Ateszworks\MultilangContentBundle\Entity\MenuTranslationEntity;
use Ateszworks\MultilangContentBundle\Entity\TranslationFilterableEntity;
use Ateszworks\MultilangContentBundle\Resolver\TypeBasedRouteResolverService;
use Ateszworks\MultilangContentBundle\UserPresentable\PresentableMenu;
use Ateszworks\MultilangContentBundle\UserPresentable\UserPresentable;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;


/**
 * Class MenuTransformer
 * @package Ateszworks\MultilangContentBundle\Transformer
 */
class MenuTransformer implements Transformer
{

    /* @var TypeBasedRouteResolverService */
    private $typeBasedResolver;

    /* @var RouterInterface */
    private $router;

    /* @var array */
    private $wildcards = ['homepage', 'list_page',
        'contact_page', 'gallery_list'];


    /**
     * MenuTransformer constructor.
     * @param TypeBasedRouteResolverService $service
     * @param RouterInterface $router
     */
    public function __construct(TypeBasedRouteResolverService $service, RouterInterface $router)
    {
        $this->typeBasedResolver = $service;
        $this->router = $router;
    }

    /**
     * @param TranslationFilterableEntity $entity
     * @param LangEntity $toLang
     * @return UserPresentable|null
     */
    public function transform(TranslationFilterableEntity $entity, LangEntity $toLang): ?UserPresentable
    {
        if (!$entity instanceof MenuEntity) return null;
        $userPresentable = new PresentableMenu();

        /* @var MenuEntity $entity */
        /* @var MenuTranslationEntity $translation */
        $translation = $entity->getTranslationForLanguage($toLang) ? $entity->getTranslationForLanguage($toLang) : $entity->getTranslations()->first();
        if (!$translation) return null;

        // Whoops, one more step:
        if (MenuEntity::RESOURCE_POINTING_TYPE == $entity->getType()) {
            $routeResolver = $this->typeBasedResolver->getResourceManagerForResourceType($entity->getTarget());
            try {
                $userPresentable->setUrl($routeResolver->getRouteForResource($entity->getTargetIdentifier()));
            } catch (NotFoundHttpException $e) {
                // fallback logic:
                $userPresentable->setUrl('#');
            }

        } else {
            $target = $this->resolveSpecialRoute($entity->getTarget());
            $userPresentable->setUrl($target);
        }

        $userPresentable->setTitle($translation->getTitle());

        return $userPresentable;
    }

    /**
     * @param string $string
     * @return string
     */
    private function resolveSpecialRoute(string $string): string
    {
        if (strstr($string, ':') === false) return $string;
        $magic = explode(':', $string)[1];
        if (!in_array($magic, $this->wildcards)) return $string;

        return $this->router->generate($magic, [], UrlGeneratorInterface::ABSOLUTE_URL);
    }
}