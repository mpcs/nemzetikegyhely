<?php

namespace Ateszworks\MultilangContentBundle\Transformer;


use Ateszworks\MultilangContentBundle\Entity\ArticleTranslationEntity;
use Ateszworks\MultilangContentBundle\Entity\LangEntity;
use Ateszworks\MultilangContentBundle\Entity\TranslationFilterableEntity;
use Ateszworks\MultilangContentBundle\UserPresentable\PresentableArticle;
use Ateszworks\MultilangContentBundle\UserPresentable\UserPresentable;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class ArticleTransformer
 * @package Ateszworks\MultilangContentBundle\Transformer
 */
class ArticleTransformer implements Transformer
{
    /* @var RouterInterface */
    private $router;

    /**
     * ArticleTransformer constructor.
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param TranslationFilterableEntity $entity
     * @param LangEntity $toLang
     * @return UserPresentable|null
     */
    public function transform(TranslationFilterableEntity $entity, LangEntity $toLang): ?UserPresentable
    {
        /* @var ArticleTranslationEntity $entity */
        $presentableArticle = new PresentableArticle();
        $parentArticle = $entity->getArticle();
        $published = $parentArticle->getPublished() ?? $parentArticle->getCreated();

        $coverImage = $parentArticle->getImageUrl() ? $this->router->generate('image',
            ['name' => $parentArticle->getImageUrl()], UrlGeneratorInterface::ABSOLUTE_URL) : null;
        $presentableArticle->setTitle($entity->getTitle());
        $presentableArticle->setCoverImage($coverImage);
        $presentableArticle->setAuthor($parentArticle->getPublisher());
        $presentableArticle->setContents($entity->getContent());
        $presentableArticle->setCreated($parentArticle->getCreated()->format('Y. m. d.'));
        $presentableArticle->setPublished($published->format('Y. m. d.'));

        return $presentableArticle;
    }


}