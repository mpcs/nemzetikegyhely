<?php

namespace Ateszworks\MultilangContentBundle\DependencyInjection;

use Ateszworks\MultilangContentBundle\ContentInterface\PointedTypeRouteResolver;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;

class AteszworksMultilangContentExtension extends Extension
{

    /**
     * Loads a specific configuration.
     *
     * @param array $configs
     * @param ContainerBuilder $container
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        /** @var Configuration $configuration */
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('ateszworks_multilang_content.should_store_lang_in_session',
            $config['language_provider']['should_store_lang_in_session']);

        $container->setParameter('ateszwork_multilang_content.article_display_route_name',
            $config['article_display_route']);

        $container->setParameter('ateszworks_multilang_content.storage_dir',
            $config['storage_directory']);

        $container->setParameter('ateszworks_multilang_content.image.process_on_save',
            $config['image']['rotate_image_by_exif_on_upload']);

        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__ . '/../Resources/config')
        );
        $loader->load('services.yaml');

        // It now even has autoconfiguration! :3 how cool
        $container->registerForAutoconfiguration(PointedTypeRouteResolver::class)
            ->addTag('aw.route_resolver');

    }
}