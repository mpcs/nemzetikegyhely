<?php

namespace Ateszworks\MultilangContentBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    /**
     * Generates the configuration tree builder.
     *
     * @return \Symfony\Component\Config\Definition\Builder\TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        /** @var $treeBuilder TreeBuilder */
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('ateszworks_multilang_content');

        $rootNode
            ->children()
                ->arrayNode('language_provider')
                    ->children()
                            ->booleanNode('should_store_lang_in_session')
                            ->defaultFalse()
                            ->end()
                    ->end()
                ->end() // language_provider
                ->scalarNode('article_display_route')
                    ->info('Name of the the bundle calls to display an article.')
                    ->defaultNull()
                ->end() // article_display_route
                ->scalarNode('storage_directory')
                    ->info('The folder the bundle should store its files in.')
                    ->defaultValue('%kernel.project_dir%/public/uploads/aw_mlcb_files')
                ->end() // storage_directory
                ->arrayNode('image')
                    ->children()
                        ->booleanNode('rotate_image_by_exif_on_upload')
                        ->defaultFalse()
                        ->end()
                    ->end()
                ->end() // image
            ->end();

        return $treeBuilder;
    }
}