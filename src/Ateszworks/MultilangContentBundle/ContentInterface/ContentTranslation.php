<?php

namespace Ateszworks\MultilangContentBundle\ContentInterface;

use Ateszworks\MultilangContentBundle\Entity\LangEntity;

/**
 * Interface ContentTranslation
 * @package Ateszworks\MultilangContentBundle\Entity
 * To use LangEntity was a necessary since you may want to know
 * language names in different languages.
 */
interface ContentTranslation
{

    /**
     * @return LangEntity
     */
    public function getLang(): LangEntity;

    /**
     * @param LangEntity $lang
     */
    public function setLang(LangEntity $lang);

    /**
     * @return string
     */
    public function getContent(): ?string;

}