<?php

namespace Ateszworks\MultilangContentBundle\ContentInterface;

use Doctrine\Common\Collections\Collection;

interface Translatable
{

    /**
     * @return Collection
     */
    public function getTranslations(): ?Collection;

    /**
     * @param Collection $translations
     */
    public function setTranslations(Collection $translations);

}