<?php

namespace Ateszworks\MultilangContentBundle\ContentInterface;
use Ateszworks\MultilangContentBundle\Entity\LangEntity;
use Ateszworks\MultilangContentBundle\Entity\TranslationFilterableEntity;
use Ateszworks\MultilangContentBundle\UserPresentable\UserPresentable;


/**
 * Interface UserPresentableProvier
 * @package Ateszworks\MultilangContentBundle\ContentInterface
 * An object that is a UserPresentableProvider has to be able to
 * provide fully translated and user presentation ready objects.
 */
interface UserPresentableProvier
{

    /**
     * @param TranslationFilterableEntity $fromEntity
     * @param LangEntity $inLang
     * @return UserPresentable|null
     */
    public function getPresentable(TranslationFilterableEntity $fromEntity, LangEntity $inLang) : ?UserPresentable;

    /**
     * @param array $entities
     * @param LangEntity $inLang
     * @return array|null
     */
    public function getPresentables(array $entities, LangEntity $inLang): ?array;

    /**
     * @param int $from
     * @param int $amount
     * @param LangEntity $inLang
     * @return array|null
     */
    public function getPresentablesPaged(int $from, int $amount, LangEntity $inLang): ?array;
}