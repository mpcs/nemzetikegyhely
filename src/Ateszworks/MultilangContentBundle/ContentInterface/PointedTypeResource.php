<?php

namespace Ateszworks\MultilangContentBundle\ContentInterface;


/**
 * Interface PointedTypeResource
 * @package Ateszworks\MultilangContentBundle\ContentInterface
 */
interface PointedTypeResource
{

    /**
     * @return mixed
     */
    public function getId();

}