<?php

namespace Ateszworks\MultilangContentBundle\ContentInterface;

use Ateszworks\MultilangContentBundle\Repository\TranslatableResourceRepository;
use Ateszworks\MultilangContentBundle\Repository\TranslationQueryableRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;


/**
 * Interface ContentProviderInterface
 * @package Ateszworks\MultilangContentBundle\Provider
 * If for whatever reason you would like to replace original content provider,
 * you can do so by creating one that implements this interface.
 * Basic requirements are put in stone by this.
 */
interface ContentProviderInterface
{

    /**
     * @param $entity
     */
    public function setManagedContentType($entity): void;

    /**
     * @param $entity
     */
    public function setManagedContentTranslationType($entity): void;

    /**
     * @param array $array
     */
    public function setCriteriaArray(array $array = []): void;

    /**
     * @param array $order
     */
    public function setOrderingForPackedRequest(array $order): void;

    /**
     * Returns all matched entities of ManagedContentType.
     * @return array
     */
    public function getAll(): array;

    /**
     * @param int $id
     * @return mixed
     */
    public function getEntityById(int $id);

    /**
     * @return Entities with currently selected language.
     */
    public function getEntitiesWithAppropriateTranslation();

    /**
     * @param int $id
     * @return ContentTranslation
     */
    public function getTranslationEntityForId(int $id): ContentTranslation;

    /**
     * @param int $from
     * @param int $amount
     * @param array $order
     * @return mixed
     */
    public function getEntitiesInPack(int $from, int $amount, array $order);

    /**
     * @param int $from
     * @param int $amount
     * @return array
     */
    public function getTranslationEntitiesInPack(int $from, int $amount): array;

    /**
     * @param $entity
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function saveByEntity($entity);

    /**
     * @param $entity
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateByEntity($entity);

    /**
     * @param $entity
     */
    public function removeByEntity($entity);

    /**
     * @return TranslatableResourceRepository
     * @throws \Exception
     */
    public function getRepositoryReference(): TranslatableResourceRepository;

    /**
     * @return TranslationQueryableRepository
     */
    public function getTranslationRepositoryReference(): TranslationQueryableRepository;

    /**
     * @return Translatable
     */
    public function getTranslationForCurrentLang(): Translatable;

    /**
     * @param int $entityId
     * @param string $langCode
     * @return ContentTranslation|null
     */
    public function getTranslationForEntityByLangCode(int $entityId, string $langCode): ?ContentTranslation;

    /**
     * @return Translatable
     */
    public function getEntityWithAppropriateTranslation(): Translatable;

    /**
     * @param int $id
     * @param string $langCode
     * @return mixed
     */
    public function getTranslationForEntityByLangCodeWithFallback(int $id, string $langCode): ?ContentTranslation;

}