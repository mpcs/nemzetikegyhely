<?php

namespace Ateszworks\MultilangContentBundle\ContentInterface;


use Ateszworks\MultilangContentBundle\Entity\TranslationFilterableEntity;

/**
 * Interface PointedTypeTranslationInterface
 * @package Ateszworks\MultilangContentBundle\ContentInterface
 */
interface PointedTypeTranslationInterface
{

    /**
     * @return string
     * Returns the name of the property the query should look for.
     */
    public static function getFilterableFieldName(): string;

    /**
     * @return string
     * Returns content of the field that the query was triggered for.
     */
    public function getFilterableFieldContents(): string;

    /**
     * @return TranslationFilterableEntity The resource, of which this is the translation of.
     */
    public function getTranslationOf(): TranslationFilterableEntity;

}