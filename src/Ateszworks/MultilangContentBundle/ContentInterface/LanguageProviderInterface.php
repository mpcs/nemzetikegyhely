<?php

namespace Ateszworks\MultilangContentBundle\ContentInterface;

use Ateszworks\MultilangContentBundle\Entity\LangEntity;

/**
 * Interface LanguageProviderInterface
 * @package Ateszworks\MultilangContentBundle\Provider
 * This service should determine which language is used by the user, set as default, etc.
 */
interface LanguageProviderInterface
{

    /**
     * @return LangEntity
     */
    public function getCurrentLanguage(): LangEntity;

    /**
     * @return LangEntity
     */
    public function getDefaultLanguage(): LangEntity;

    /**
     * @return LangEntity[]
     */
    public function getAllAvailableLanguages(): array;

    /**
     * @param string $code
     * @return LangEntity
     */
    public function getLangByCode(string $code): ?LangEntity;

    /**
     * @param int $id
     * @return LangEntity|null
     */
    public function getLangById(int $id): ?LangEntity;
    
    /**
     * @return array|null
     */
    public function getLanguagesForSwitcher(): ?array;

}