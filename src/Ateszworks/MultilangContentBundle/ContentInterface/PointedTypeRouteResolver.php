<?php

namespace Ateszworks\MultilangContentBundle\ContentInterface;

/**
 * Interface PointedTypeRouteResolver
 * @package Ateszworks\MultilangContentBundle\ContentInterface
 */
interface PointedTypeRouteResolver
{

    /**
     * @return string
     */
    public function getPointedType(): string;

    /**
     * @param string $id
     * @return string
     */
    public function getRouteForResource(string $id): string;

    /**
     * @return string
     * Returns fully qualified class name of the entity that
     * the class/object implements this managing.
     */
    public function getManagedResourceTranslationClassName(): string;

    /**
     * @return string
     * Returns the name of the property of the entity the query should look for the
     * queried string in.
     */
    public function getFieldForQuerying(): string;

}