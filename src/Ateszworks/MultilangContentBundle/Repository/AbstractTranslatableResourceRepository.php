<?php

namespace Ateszworks\MultilangContentBundle\Repository;

use Ateszworks\MultilangContentBundle\Entity\LangEntity;
use Doctrine\ORM\EntityRepository;

/**
 * Class AbstractTranslatableResourceRepository
 * @package Ateszworks\MultilangContentBundle\Repository
 */
abstract class AbstractTranslatableResourceRepository extends EntityRepository implements TranslatableResourceRepository
{

    /* @var array */
    private $order = ['id' => 'DESC'];

    /* @var array */
    private $criteria = [];

    /* @param array $order */
    public function setOrder(array $order): void
    {
        $this->order = $order;
    }

    /* @param array $criteria */
    public function setCriteria(array $criteria): void
    {
        $this->criteria = $criteria;
    }

    /**
     * @param LangEntity|null $withLangFilter
     * @param LangEntity|null $fallbackLang
     * @return mixed ... A query result object
     */
    public function getEntityAndTranslations(?LangEntity $withLangFilter = null, ?LangEntity $fallbackLang = null)
    {
        $orderKey = array_keys($this->order)[0];
        $qb = $this->createQueryBuilder('tableAlias');
        if ($withLangFilter) $qb->where('tableAlias.lang = :lang')->setParameter('lang', $withLangFilter);

        $res = $qb->getQuery()->getResult();

        if (count($res) == 0 && $fallbackLang) {
            return $qb->where('tableAlias.lang = :lang')
                ->setParameter('lang', $fallbackLang)
                ->orderBy('tableAlias.' . $orderKey, $this->order[$orderKey])
                ->getQuery()
                ->getResult();
        }

        $this->reset();
        return $res;
    }

    /**
     * @param LangEntity $concreteLanguage
     * @param int|null $from
     * @param int|null $amount
     * @return mixed
     */
    public function getEntitiesWithTranslationByLang(LangEntity $concreteLanguage, ?int $from = 0, ?int $amount = null)
    {
        $orderKey = array_keys($this->order)[0];
        $qb = $this->createQueryBuilder('tableAlias')
            ->leftJoin('tableAlias.translations', 't', 'WITH', 't.lang = :lang')
            ->setParameter('lang', $concreteLanguage)
            ->orderBy('tableAlias.' . $orderKey, $this->order[$orderKey]);

        if (count($this->criteria) > 0) {
            foreach ($this->criteria as $key => $val) {
                $qb->andWhere($qb->expr()->eq('tableAlias.' . $key, ':' . $key))
                    ->setParameter($key, $val);
            }
        }

        if ($from) $qb->setFirstResult($from);
        if ($amount) $qb->setFirstResult($amount);

        $this->reset();
        return $qb->getQuery()->getResult();
    }


    /**
     * Resets criteria and order.
     */
    private function reset(): void
    {
        $this->criteria = [];
        $this->order = ['id' => 'DESC'];
    }
}