<?php

namespace Ateszworks\MultilangContentBundle\Repository;

use App\Ateszworks\MultilangContentBundle\Repository\FilterableRepository;
use Ateszworks\MultilangContentBundle\Entity\LangEntity;

/**
 * Interface TranslatableResourceRepository
 * @package App\Ateszworks\MultilangContentBundle\Entity
 */
interface TranslatableResourceRepository extends OrderableRepository, FilterableRepository
{

    /**
     * @param LangEntity|null $withLangFilter
     * @param LangEntity|null $fallbackLang
     * @return mixed ... A query result object
     */
    public function getEntityAndTranslations(?LangEntity $withLangFilter = null, ?LangEntity $fallbackLang = null);

    /**
     * @param LangEntity $concreteLanguage
     * Returns entities with translation set by injected LangEntity.
     * @param int|null $from
     * @param int|null $amount
     * @return mixed
     */
    public function getEntitiesWithTranslationByLang(LangEntity $concreteLanguage, ?int $from = 0, ?int $amount = null);
}