<?php

namespace App\Ateszworks\MultilangContentBundle\Repository;


/**
 * Interface FilterableRepository
 * @package App\Ateszworks\MultilangContentBundle\Repository
 */
interface FilterableRepository
{

    /**
     * Setting criteria for query.
     * @param array $criteria
     */
    public function setCriteria(array $criteria): void;

}