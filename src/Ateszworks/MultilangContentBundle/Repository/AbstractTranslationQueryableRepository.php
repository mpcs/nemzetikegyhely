<?php

namespace Ateszworks\MultilangContentBundle\Repository;

use Ateszworks\MultilangContentBundle\ContentInterface\Translatable;
use Ateszworks\MultilangContentBundle\Entity\LangEntity;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Intl\Exception\NotImplementedException;

abstract class AbstractTranslationQueryableRepository extends EntityRepository implements TranslationQueryableRepository
{

    /**
     * @var array
     */
    private $order = ['id' => 'DESC'];

    /**
     * @param array $order
     * Used for setting ordering in repository
     */
    public function setOrder(array $order): void
    {
        $this->order = $order;
    }

    /**
     * @param int $from
     * @param int $amount
     * @param LangEntity|null $optionalLang
     * @return array
     */
    public function getTranslationsInPacksByLang(?int $from = 0, ?int $amount, ?LangEntity $optionalLang): array
    {
        $orderKey = array_keys($this->order)[0];
        $qb = $this->createQueryBuilder('tableAlias')
            ->setMaxResults($amount)
            ->setFirstResult($from)
            ->orderBy($orderKey, $this->order[$orderKey]);

        if ($optionalLang) $qb->andWhere('tableAlias.lang = :lang')->setParameter('lang', $optionalLang);

        return $qb->getQuery()->getResult();
    }

    /**
     * @return Translatable
     */
    public function getTranslationForCurrentLang(): Translatable
    {
        throw new NotImplementedException('Will be implemented.');
//        $qb = $this->createQueryBuilder('tAlias')
//            ->andWhere('tAlias.lang = :lang')
//            ->setParameter('lang', 'hu')
//            ->setMaxResults(1);
//
//        $firstTry = $qb->getQuery()->getResult();
    }

}