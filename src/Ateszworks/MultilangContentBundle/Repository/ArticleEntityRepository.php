<?php

namespace Ateszworks\MultilangContentBundle\Repository;

use Ateszworks\MultilangContentBundle\Entity\ArticleEntity;
use Ateszworks\MultilangContentBundle\Entity\LangEntity;
use Doctrine\ORM\Query\Expr\Join;

/**
 * ArticleEntityRepository
 */
class ArticleEntityRepository extends AbstractTranslatableResourceRepository
{

    /**
     * @param LangEntity $inLang
     * @param bool $isListable
     * @return array|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getLatestArticle(LangEntity $inLang, bool $isListable = true): ?ArticleEntity
    {
        $qb = $this->createQueryBuilder('article');
        $qb->andWhere($qb->expr()->eq('article.isListed', ':isListed'))
            ->innerJoin('article.translations', 't',
                Join::WITH, $qb->expr()->eq('t.lang', ':lang'))
            ->setParameter('lang', $inLang)
            ->setParameter('isListed', $isListable)
            ->setMaxResults(1)
            ->orderBy('article.created', 'DESC');

        return $qb->getQuery()->getOneOrNullResult();
    }

}
