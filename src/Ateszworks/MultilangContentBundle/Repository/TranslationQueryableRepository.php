<?php

namespace Ateszworks\MultilangContentBundle\Repository;

use Ateszworks\MultilangContentBundle\ContentInterface\Translatable;
use Ateszworks\MultilangContentBundle\Entity\LangEntity;

interface TranslationQueryableRepository extends OrderableRepository
{

    /**
     * @param int $from
     * @param int $amount
     * @param LangEntity|null $optionalLang
     * @return array
     */
    public function getTranslationsInPacksByLang(?int $from = null, ?int $amount, ?LangEntity $optionalLang): array;

    /**
     * Gets corresponding translation for current language.
     * @return Translatable
     */
    public function getTranslationForCurrentLang(): Translatable;

}