<?php

namespace App\Ateszworks\MultilangContentBundle\Repository;


/**
 * Interface ResettableRepository
 * @package App\Ateszworks\MultilangContentBundle\Repository
 */
interface ResettableRepository
{

    /**
     * Use it to restore original state.
     */
    public function reset(): void;

}