<?php

namespace Ateszworks\MultilangContentBundle\Repository;


interface OrderableRepository
{
    /**
     * @param array $order
     * Used for setting ordering in repository
     */
    public function setOrder(array $order): void;
}