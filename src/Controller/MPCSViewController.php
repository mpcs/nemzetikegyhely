<?php

namespace App\Controller;

use Ateszworks\MultilangContentBundle\ContentInterface\LanguageProviderInterface;
use Ateszworks\MultilangContentBundle\Manager\MenuManager;


/**
 * Class MPCSViewController
 * @package App\Controller
 */
class MPCSViewController extends MPCSMetaAwareController
{

    /* @var MenuManager */
    protected $menuManager;

    /* @var LanguageProviderInterface */
    protected $languageProvider;

    /* @var array */
    protected $commonViewInfo;

    /**
     * MPCSViewController constructor.
     * @param MenuManager $menuManager
     * @param LanguageProviderInterface $languageProvider
     */
    public function __construct(MenuManager $menuManager, LanguageProviderInterface $languageProvider)
    {
        $this->menuManager = $menuManager;
        $this->languageProvider = $languageProvider;
        $this->commonViewInfo = [
            'menus' => $menuManager->getAll(),
            'langs' => $languageProvider->getLanguagesForSwitcher(),
            'currentLanguage' => $languageProvider->getCurrentLanguage(),
            'currentLangCode' => $languageProvider->getCurrentLanguage()->getCode()
        ];
    }

}