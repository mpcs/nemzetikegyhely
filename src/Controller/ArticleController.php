<?php

namespace App\Controller;


use App\Manager\MPCSArticleManager;
use App\Utils\MpcsUtils;
use Ateszworks\MultilangContentBundle\ContentInterface\LanguageProviderInterface;
use Ateszworks\MultilangContentBundle\Manager\MenuManager;
use Ateszworks\MultilangContentBundle\UserPresentable\PresentableArticle;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class ArticleController extends MPCSViewController
{

    /* @var MPCSArticleManager */
    private $mpcsArticleManager;

    /**
     * ArticleController constructor.
     * @param MenuManager $menuManager
     * @param LanguageProviderInterface $languageProvider
     * @param MPCSArticleManager $articleManager
     */
    public function __construct(MenuManager $menuManager,
                                LanguageProviderInterface $languageProvider,
                                MPCSArticleManager $articleManager)
    {
        parent::__construct($menuManager, $languageProvider);
        $this->mpcsArticleManager = $articleManager;
    }

    /**
     * @return Response
     */
    public function listArticles(): Response
    {
        return $this->render('main/list.html.twig', array_merge(
            $this->commonViewInfo,
            ['articles' => MpcsUtils::separate($this->mpcsArticleManager->getAllWithRoutes(true))]
        ));
    }

    /**
     * @param Request $request
     * @param RouterInterface $router
     * @param LanguageProviderInterface $languageProvider
     * @return Response
     */
    public function viewArticle(Request $request,
                                RouterInterface $router,
                                LanguageProviderInterface $languageProvider): Response
    {
        $slug = $request->attributes->get('article_slug');
        /* @var PresentableArticle $article */
        $article = $this->mpcsArticleManager->getArticleWithImagesBySlug($slug);
        $viewInfo = array_merge($this->commonViewInfo, [
            'article' => $article,
            'shareUrl' => $router->generate('localized_view_article', [
                'article_slug' => $slug,
                '_locale' => $languageProvider->getCurrentLanguage()->getCode()
            ], UrlGeneratorInterface::ABSOLUTE_URL)
        ]);
        $this->emitWillDisplayEvent($article);

        return $this->render('main/typographic.html.twig', $viewInfo);
    }
}