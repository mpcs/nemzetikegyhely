<?php

namespace App\Controller;

use App\Manager\MPCSGalleryImageManager;
use App\Manager\MPCSGalleryManager;
use Ateszworks\MultilangContentBundle\ContentInterface\LanguageProviderInterface;
use Ateszworks\MultilangContentBundle\Manager\MenuManager;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class GalleryController
 * @package App\Controller
 */
class GalleryController extends MPCSViewController
{
    /**
     * GalleryController constructor.
     * @param MenuManager $menuManager
     * @param LanguageProviderInterface $languageProvider
     */
    public function __construct(MenuManager $menuManager, LanguageProviderInterface $languageProvider)
    {
        parent::__construct($menuManager, $languageProvider);
    }


    /**
     * @param MPCSGalleryManager $manager
     * @return Response
     */
    public function listGalleries(MPCSGalleryManager $manager): Response
    {
        return $this->render('main/galleries.html.twig', array_merge(
            $this->commonViewInfo,
            ['galleries' => $manager->getPresentableGalleries()]
        ));
    }

    /**
     * @param Request $request
     * @param MPCSGalleryManager $galleryManager
     * @param RouterInterface $router
     * @param LanguageProviderInterface $languageProvider
     * @return Response
     */
    public function viewGallery(Request $request,
                                MPCSGalleryManager $galleryManager,
                                RouterInterface $router,
                                LanguageProviderInterface $languageProvider): Response
    {
        $slug = $request->get('url_slug');
        $data = $galleryManager->getPresentableBySlug($slug);
        $this->emitWillDisplayEvent($data['translation']);
        return $this->render('main/gallery.html.twig', array_merge(
            $this->commonViewInfo,
            $data,
            [
                'shareUrl' => $router->generate('gallery', [
                    '_locale' => $languageProvider->getCurrentLanguage()->getCode(),
                    'url_slug' => $slug
                ], UrlGeneratorInterface::ABSOLUTE_URL)
            ]
        ));
    }

    /**
     * @param Request $request
     * @param MPCSGalleryManager $galleryManager
     * @param MPCSGalleryImageManager $imageManager
     * @return BinaryFileResponse
     */
    public function viewGalleryImage(Request $request,
                                     MPCSGalleryManager $galleryManager,
                                     MPCSGalleryImageManager $imageManager): BinaryFileResponse
    {
        $urlSlug = $request->get('url_slug');
        $imageName = $request->get('image_name');
        $gallery = $galleryManager->getTranslationWithFallback($urlSlug)->getGallery();

        return new BinaryFileResponse($imageManager->getImagePath($gallery, $imageName));
    }

}