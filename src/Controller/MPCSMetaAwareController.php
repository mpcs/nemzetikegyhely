<?php

namespace App\Controller;


use App\Event\WillDisplayContentEvent;
use App\Manager\MPCSMetaManager;
use Ateszworks\MultilangContentBundle\UserPresentable\UserPresentable;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class MPCSMetaAwareController
 * @package App\Controller
 */
class MPCSMetaAwareController extends Controller
{

    /* @var EventDispatcherInterface */
    protected $dispatcher;

    /* @var MPCSMetaManager */
    protected $metaManager;

    /* @var LoggerInterface */
    protected $logger;

    /**
     * @param string $view
     * @param array $parameters
     * @param Response|null $response
     * @return Response
     */
    protected function render(string $view, array $parameters = array(), Response $response = null): Response
    {
        $this->handleDependencies();
        $parameters = array_merge($parameters, $this->metaManager->getAsArray());
        return parent::render($view, $parameters, $response);
    }

    /**
     * @param string $view
     * @param array $parameters
     * @return string
     */
    protected function renderView(string $view, array $parameters = array()): string
    {
        $this->handleDependencies();
        $parameters = array_merge($parameters, $this->metaManager->getAsArray());
        return parent::renderView($view, $parameters);
    }

    public function setDependencies(EventDispatcherInterface $dispatcher, MPCSMetaManager $manager, LoggerInterface $logger): void
    {
        $this->dispatcher = $dispatcher;
        $this->metaManager = $manager;
        $this->logger = $logger;
    }

    /**
     * Emits willDisplayContent event with supplied UserPresentable with automatic conversion.
     * @param UserPresentable $content
     */
    protected function emitWillDisplayEvent(UserPresentable $content): void
    {
        $this->handleDependencies();
        if (!$this->metaManager || !$this->dispatcher) return;

        $metaReadable = $this->metaManager->autoTransformContent($content);
        if (!$metaReadable) {
            $this->logger->error('Could not transform content.');
            return;
        }

        $event = new WillDisplayContentEvent($metaReadable);
        $this->dispatcher->dispatch('content.will.display', $event);
    }

    /**
     * Associates necessary dependencies for methods. This happens here,
     * so I can make it truly transparent for above layers.
     */
    private function handleDependencies(): void
    {
        if ($this->metaManager && $this->dispatcher && $this->logger) return;
        $helper = $this->container->get('mpcs.controller.meta_aware_helper');

        $this->metaManager = $this->metaManager ?? $helper->getMetaManager();
        $this->dispatcher = $this->dispatcher ?? $helper->getDispatcher();
        $this->logger = $this->logger ?? $helper->getLogger();
    }

}