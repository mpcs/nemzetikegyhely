<?php

namespace App\Controller;


use App\Manager\MPCSMetaManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class MetaAwareHelper
{

    /* @var EventDispatcherInterface */
    private $dispatcher;

    /* @var MPCSMetaManager */
    private $metaManager;

    /* @var LoggerInterface */
    private $logger;

    /**
     * MetaAwareHelper constructor.
     * @param EventDispatcherInterface $dispatcher
     * @param MPCSMetaManager $metaManager
     * @param LoggerInterface $logger
     */
    public function __construct(EventDispatcherInterface $dispatcher, MPCSMetaManager $metaManager,
                                LoggerInterface $logger)
    {
        $this->dispatcher = $dispatcher;
        $this->metaManager = $metaManager;
        $this->logger = $logger;
    }

    /**
     * @return EventDispatcherInterface
     */
    public function getDispatcher(): EventDispatcherInterface
    {
        return $this->dispatcher;
    }

    /**
     * @return MPCSMetaManager
     */
    public function getMetaManager(): MPCSMetaManager
    {
        return $this->metaManager;
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

}