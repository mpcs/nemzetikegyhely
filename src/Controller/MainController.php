<?php

namespace App\Controller;

use App\Manager\MPCSArticleManager;
use App\Manager\MPCSCarouselManager;
use App\Manager\MPCSFileManager;
use App\Manager\MPCSGalleryImageManager;
use App\Manager\MPCSGalleryManager;
use Ateszworks\MultilangContentBundle\ContentInterface\LanguageProviderInterface;
use Ateszworks\MultilangContentBundle\Manager\MenuManager;
use Ateszworks\MultilangContentBundle\UserPresentable\PresentableArticle;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Class MainController
 * @package Controller
 */
class MainController extends MPCSViewController
{

    /**
     * MainController constructor.
     * @param MenuManager $menuManager
     * @param LanguageProviderInterface $languageProvider
     */
    public function __construct(MenuManager $menuManager,
                                LanguageProviderInterface $languageProvider)
    {
        parent::__construct($menuManager, $languageProvider);
    }

    /**
     * Main action for catching requests.
     * @param MPCSCarouselManager $carouselManager
     * @param MPCSArticleManager $articleManager
     * @return Response
     * @throws \Exception
     */
    public function main(MPCSCarouselManager $carouselManager, MPCSArticleManager $articleManager): Response
    {
        $latestArticle = $articleManager->getLatestArticleWithRoute();
        return $this->render('main/homepage.html.twig', array_merge($this->commonViewInfo, [
            'latestArticle' => $latestArticle['article'],
            'latestArticleLink' => $latestArticle['route'],
            'carouselItems' => $carouselManager->getAllPresentables()
        ]));
    }

    /**
     * Serves a static file with the correct mime-type.
     * @param string $file_name
     * @param MPCSFileManager $fileManager
     * @return BinaryFileResponse
     */
    public function serveStaticFile(string $file_name, MPCSFileManager $fileManager)
    {
        $file = $fileManager->serveStaticFile($file_name);
        $response = new BinaryFileResponse($file->getPathname());

        // Set headers
        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-Type', $file->getMimeType());
        $response->headers->set('Content-Disposition', $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $file->getFilename()
        ));

        return $response;
    }

    /**
     * @return Response
     */
    public function contactPage(): Response
    {
        return $this->render('main/contact.html.twig', $this->commonViewInfo);
    }

    /**
     * @param string $name
     * @param bool $fromBundle
     * @param MPCSFileManager $fileManager
     * @return BinaryFileResponse
     */
    public function viewImage(string $name, bool $fromBundle = true, MPCSFileManager $fileManager): BinaryFileResponse
    {
        $image = $fileManager->getResourceUrl($name, $fromBundle);
        return new BinaryFileResponse($image);
    }
}