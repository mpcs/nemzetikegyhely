<?php

namespace App\Transformer;


use App\MetaReadable\GeneralMetaReadable;
use App\MetaReadable\MetaReadableContent;
use App\MetaReadable\MetaReadableContentTransformer;
use App\UserPresentable\MPCSPresentableArticle;
use App\Utils\MpcsUtils;
use Ateszworks\MultilangContentBundle\Entity\ArticleTranslationEntity;
use Ateszworks\MultilangContentBundle\UserPresentable\PresentableArticle;
use Ateszworks\MultilangContentBundle\UserPresentable\UserPresentable;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class ArticleMetaReadableTransformer
 * @package App\Transformer
 */
class ArticleMetaReadableTransformer implements MetaReadableContentTransformer
{

    /* @var RouterInterface */
    protected $router;

    /**
     * ArticleMetaReadableTransformer constructor.
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param UserPresentable $presentableArticle
     * @return MetaReadableContent
     */
    public function transform(UserPresentable $presentableArticle): MetaReadableContent
    {
        /* @var $presentableArticle PresentableArticle */
        return (new GeneralMetaReadable())
            ->setOgTitle($presentableArticle->getTitle())
            ->setOgDescription(MpcsUtils::introText($presentableArticle->getContents()))
            ->setOgImage($presentableArticle->getCoverImage())
            ->setOgUrl(null)// todo: should use router!
            ->setTwitterTitle($presentableArticle->getTitle())
            ->setTwitterDescription(MpcsUtils::introText($presentableArticle->getContents(), 200))
            ->setTwitterCard('summary_large_image')// todo: introduce consts in mpcsmetamanager
            ->setTwitterImage($presentableArticle->getCoverImage());
    }

    /**
     * Returns the FQCN of meta type it transforms. Used as id, must be unique.
     * @return array
     */
    public function getAssociatedMetaType(): array
    {
        return [PresentableArticle::class, MPCSPresentableArticle::class];
    }
}