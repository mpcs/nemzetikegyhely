<?php

namespace App\Transformer;


use App\Entity\CarouselPage;
use App\Entity\CarouselPageTranslation;
use App\Manager\MPCSFileManager;
use App\UserPresentable\PresentableCarouselPage;
use App\Utils\MpcsUtils;
use Ateszworks\MultilangContentBundle\ContentInterface\LanguageProviderInterface;
use Ateszworks\MultilangContentBundle\Entity\LangEntity;
use Ateszworks\MultilangContentBundle\Entity\TranslationFilterableEntity;
use Ateszworks\MultilangContentBundle\Transformer\Transformer;
use Ateszworks\MultilangContentBundle\UserPresentable\UserPresentable;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class CarouselTransformer
 * @package App\Transformer
 */
class CarouselTransformer implements Transformer
{

    /* @var LanguageProviderInterface */
    private $languageProvider;

    /* @var MPCSFileManager */
    private $fileManager;

    /**
     * CarouselTransformer constructor.
     * @param LanguageProviderInterface $languageProvider
     */
    public function __construct(LanguageProviderInterface $languageProvider, MPCSFileManager $manager)
    {
        $this->languageProvider = $languageProvider;
        $this->fileManager = $manager;
    }

    /**
     * @param TranslationFilterableEntity $entity
     * @param LangEntity $toLang
     * @return UserPresentable
     */
    function transform(TranslationFilterableEntity $entity, LangEntity $toLang): ?UserPresentable
    {
        /* @var $entity CarouselPage */
        /* @var $translation CarouselPageTranslation */
        $defaultLang = $this->languageProvider->getDefaultLanguage();
        $translation = $entity->getTranslationForLanguage($toLang)
            ?? $entity->getTranslationForLanguage($defaultLang);
        $smallImageInB64 = null;

        try {
            $smallLocation = $this->fileManager->getFullFileURL(MpcsUtils::getSmallNameFromNormal($entity->getImage()));
            $smallImageInB64 = MpcsUtils::getSmallFileAsB64($smallLocation);
        } catch (NotFoundHttpException $e) {
            $smallImageInB64 = null;
        }

        return (new PresentableCarouselPage())
            ->setImageUrl($entity->getImage())
            ->setSmallImage($smallImageInB64)
            ->setContent($translation ? $translation->getContent() : null);
    }
}