<?php

namespace App\Transformer;


use Ateszworks\MultilangContentBundle\Transformer\Transformer;

interface ImageTransformerInterface extends Transformer
{
    public function customTransform($param1 = null, $param2 = null);
}