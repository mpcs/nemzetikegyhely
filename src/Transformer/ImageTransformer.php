<?php

namespace App\Transformer;

use App\Entity\ArticleAttachedImage;
use App\Entity\Image;
use App\Resolver\ImageRouteResolver;
use App\UserPresentable\PresentableImage;

/**
 * Class ImageTransformer
 * @package App\Transformer
 */
class ImageTransformer extends CustomTransformer implements GalleryImageTransformerInterface
{

    /* @var ImageRouteResolver */
    private $routeResolver;

    /**
     * ImageTransformer constructor.
     * @param ImageRouteResolver $routeResolver
     */
    public function __construct(ImageRouteResolver $routeResolver)
    {
        $this->routeResolver = $routeResolver;
    }

    /**
     * @param null $param1
     * @param null $param2
     * @return PresentableImage|mixed
     * @throws \Exception
     */
    public function customTransform($param1 = null, $param2 = null)
    {
        /* @var ArticleAttachedImage $param1 */
        if (!$param1) return new PresentableImage();
        return PresentableImage::initWithArray($this->getUrlForImage($param1));
    }

    /**
     * @param Image $image
     * @return array
     * @throws \Exception
     */
    private function getUrlForImage(Image $image): array
    {
        return $this->routeResolver->resolveRoute($image);
    }
}