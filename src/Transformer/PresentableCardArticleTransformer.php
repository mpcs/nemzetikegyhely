<?php

namespace App\Transformer;


use App\Manager\MPCSFileManager;
use App\UserPresentable\PresentableCardArticle;
use App\Utils\MpcsUtils;
use App\Utils\StringReplacer;
use Ateszworks\MultilangContentBundle\Entity\ArticleTranslationEntity;
use Ateszworks\MultilangContentBundle\Entity\LangEntity;
use Ateszworks\MultilangContentBundle\Entity\TranslationFilterableEntity;
use Ateszworks\MultilangContentBundle\Transformer\Transformer;
use Ateszworks\MultilangContentBundle\UserPresentable\UserPresentable;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class PresentableCardArticleTransformer
 * @package App\Transformer
 */
class PresentableCardArticleTransformer implements Transformer
{

    /* @var RouterInterface */
    private $router;

    /* @var MPCSFileManager */
    private $fileManager;

    /* @var StringReplacer */
    private $stringReplacer;

    /**
     * ArticleTransformer constructor.
     * @param RouterInterface $router
     * @param MPCSFileManager $fileManager
     * @param StringReplacer $replacer
     */
    public function __construct(RouterInterface $router, MPCSFileManager $fileManager, StringReplacer $replacer)
    {
        $this->router = $router;
        $this->fileManager = $fileManager;
        $this->stringReplacer = $replacer;
    }

    /**
     * @param TranslationFilterableEntity $entity
     * @param LangEntity $toLang
     * @return UserPresentable
     */
    function transform(TranslationFilterableEntity $entity, LangEntity $toLang): ?UserPresentable
    {
        /* @var ArticleTranslationEntity $entity */
        $presentable = new PresentableCardArticle();
        $parentArticle = $entity->getArticle();
        $published = $parentArticle->getPublished() ?? $parentArticle->getCreated();

        $smallImage = $this->getSmallImageInB64($parentArticle->getImageUrl());
        $coverImage = $parentArticle->getImageUrl() ? $this->router->generate('image',
            ['name' => $parentArticle->getImageUrl()], UrlGeneratorInterface::ABSOLUTE_URL) : null;
        $presentable->setTitle($entity->getTitle());
        $presentable->setUrl($this->router->generate('localized_view_article', [
            '_locale' => $toLang->getCode(), 'article_slug' => $entity->getUrlSlug()
        ], UrlGeneratorInterface::ABSOLUTE_URL));
        $presentable->setCoverImage($coverImage);
        $presentable->setSmallCoverImage($smallImage);
        $presentable->setAuthor($parentArticle->getPublisher());
        $presentable->setIntroText($this->stringReplacer
            ->injectData(MpcsUtils::introText($entity->getContent(), 250)));
        $presentable->setCreated($parentArticle->getCreated()->format('Y. m. d.'));
        $presentable->setPublished($published->format('Y. m. d.'));

        return $presentable;
    }

    /**
     * @param null|string $bigImageName
     * @return null|string
     */
    private function getSmallImageInB64(?string $bigImageName): ?string
    {
        if (!$bigImageName) return null;
        try {
            $smallImage = $this->fileManager->getFullFileURL(MpcsUtils::getSmallNameFromNormal($bigImageName));
            return MpcsUtils::getSmallFileAsB64($smallImage);
        } catch (NotFoundHttpException $e) {
            return null;
        }
    }
}