<?php

namespace App\Transformer;


use App\MetaReadable\GeneralMetaReadable;
use App\MetaReadable\MetaReadableContent;
use App\MetaReadable\MetaReadableContentTransformer;
use App\UserPresentable\PresentableGallery;
use Ateszworks\MultilangContentBundle\UserPresentable\UserPresentable;

class GalleryMetaReadableTransformer implements MetaReadableContentTransformer
{


    /**
     * @param UserPresentable $entity
     * @return MetaReadableContent
     */
    public function transform(UserPresentable $entity): MetaReadableContent
    {
        /* @var PresentableGallery $entity */
        return (new GeneralMetaReadable())
            ->setTwitterTitle($entity->getTitle())
            ->setOgTitle($entity->getTitle())
            ->setOgImage($entity->getCoverImage())
            ->setTwitterImage($entity->getCoverImage());
    }

    /**
     * Returns the FQCN of meta type it transforms. Used as id, must be unique.
     * @return array
     */
    public function getAssociatedMetaType(): array
    {
        return [PresentableGallery::class];
    }
}