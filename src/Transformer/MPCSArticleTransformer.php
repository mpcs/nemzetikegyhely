<?php

namespace App\Transformer;


use App\Manager\MPCSFileManager;
use App\UserPresentable\MPCSPresentableArticle;
use App\Utils\MpcsUtils;
use App\Utils\StringReplacer;
use Ateszworks\MultilangContentBundle\Entity\LangEntity;
use Ateszworks\MultilangContentBundle\Entity\TranslationFilterableEntity;
use Ateszworks\MultilangContentBundle\Transformer\ArticleTransformer;
use Ateszworks\MultilangContentBundle\UserPresentable\PresentableArticle;
use Ateszworks\MultilangContentBundle\UserPresentable\UserPresentable;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class MPCSArticleTransformer
 * @package App\Transformer
 */
class MPCSArticleTransformer extends ArticleTransformer
{

    /* @var MPCSFileManager */
    private $fileManager;

    /* @var StringReplacer */
    private $stringReplacer;

    /**
     * @param MPCSFileManager $fileManager
     * @param StringReplacer $replacer
     */
    public function setFileManager(MPCSFileManager $fileManager, StringReplacer $replacer): void
    {
        $this->fileManager = $fileManager;
        $this->stringReplacer = $replacer;
    }

    /**
     * @param TranslationFilterableEntity $entity
     * @param LangEntity $toLang
     * @return UserPresentable|null
     */
    public function transform(TranslationFilterableEntity $entity, LangEntity $toLang): ?UserPresentable
    {
        /* @var PresentableArticle $presentableArticle */
        /* @var MPCSPresentableArticle $MPCSPresentable */
        $presentableArticle = parent::transform($entity, $toLang);
        $MPCSPresentable = MPCSPresentableArticle::createFromPresentableArticle($presentableArticle);
        $MPCSPresentable->setContents($this->stringReplacer->injectData($MPCSPresentable->getContents()));
        $MPCSPresentable->setSmallCoverImage($this->getSmallImageAsB64($presentableArticle->getCoverImage()));

        return $MPCSPresentable;
    }

    /**
     * @param null|string $coverImage
     * @return null|string
     * I am doing everything in order to avoid unnecessary db requests, so working with url here:
     */
    private function getSmallImageAsB64(?string $coverImage): ?string
    {
        if (!$coverImage) return null;

        $parts = explode('/', $coverImage);
        $coverImage = $parts[count($parts) - 1];

        try {
            $url = $this->fileManager->getFullFileURL(MpcsUtils::getSmallNameFromNormal($coverImage));
            return MpcsUtils::getSmallFileAsB64($url);
        } catch (NotFoundHttpException $e) {
            return null;
        }
    }

}