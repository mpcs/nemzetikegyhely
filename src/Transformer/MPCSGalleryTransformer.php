<?php

namespace App\Transformer;


use App\Entity\Gallery;
use App\Entity\GalleryTranslation;
use App\Manager\MPCSGalleryImageManager;
use App\UserPresentable\PresentableGallery;
use App\Utils\MpcsUtils;
use Ateszworks\MultilangContentBundle\ContentInterface\LanguageProviderInterface;
use Ateszworks\MultilangContentBundle\Entity\LangEntity;
use Ateszworks\MultilangContentBundle\Entity\TranslationFilterableEntity;
use Ateszworks\MultilangContentBundle\UserPresentable\UserPresentable;

/**
 * Class MPCSGalleryTransformer
 * @package App\Transformer
 */
class MPCSGalleryTransformer extends CustomTransformer
{
    /* @var LanguageProviderInterface */
    private $langProvider;

    /* @var MPCSGalleryImageManager */
    private $galleryImageManager;

    /**
     * MPCSGalleryTransformer constructor.
     * @param LanguageProviderInterface $langProvider
     * @param MPCSGalleryImageManager $galleryImageManager
     */
    public function __construct(LanguageProviderInterface $langProvider, MPCSGalleryImageManager $galleryImageManager)
    {
        $this->langProvider = $langProvider;
        $this->galleryImageManager = $galleryImageManager;
    }

    /**
     * @param TranslationFilterableEntity $entity
     * @param LangEntity $toLang
     * @return UserPresentable|null
     */
    public function transform(TranslationFilterableEntity $entity, LangEntity $toLang): ?UserPresentable
    {
        /* @var Gallery $entity */
        /* @var GalleryTranslation $translation */
        $translation = $entity->getTranslationForLanguage($toLang) ??
            $entity->getTranslationForLanguage($this->langProvider->getDefaultLanguage()) ??
            $entity->getTranslations()->first();

        $image = $entity->getImages()->first();
        $published = $entity->getPublished() ?? $entity->getCreated();
        $presentableGallery = new PresentableGallery();

        if ($image) {
            $imgUrls = $this->galleryImageManager->getPresentable($image);
            $presentableGallery->setCoverImage($imgUrls->getUrl());
            $presentableGallery
                ->setSmallCoverImage(MpcsUtils::getSmallNameFromNormal($imgUrls->getUrl()));
        }

        if (!$translation) return null;
        $presentableGallery->setTitle($translation->getTitle());
        $presentableGallery->setUrlSlug($translation->getUrlSlug());
        $presentableGallery->setCreated($entity->getCreated()->format('Y.m.d'));
        $presentableGallery->setPublished($published->format('Y.m.d'));

        return $presentableGallery;
    }

    /**
     * Trying to give you some freedom for converting you custom types.
     * @param null $param1
     * @param null $param2
     * @return mixed
     */
    public function customTransform($param1 = null, $param2 = null)
    {
        /* @var $param1 GalleryTranslation */
        if (!($param1 instanceof GalleryTranslation)) return null;
        return (new PresentableGallery())->setTitle($param1->getTitle());
    }
}