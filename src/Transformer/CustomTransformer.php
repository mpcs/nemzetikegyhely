<?php

namespace App\Transformer;


use Ateszworks\MultilangContentBundle\Entity\LangEntity;
use Ateszworks\MultilangContentBundle\Entity\TranslationFilterableEntity;
use Ateszworks\MultilangContentBundle\Transformer\Transformer;
use Ateszworks\MultilangContentBundle\UserPresentable\UserPresentable;

/**
 * Class CustomTransformer
 * @package App\Transformer
 */
abstract class CustomTransformer implements Transformer
{
    /**
     * @param TranslationFilterableEntity $entity
     * @param LangEntity $toLang
     * @return UserPresentable|null
     */
    function transform(TranslationFilterableEntity $entity, LangEntity $toLang): ?UserPresentable
    {
        return null;
    }

    /**
     * Trying to give you some freedom for converting you custom types.
     * @param null $param1
     * @param null $param2
     * @return mixed
     */
    abstract public function customTransform($param1 = null, $param2 = null);
}