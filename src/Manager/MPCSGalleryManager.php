<?php

namespace App\Manager;


use App\Entity\Gallery;
use App\Entity\GalleryImage;
use App\Entity\GalleryTranslation;
use App\Provider\MPCSContentProvider;
use App\Transformer\MPCSGalleryTransformer;
use App\Utils\TranslationFilter;
use Ateszworks\MultilangContentBundle\ContentInterface\ContentTranslation;
use Ateszworks\MultilangContentBundle\ContentInterface\UserPresentableProvier;
use Ateszworks\MultilangContentBundle\Entity\LangEntity;
use Ateszworks\MultilangContentBundle\Entity\TranslationFilterableEntity;
use Ateszworks\MultilangContentBundle\Manager\AbstractContentManager;
use Ateszworks\MultilangContentBundle\UserPresentable\UserPresentable;
use Ateszworks\MultilangContentBundle\Utils\UrlUtils;
use Doctrine\Common\Collections\ArrayCollection;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class MPCSGalleryManager
 * @package App\Manager
 */
class MPCSGalleryManager extends AbstractContentManager implements UserPresentableProvier
{

    /* @var TranslationFilter */
    private $translatableManager;

    /* @var MPCSContentProvider */
    protected $contentProvider;

    /* @var MPCSGalleryFileManager */
    protected $fileManager;

    /* @var MPCSGalleryImageManager */
    protected $imageManager;

    /**
     * MPCSGalleryManager constructor.
     * @param MPCSContentProvider $contentProvider
     * @param LoggerInterface $logger
     * @param SessionInterface $session
     * @throws \Ateszworks\MultilangContentBundle\Exception\BadRepositoryTypeException
     */
    public function __construct(MPCSContentProvider $contentProvider, LoggerInterface $logger,
                                SessionInterface $session)
    {
        parent::__construct($contentProvider, $logger, $session);
        $this->contentProvider->setManagedContentType(Gallery::class);
        $this->contentProvider->setManagedContentTranslationType(GalleryTranslation::class);
        $this->contentProvider->setOrderingForPackedRequest(['published' => 'DESC', 'created' => 'DESC']);
    }

    /**
     * @param MPCSGalleryFileManager $fileManager
     */
    public function setFileManager(MPCSGalleryFileManager $fileManager): void
    {
        $this->fileManager = $fileManager;
    }

    /**
     * @param MPCSGalleryTransformer $transformer
     */
    public function setTransformer(MPCSGalleryTransformer $transformer): void
    {
        $this->transformer = $transformer;
    }

    /**
     * @param TranslationFilter $translatableManager
     */
    public function setTranslatableManager(TranslationFilter $translatableManager): void
    {
        $this->translatableManager = $translatableManager;
    }

    /**
     * @param MPCSGalleryImageManager $imageManager
     */
    public function setImageManager(MPCSGalleryImageManager $imageManager): void
    {
        $this->imageManager = $imageManager;
    }

    /**
     * @return array
     */
    public function getPresentableGalleries(): array
    {
        $galleries = $this->getGalleryTranslations();
        return $this->getPresentables($galleries, $this->languageProvider->getCurrentLanguage());
    }

    /**
     * @return array
     */
    public function adminListableWithTranslations(): ?array
    {
        return $this->getContentTranslations();
    }

    /**
     * @param int $resourceId
     * @return Gallery
     */
    public function getById(int $resourceId): Gallery
    {
        return $this->getEntityById($resourceId);
    }

    /**
     * @param $entity
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save($entity): void
    {
        /* @var $entity Gallery */
        $this->translatableManager->filterTranslations($entity);
        foreach ($entity->getTranslations() as $translation) {
            /* @var GalleryTranslation $translation */
            $translation->setGallery($entity);
            $translation->setUrlSlug(UrlUtils::getCleanFormOfString($translation->getTitle()));
        }
        parent::save($entity);
    }

    /**
     * @param string $slug
     * @return array|null
     */
    public function getPresentableBySlug(string $slug): ?array
    {
        $translation = $this->getTranslationWithFallback($slug);
        if (!$translation) return null;

        $gallery = $translation->getGallery();
        return ['gallery_slug' => $translation->getUrlSlug(),
            'translation' => $this->getPresentable($gallery, $this->languageProvider->getCurrentLanguage()),
            'images' => $this->imageManager->getPresentableImages($gallery)];
    }

    /**
     * @param string $slug
     * @return GalleryTranslation
     */
    public function getTranslationWithFallback(string $slug): GalleryTranslation
    {
        return $this->contentProvider->getTranslationWithResourceByParameter($slug);
    }

    /**
     * @param int $id
     * @param string $lang
     * @return ContentTranslation|null
     */
    public function getTranslationWithFallbackById(int $id, string $lang): ?ContentTranslation
    {
        return $this->contentProvider->getTranslationForEntityByLangCodeWithFallback($id, $lang);
    }

    /**
     * @return array|null
     */
    private function getGalleryTranslations(): ?array
    {
        return $this->contentProvider->getEntitiesWithAppropriateTranslation();
    }

    // AbstractContentManager

    /**
     * @return array|null
     */
    public function getEntityTranslations(): ?array
    {
        return $this->getGalleryTranslations();
    }

    // UserPresentableProvider

    /**
     * @param TranslationFilterableEntity $fromEntity
     * @param LangEntity $inLang
     * @return UserPresentable|null
     */
    public function getPresentable(TranslationFilterableEntity $fromEntity, LangEntity $inLang): ?UserPresentable
    {
        return $this->transformer->transform($fromEntity, $inLang);
    }

    /**
     * @param array $entities
     * @param LangEntity $inLang
     * @return array|null
     */
    public function getPresentables(array $entities, LangEntity $inLang): ?array
    {
        return array_map(function (Gallery $gallery) use ($inLang) {
            return $this->getPresentable($gallery, $inLang);
        }, $entities);
    }

    /**
     * @param int $from
     * @param int $amount
     * @param LangEntity $inLang
     * @return array|null
     */
    public function getPresentablesPaged(int $from, int $amount, LangEntity $inLang): ?array
    {
        // TODO: Implement getPresentablesPaged() method.
    }

    // TranslatabeManager

    /**
     * @param Gallery $galleryEntity
     * @return bool
     */
    public function checkTranslations(Gallery $galleryEntity): bool
    {
        return $this->translatableManager->checkTranslations($galleryEntity);
    }

    /**
     * @param $resouceId
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removePageById($resouceId): void
    {
        /* @var $entity Gallery */
        $entity = $this->contentProvider->getEntityById($resouceId);
        if (!$entity)
            throw new NotFoundHttpException('Nem található tartalom', null, 404);

        $this->contentProvider->removeByEntity($entity);
    }

    /**
     * @param Gallery $gallery
     * @param array $images
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function attachImages(Gallery $gallery, array $images): void
    {
        $currentImages = $gallery->getImages() ?? new ArrayCollection();
        foreach ($images as &$image) {
            /* @var $image GalleryImage */
            if (!$image instanceof GalleryImage) continue;

            $image->setGallery($gallery);
            $currentImages->add($image);
        }

        /* @var $this ->contentProvider */
        $gallery->setImages($currentImages);
        $images[] = $gallery;
        $this->contentProvider->batchSaveOperation($images);
    }

    /**
     * @param int $galId
     * @param string $resource
     * @return BinaryFileResponse
     */
    public function getImageAsBinaryResponse(int $galId, string $resource): BinaryFileResponse
    {
        return new BinaryFileResponse($this->fileManager->getImagePath($galId, $resource));
    }

    /**
     * @param $entity
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove($entity): void
    {
        /* @var Gallery $entity */
        $entityId = $entity->getId();
        parent::remove($entity);
        $this->fileManager->removeGalleryDirectory($entityId);
    }

    /**
     * @param int $toGallery
     * @param UploadedFile $concreteImage
     * @return array
     */
    public function uploadImage(int $toGallery, UploadedFile $concreteImage): array
    {
        return $this->fileManager->uploadImageToGallery($toGallery, $concreteImage);
    }
}