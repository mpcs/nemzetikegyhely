<?php

namespace App\Manager;

use App\Entity\Gallery;
use Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class MPCSGalleryFileManager
 * @package App\Manager
 */
class MPCSGalleryFileManager extends MPCSFileManager
{

    /* @var Gallery */
    private $managedGallery;

    /* @var string */
    private $defaultWorkingDirectory;

    /**
     * MPCSGalleryFileManager constructor.
     * @param string $heySymfoYouBugos
     */
    // todo: debug WHY DA FUCK we get wrong param here
    // TODO: Refactor: Composition over inheritance... Just have an mpcsfm property!
    /**
     * MPCSGalleryFileManager constructor.
     * @param string $heySymfoYouBugos
     * @param bool $processImageOnUpload
     */
    public function __construct(string $heySymfoYouBugos, bool $processImageOnUpload)
    {
        parent::__construct($heySymfoYouBugos, $processImageOnUpload);
        $this->defaultWorkingDirectory = $heySymfoYouBugos;
    }

    /**
     * @param Gallery $managedGallery
     * @return MPCSGalleryFileManager
     */
    public function setManagedGallery(Gallery $managedGallery): MPCSGalleryFileManager
    {
        $this->managedGallery = $managedGallery;
        $this->workingDirectory = $this->defaultWorkingDirectory . '/' . $managedGallery->getId();
        return $this;
    }

    /**
     * @param int $galleryId
     * @param string $imageName
     * @return string
     */
    public function getImagePath(int $galleryId, string $imageName): string
    {
        $path = $this->constructImagePath($galleryId, $imageName);
        if (!$this->fileSystem->exists($path)) throw new NotFoundHttpException();
        return $path;
    }

    /**
     * @param $galleryId
     * @param $storedName
     */
    public function removeImageFromGallery($galleryId, $storedName)
    {
        try {
            $removePath = $this->getImagePath($galleryId, $storedName);
            $this->fileSystem->remove($removePath);
            $this->fileSystem->remove($this->convertNameToSmall($removePath));
        } catch (Exception $e) {
            $this->logger->error('Could not remove image', [
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
        }
    }

    /**
     * @param int $galleryId
     */
    public function removeGalleryDirectory(int $galleryId): void
    {
        $galleryPath = $this->constructGalleryPath($galleryId);
        if (!$this->fileSystem->exists($galleryPath)) return;
        $this->fileSystem->remove($galleryPath);
    }

    /**
     * @param int $galleryId
     * @return string
     */
    private function constructGalleryPath(int $galleryId): string
    {
        return $this->defaultWorkingDirectory . '/' . $galleryId;
    }

    /**
     * @param int $galleryId
     * @param string $imageName
     * @return string
     */
    private function constructImagePath(int $galleryId, string $imageName): string
    {
        return $this->constructGalleryPath($galleryId) . '/' . $imageName;
    }

    /**
     * @param int $galleryID
     * @param UploadedFile $concreteImage
     * @return array
     */
    public function uploadImageToGallery(int $galleryID, UploadedFile $concreteImage): array
    {
        $this->workingDirectory = $this->defaultWorkingDirectory . '/' . $galleryID;
        $url = $this->uploadImage($concreteImage);

        return [
            'small' => $this->convertNameToSmall($url),
            'normal' => $url
        ];
    }

}