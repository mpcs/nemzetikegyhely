<?php

namespace App\Manager;

use App\Entity\CarouselPage;
use App\Entity\CarouselPageTranslation;
use App\Transformer\CarouselTransformer;
use App\Utils\TranslationFilter;
use Ateszworks\MultilangContentBundle\ContentInterface\ContentProviderInterface;
use Ateszworks\MultilangContentBundle\ContentInterface\LanguageProviderInterface;
use Ateszworks\MultilangContentBundle\ContentInterface\UserPresentableProvier;
use Ateszworks\MultilangContentBundle\Entity\LangEntity;
use Ateszworks\MultilangContentBundle\Entity\TranslationFilterableEntity;
use Ateszworks\MultilangContentBundle\Manager\FileManager;
use Ateszworks\MultilangContentBundle\Manager\RemoveCapableManager;
use Ateszworks\MultilangContentBundle\UserPresentable\UserPresentable;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class MPCSCarouselManager
 * @package App\Manager
 */
class MPCSCarouselManager extends TranslationFilter implements UserPresentableProvier,
    RemoveCapableManager
{
    /* @var LanguageProviderInterface */
    private $langProvider;

    /* @var ContentProviderInterface */
    private $contentProvider;

    /* @var FileManager */
    private $fileManager;

    /* @var CarouselTransformer */
    private $transformer;

    /**
     * MPCSCarouselManager constructor.
     * @param ContentProviderInterface $contentProvider
     * @param CarouselTransformer $carouselTransformer
     * @param LanguageProviderInterface $languageProvider
     */
    public function __construct(ContentProviderInterface $contentProvider, CarouselTransformer $carouselTransformer,
                                LanguageProviderInterface $languageProvider)
    {
        parent::__construct();
        $this->langProvider = $languageProvider;
        $this->transformer = $carouselTransformer;
        $this->contentProvider = $contentProvider;
        $this->contentProvider->setManagedContentType(CarouselPage::class);
        $this->contentProvider->setManagedContentTranslationType(CarouselPageTranslation::class);
        $this->contentProvider->getTranslationRepositoryReference()->setOrder(['weight' => 'ASC']);
    }

    /**
     * @param FileManager $fileManager
     */
    public function setFileManager(FileManager $fileManager): void
    {
        $this->fileManager = $fileManager;
    }

    /**
     * @return array|null
     */
    public function getAll(): ?array
    {
        return $this->contentProvider->getEntitiesWithAppropriateTranslation();
    }

    /**
     * @return array|null
     */
    public function getAllPresentables(): ?array
    {
        return $this->getPresentables($this->getAll() ?? [], $this->langProvider->getCurrentLanguage());
    }

    /**
     * @param int $resourceId
     * @return CarouselPage
     */
    public function getById(int $resourceId): CarouselPage
    {
        return $this->contentProvider->getEntityById($resourceId);
    }

    /**
     * @param CarouselPage $carouselPageEntity
     * @param UploadedFile|null $file
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(CarouselPage $carouselPageEntity, UploadedFile $file = null): void
    {
        if ($file) {
            if ($carouselPageEntity->getImage()) $this->fileManager->removeFile($carouselPageEntity->getImage());
            $carouselPageEntity->setImage($this->fileManager->uploadImage($file));
        }

        $this->filterTranslations($carouselPageEntity);

        $translations = $carouselPageEntity->getTranslations();
        /* @var $translation CarouselPageTranslation */
        foreach ($translations as $translation) $translation->setPage($carouselPageEntity);
        $this->contentProvider->saveByEntity($carouselPageEntity);
    }

    /**
     * @param TranslationFilterableEntity $fromEntity
     * @param LangEntity $inLang
     * @return UserPresentable|null
     */
    public function getPresentable(TranslationFilterableEntity $fromEntity, LangEntity $inLang): ?UserPresentable
    {
        return $this->transformer->transform($fromEntity, $inLang);
    }

    /**
     * @param array $entities
     * @param LangEntity $inLang
     * @return array|null
     */
    public function getPresentables(array $entities, LangEntity $inLang): ?array
    {
        return array_map(function ($entity) use ($inLang) {
            return $this->getPresentable($entity, $inLang);
        }, $entities);
    }

    /**
     * @param int $from
     * @param int $amount
     * @param LangEntity $inLang
     * @return array|null
     */
    public function getPresentablesPaged(int $from, int $amount, LangEntity $inLang): ?array
    {
        return [];
    }

    /**
     * @param int $resouceId
     */
    public function removeById(int $resouceId): void
    {
        /* @var $entity CarouselPage */
        $entity = $this->contentProvider->getEntityById($resouceId);
        if (!$entity) throw new NotFoundHttpException('Nem található tartalom', null, 404);

        $image = $entity->getImage();
        if ($image) $this->fileManager->removeFile($image);
        $this->remove($entity);
    }

    // MARK: - RemoveCapableManager
    /**
     * @param $entity
     */
    public function remove($entity): void
    {
        $this->contentProvider->removeByEntity($entity);
    }
}