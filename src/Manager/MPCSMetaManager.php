<?php

namespace App\Manager;


use App\Event\WillDisplayContentEvent;
use App\MetaReadable\MetaReadableContent;
use App\MetaReadable\MetaReadableContentTransformer;
use App\UserPresentable\PresentableMetaBag;
use Ateszworks\MultilangContentBundle\UserPresentable\UserPresentable;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class MPCSMetaManager
 * @package App\Manager
 */
class MPCSMetaManager
{

    /* @var PresentableMetaBag */
    private $presentableMetaBag;

    /* @var Translator */
    private $translator;

    /* @var RouterInterface */
    private $router;

    /* @var string */
    private $ogImage;

    /* @var string */
    private $twitterImage;

    /* @var array */
    private $transformers = []; // Managed meta type => transformer

    /**
     * MPCSMetaManager constructor.
     * @param TranslatorInterface $translator
     * @param string $ogImage
     * @param string $twitterImage
     * @param RouterInterface $router
     */
    public function __construct(TranslatorInterface $translator,
                                string $ogImage,
                                string $twitterImage,
                                RouterInterface $router)
    {
        $this->translator = $translator;
        $this->ogImage = $ogImage;
        $this->twitterImage = $twitterImage;
        $this->router = $router;
    }

    /**
     * @param UserPresentable $presentable
     * @return MetaReadableContent
     */
    public function autoTransformContent(UserPresentable $presentable): ?MetaReadableContent
    {
        $transformer = $this->getTransformerByMetaType(get_class($presentable));
        if (!$transformer) return null;
        return $transformer->transform($presentable);
    }

    /**
     * @param iterable $services
     */
    public function recieveTaggedServices(iterable $services): void
    {
        $this->processServices($services);
    }

    /**
     * @param string $id
     * @return MetaReadableContentTransformer|null
     */
    public function getTransformerByMetaType(string $id): ?MetaReadableContentTransformer
    {
        return in_array($id, array_keys($this->transformers)) ? $this->transformers[$id] : null;
    }

    /**
     * @param WillDisplayContentEvent $event
     */
    public function onContentWillDisplay(WillDisplayContentEvent $event): void
    {
        $content = $event->getContent();
        if (!$content) return;
        $this->presentableMetaBag = $this->transform($content);
    }

    /**
     * @return array
     */
    public function getAsArray(): array
    {
        return ['meta' => $this->getAppropriateMetaBag()];
    }

    // Mark: Private

    /**
     * @return PresentableMetaBag
     */
    private function getAppropriateMetaBag(): PresentableMetaBag
    {
        if (!$this->presentableMetaBag) return $this->buildDefaultMetaBag();
        return $this->presentableMetaBag;
    }

    /**
     * @param MetaReadableContent $content
     * @return PresentableMetaBag
     */
    private function transform(MetaReadableContent $content): PresentableMetaBag
    {
        return (new PresentableMetaBag())
            ->setOgTitle($content->getOgTitle())
            ->setOgDescription($content->getOgDescription())
            ->setOgImage($content->getOgImage() ??
                $this->router->generate('img', ['name' => $this->ogImage],
                    UrlGeneratorInterface::ABSOLUTE_URL))
            ->setOgUrl($content->getOgUrl())
            ->setTwitterTitle($content->getTwitterImage())
            ->setTwitterDescription($content->getOgDescription())
            ->setTwitterCard($content->getTwitterCard())
            ->setTwitterImage($content->getOgImage() ??
                $this->router->generate('img', ['name' => $this->twitterImage],
                    UrlGeneratorInterface::ABSOLUTE_URL));
    }

    /**
     * @return PresentableMetaBag
     */
    private function buildDefaultMetaBag(): PresentableMetaBag
    {
        $ogImageRoute = $this->router->generate('img', ['name' => $this->ogImage],
            UrlGeneratorInterface::ABSOLUTE_URL);
        $twitterImageRoute = $this->router->generate('img', ['name' => $this->twitterImage],
            UrlGeneratorInterface::ABSOLUTE_URL);


        return (new PresentableMetaBag())
            ->setOgTitle($this->translator->trans('meta.og.title'))
            ->setOgDescription($this->translator->trans('meta.og.description'))
            ->setOgImage($ogImageRoute)
            ->setTwitterTitle($this->translator->trans('meta.twitter.title'))
            ->setOgUrl(null)
            ->setTwitterDescription($this->translator->trans('meta.twitter.description'))
            ->setTwitterCard($this->translator->trans('meta.twitter.card'))
            ->setTwitterImage($twitterImageRoute);
    }

    /**
     * @param iterable $services
     */
    private function processServices(iterable $services): void
    {
        foreach ($services as $service) {
            /* @var MetaReadableContentTransformer $service */
            foreach ($service->getAssociatedMetaType() as $type) $this->transformers[$type] = $service;
        }
    }
}