<?php

namespace App\Manager;


use App\Entity\ArticleAttachedImage;
use App\Provider\MPCSContentProvider;
use Ateszworks\MultilangContentBundle\Entity\ArticleEntity;
use Doctrine\ORM\PersistentCollection;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class MPCSAttachedImageManager
 * @package App\Manager
 */
class MPCSAttachedImageManager
{

    /* @var MPCSFileManager */
    private $fileManager;

    /* @var MPCSContentProvider */
    private $contentProvider;

    /* @var LoggerInterface */
    private $logger;

    /**
     * MPCSAttachedImageManager constructor.
     * @param MPCSFileManager $fileManager
     * @param MPCSContentProvider $contentProvider
     * @param LoggerInterface $logger
     */
    public function __construct(MPCSFileManager $fileManager, MPCSContentProvider $contentProvider, LoggerInterface $logger)
    {
        $this->fileManager = $fileManager;
        $this->contentProvider = $contentProvider;
        $this->logger = $logger;
        $this->contentProvider->setManagedContentType(ArticleAttachedImage::class);
    }

    /**
     * @param ArticleEntity $articleEntity
     * @param array $imageArray
     * @throws \Exception
     */
    public function attachImagesToArticle(ArticleEntity $articleEntity, array $imageArray = []): void
    {
        if (count($imageArray) < 1) return;

        $entites = array_map(function (string $url) use ($articleEntity) {
            return (new ArticleAttachedImage())->setImage($url)->setArticle($articleEntity);
        }, $this->uploadImages($imageArray));

        $this->doBatchSave($entites);
    }

    /**
     * @param ArticleEntity $forArticle
     * @return array
     */
    public function loadAttachedImages(ArticleEntity $forArticle): array
    {
        $this->contentProvider->setManagedContentType(ArticleAttachedImage::class);
        $this->contentProvider->setCriteriaArray(['article' => $forArticle]);
        return $this->contentProvider->getAll();
    }

    /**
     * @param ArticleEntity $article
     */
    public function removeImagesOf(ArticleEntity $article): void
    {
        $this->contentProvider->setManagedContentType(ArticleAttachedImage::class);
        $this->contentProvider->setCriteriaArray(['article' => $article]);
        $images = $this->contentProvider->getAll();
        foreach ($images as $image) {
            /* @var $image ArticleAttachedImage */
            $this->fileManager->removeFile($image->getImage());
            try {
                $this->contentProvider->removeByEntity($image);
            } catch (Exception $e) {
                $this->logger->error('Unable to remove ArticleAttachedImage from DB.', [
                    'message' => $e->getMessage(),
                    'trace' => $e->getTraceAsString()
                ]);
            }
        }
    }

    /**
     * @param $fileUrl
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeImageByUrl($fileUrl): void
    {
        $this->fileManager->removeImage($fileUrl);
        $this->contentProvider->setCriteriaArray(['image' => $fileUrl]);
        $row = $this->contentProvider->getAll();
        if (count($row) == 0) return;
        $this->contentProvider->removeByEntity($row[0]);
    }

    /**
     * @param array $withEntities
     * @throws \Exception
     */
    private function doBatchSave(array $withEntities): void
    {
        try {
            $this->contentProvider->batchSaveOperation($withEntities);
        } catch (\Exception $e) {
            $this->logger->alert('Unable to save image.', ['message' => $e->getMessage()]);
            throw $e;
        }
    }

    /**
     * @param array $images
     * @return array
     */
    private function uploadImages(array $images = []): array
    {
        if (count($images) < 1) return [];
        return array_map(function (UploadedFile $file) {
            if ($this->fileManager->exists($file->getClientOriginalName()))
                return $file->getClientOriginalName();
            return $this->fileManager->uploadImage($file);
        }, $images);
    }
}