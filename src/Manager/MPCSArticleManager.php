<?php

namespace App\Manager;


use App\Entity\ArticleAttachedImage;
use App\Transformer\ImageTransformer;
use App\Transformer\PresentableCardArticleTransformer;
use App\UserPresentable\MPCSPresentableArticle;
use App\Utils\MpcsUtils;
use Ateszworks\MultilangContentBundle\ContentInterface\ContentProviderInterface;
use Ateszworks\MultilangContentBundle\Entity\ArticleEntity;
use Ateszworks\MultilangContentBundle\Entity\ArticleTranslationEntity;
use Ateszworks\MultilangContentBundle\Entity\LangEntity;
use Ateszworks\MultilangContentBundle\Entity\TranslationFilterableEntity;
use Ateszworks\MultilangContentBundle\Manager\ArticleManager;
use Ateszworks\MultilangContentBundle\Repository\ArticleEntityRepository;
use Ateszworks\MultilangContentBundle\UserPresentable\PresentableArticle;
use Ateszworks\MultilangContentBundle\UserPresentable\UserPresentable;
use Doctrine\ORM\NonUniqueResultException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class MPCSArticleManager
 * @package App\Manager
 */
class MPCSArticleManager extends ArticleManager
{

    /* @var RouterInterface */
    private $router;

    /* @var PresentableCardArticleTransformer */
    private $cardArticleTransformer;

    /* @var MPCSAttachedImageManager */
    private $attachedImageManager;

    /* @var ImageTransformer */
    private $imageTransformer;

    /**
     * MPCSArticleManager constructor.
     * @param ContentProviderInterface $contentProvider
     * @param LoggerInterface $logger
     * @param SessionInterface $session
     * @param ImageTransformer $imageTransformer
     * @throws \Ateszworks\MultilangContentBundle\Exception\BadRepositoryTypeException
     */
    public function __construct(ContentProviderInterface $contentProvider,
                                LoggerInterface $logger, SessionInterface $session,
                                ImageTransformer $imageTransformer)
    {
        parent::__construct($contentProvider, $logger, $session);
        $this->imageTransformer = $imageTransformer;
    }

    /**
     * @param MPCSAttachedImageManager $attachedImageManager
     * @return MPCSArticleManager
     */
    public function setAttachedImageManager(MPCSAttachedImageManager $attachedImageManager): MPCSArticleManager
    {
        $this->attachedImageManager = $attachedImageManager;
        return $this;
    }

    /**
     * @param RouterInterface $router
     * @return MPCSArticleManager
     */
    public function setRouter(RouterInterface $router): MPCSArticleManager
    {
        $this->router = $router;
        return $this;
    }

    /**
     * @param PresentableCardArticleTransformer $cardArticleTransformer
     * @return MPCSArticleManager
     */
    public function setCardArticleTransformer(PresentableCardArticleTransformer $cardArticleTransformer): MPCSArticleManager
    {
        $this->cardArticleTransformer = $cardArticleTransformer;
        return $this;
    }

    /**
     * @param bool $onlyListables
     * @return array
     */
    public function getAll(bool $onlyListables = true): array
    {
        $this->contentProvider->setOrderingForPackedRequest(['published' => 'DESC', 'created' => 'DESC']);
        $this->contentProvider->setCriteriaArray(['isListed' => $onlyListables]);

        return array_values(array_filter(array_map(function (ArticleEntity $entity) {
            return $entity->getTranslationForLanguage($this->languageProvider->getCurrentLanguage());
        }, $this->contentProvider->getEntitiesWithAppropriateTranslation()),
            function (?ArticleTranslationEntity $translationEntity) {
                return null != $translationEntity;
            })); // array_values??? I mean like... PHP, why?! ... why?! :'(
    }

    /**
     * @param bool $onlyListables
     * @return array
     */
    public function getAllWithRoutes(bool $onlyListables = true): array
    {
        return array_map(function (ArticleTranslationEntity $entity) {
            return $this->cardArticleTransformer->transform($entity, $this->languageProvider->getCurrentLanguage());
        }, $this->getAll($onlyListables) ?? []);
    }

    /**
     * @return array|null
     * @throws \Exception
     */
    public function getLatestArticleWithRoute(): ?array // [route:, article:]
    {
        $article = $this->getLatestArticle();
        if (!$article) return null;

        $lang = $this->languageProvider->getCurrentLanguage();
        /* @var ArticleTranslationEntity $translation */
        $translation = $article->getTranslationForLanguage($lang);

        /* @var PresentableArticle $transformedArticle */
        $transformedArticle = $this->getPresentable($translation, $lang);

        $text = MpcsUtils::introText($transformedArticle->getContents(), 700);
        $transformedArticle->setContents($text);

        return [
            'route' => $this->router->generate('localized_view_article', [
                'article_slug' => $translation->getUrlSlug(),
                '_locale' => $this->languageProvider->getCurrentLanguage()->getCode()
            ]),
            'article' => $transformedArticle
        ];
    }

    /**
     * @param ArticleTranslationEntity $translationEntity
     * @return array
     */
    public function getAttachedImages(?ArticleTranslationEntity $translationEntity): array
    {
        if (!$translationEntity || !($article = $translationEntity->getArticle())) return [];
        return $this->loadImagesForArticle($article);
    }

    /**
     * @param ArticleEntity $article
     * @return array
     */
    public function loadImagesForArticle(ArticleEntity $article): array
    {
        $images = $this->attachedImageManager->loadAttachedImages($article);
        return array_map(function ($image) {
            /* @var $image ArticleAttachedImage */
            return [
                'small' => $this->fileManager->convertNameToSmall($image->getImage()),
                'original' => $image->getImage()
            ];
        }, $images);
    }

    /**
     * @param string $slug
     * @return MPCSPresentableArticle
     * @throws \Exception
     */
    public function getArticleWithImagesBySlug(string $slug): MPCSPresentableArticle
    {
        /* @var MPCSPresentableArticle $presentable */
        $translation = $this->getTranslationBySlug($slug);
        if (!$translation) throw
        new NotFoundHttpException($this->translator->trans('user_messages.content_not_found'));

        $images = $this->attachedImageManager->loadAttachedImages($translation->getArticle());
        $presentable = $this->getPresentable($translation, $this->languageProvider->getCurrentLanguage());
        $presentable->setImages(array_map(function ($image) {
            return $this->imageTransformer->customTransform($image);
        }, $images));

        return $presentable;
    }

    // UserPresentableProvider

    /**
     * @param TranslationFilterableEntity $entity
     * @param LangEntity $inLang
     * @return UserPresentable
     */
    public function getPresentable(TranslationFilterableEntity $entity, LangEntity $inLang): UserPresentable
    {
        /* @var PresentableArticle $presArticle */
        /* @var MPCSPresentableArticle $MPCSPresentable */
        // HEY BOI! Look at Kernel.php. ArticleTransformer is overridden by a compiler pass.
        // No need for this!
        $presArticle = parent::getPresentable($entity, $inLang);
//        $MPCSPresentable = MPCSPresentableArticle::createFromPresentableArticle($presArticle);

        return $presArticle;
    }

    // MARK: - Private

    /**
     * @return TranslationFilterableEntity|null
     * @throws \Exception
     */
    private function getLatestArticle(): ?TranslationFilterableEntity
    {
        // todo: work out a solution and remove repository ref. calls!
        /* @var ArticleEntityRepository $repositoryReference */
        $repositoryReference = $this->contentProvider->getRepositoryReference();
        $lang = $this->languageProvider->getCurrentLanguage();

        /* @var ArticleEntity $article */
        $article = null;
        try {
            $article = $repositoryReference->getLatestArticle($lang);
        } catch (NonUniqueResultException $e) {
            $this->logger->error('More than one results!', ['results' => $article]);
            return null;
        }

        return $article;
    }
}
