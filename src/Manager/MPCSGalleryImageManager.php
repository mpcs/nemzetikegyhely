<?php

namespace App\Manager;

use App\Entity\Gallery;
use App\Entity\GalleryImage;
use App\Entity\Image;
use App\Transformer\GalleryImageTransformerInterface;
use App\Transformer\ImageTransformer;
use App\Transformer\ImageTransformerInterface;
use App\UserPresentable\PresentableImage;
use Ateszworks\MultilangContentBundle\ContentInterface\ContentProviderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class MPCSGalleryImageManager
 * @package App\Manager
 */
class MPCSGalleryImageManager
{

    /* @var ContentProviderInterface */
    protected $contentProvider;

    /* @var ImageTransformerInterface */
    protected $transformer;

    /* @var MPCSGalleryFileManager */
    protected $fileManager;

    /* @var Gallery */
    private $gallery = null;

    /**
     * MPCSGalleryImageManager constructor.
     * @param ContentProviderInterface $contentProvider
     * @param ImageTransformerInterface $transformer
     * @param MPCSGalleryFileManager $fileManager
     */
    public function __construct(ContentProviderInterface $contentProvider,
                                ImageTransformerInterface $transformer,
                                MPCSGalleryFileManager $fileManager)
    {
        $this->contentProvider = $contentProvider;
        $this->transformer = $transformer;
        $this->fileManager = $fileManager;

        $this->contentProvider->setManagedContentType(GalleryImage::class);
    }

    /**
     * @param Gallery $gallery
     * @return MPCSGalleryImageManager
     */
    public function setGallery(Gallery $gallery): MPCSGalleryImageManager
    {
        $this->gallery = $gallery;
        $this->fileManager->setManagedGallery($gallery);
        return $this;
    }

    /**
     * @param Gallery $ofGallery
     * @return array
     */
    public function getImages(Gallery $ofGallery): array
    {
        $this->contentProvider->setCriteriaArray(['gallery' => $ofGallery]);
        return $this->contentProvider->getAll();
    }

    /**
     * @param Gallery $gallery
     * @param string $imageName
     * @return string
     */
    public function getImagePath(Gallery $gallery, string $imageName): string
    {
        return $this->fileManager->getImagePath($gallery->getId(), $imageName);
    }

    /**
     * @param Gallery $gallery
     * @return array
     */
    public function getPresentableImages(Gallery $gallery): array
    {
        $images = $gallery->getImages()->toArray();
        return $this->getPresentables($images);
    }

    /**
     * @param array $images
     * @return array
     */
    public function getPresentables(array $images): array
    {
        return array_map(function (Image $image) {
            return $this->getPresentable($image);
        }, $images);
    }

    /**
     * @param Image $image
     * @return PresentableImage
     */
    public function getPresentable(Image $image): PresentableImage
    {
        return $this->transformer->customTransform($image);
    }

    /**
     * @param array $images
     * @return array
     */
    public function handleUpload($images = []): array
    {
        return array_map(function (UploadedFile $image) {
            return (new GalleryImage())
                ->setGallery($this->gallery)
                ->setStoredName($this->fileManager->uploadImage($image))
                ->setOriginalName($image->getClientOriginalName());
        }, $images);
    }

    /**
     * @param int $galleryId
     * @param string $resourceName
     */
    public function removeById(int $galleryId, string $resourceName): void
    {
        $this->contentProvider->setCriteriaArray(['gallery' => $galleryId, 'storedName' => $resourceName]);
        $entityArray = $this->contentProvider->getAll();
        if (!$entityArray || count($entityArray) < 1) return;
        $this->contentProvider->removeByEntity($entityArray[0]);
        $this->fileManager->removeImageFromGallery($galleryId, $resourceName);
    }

    /**
     * @param Gallery $gallery
     * @param GalleryImage $galleryImage
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function attachToGallery(Gallery $gallery, GalleryImage $galleryImage): void
    {
        $galleryImage->setGallery($gallery);
        $this->contentProvider->saveByEntity($galleryImage);
    }

}