<?php

namespace App\Manager;


use Ateszworks\MultilangContentBundle\Manager\FileManager;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class MPCSFileManager
 * @package App\Manager
 */
class MPCSFileManager extends FileManager
{
    /* @var string */
    protected $og_image;

    /* @var string */
    protected $twitter_image;

    /* @var string */
    protected $tempDir;

    /* @var string */
    protected $kernelProjectDir;

    /**
     * MPCSFileManager constructor.
     * @param string $workingDirectory
     * @param bool $processImageOnUpload
     */
    public function __construct(string $workingDirectory, bool $processImageOnUpload)
    {
        parent::__construct($workingDirectory, $processImageOnUpload);
        $this->tempDir = $workingDirectory . '/temp';
        $this->kernelProjectDir = $workingDirectory; // note: have to do this, look at services.yaml
    }

    /**
     * @param String $fileName
     * @param bool $fromBundle
     * @return String
     */
    public function getResourceUrl(String $fileName, bool $fromBundle = false): String
    {
        if ($fromBundle) return $this->getFullFileURL($fileName);
        $filePath = $this->kernelProjectDir . '/../../images/' . $fileName;
        if (!$this->fileSystem->exists($filePath)) throw new NotFoundHttpException($filePath);

        return $filePath;
    }

    /**
     * @param string $fileName
     * @return File
     */
    public function serveStaticFile(string $fileName): File
    {
        return new File($this->kernelProjectDir . '/../../static-files/' . $fileName, true);
    }

    /**
     * @param string $fileName
     * @return bool
     */
    public function exists(string $fileName): bool
    {
        return $this->fileSystem->exists($this->workingDirectory . '/' . $fileName);
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function uploadTempFile(UploadedFile $file)
    {
        $this->checkTempDir();
        $extension = $file->guessExtension() ?? '.bin';
        $uniqueName = $this->generateUniqueName();
        $fileName = $uniqueName . '.' . $extension;
        $file->move($this->tempDir, $fileName);

        return $fileName;
    }

    /**
     * @param string $fileName
     * @return string
     */
    public function getTempFileUrl(string $fileName): string
    {
        $path = $this->tempDir . '/' . $fileName;
        if (!$this->fileSystem->exists($path)) throw new NotFoundHttpException($path);

        return $path;
    }

    /**
     * @param string $fileName
     * @return void
     */
    public function removeFromTemp(string $fileName): void
    {
        $path = $this->tempDir . '/' . $fileName;
        $this->fileSystem->remove($path);
    }

    /**
     * Checks if temp dir exists. If not, creates.
     */
    private function checkTempDir(): void
    {
        if ($this->fileSystem->exists($this->tempDir)) return;
        $this->fileSystem->mkdir($this->tempDir);
    }
}