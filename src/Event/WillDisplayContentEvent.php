<?php

namespace App\Event;


use App\MetaReadable\MetaReadableContent;
use Symfony\Component\EventDispatcher\Event;

class WillDisplayContentEvent extends Event
{

    CONST NAME = 'content.will.display';

    /* @var MetaReadableContent */
    protected $content;

    /**
     * WillDisplayContentEvent constructor.
     * @param MetaReadableContent $content
     */
    public function __construct(MetaReadableContent $content)
    {
        $this->content = $content;
    }

    /**
     * @return MetaReadableContent
     */
    public function getContent(): MetaReadableContent
    {
        return $this->content;
    }

    /**
     * @param MetaReadableContent $content
     */
    public function setContent(MetaReadableContent $content): void
    {
        $this->content = $content;
    }

}