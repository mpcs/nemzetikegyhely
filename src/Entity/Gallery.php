<?php

namespace App\Entity;

use Ateszworks\MultilangContentBundle\ContentInterface\PointedTypeResource;
use Ateszworks\MultilangContentBundle\Entity\TranslationFilterableEntity;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Gallery
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\GalleryRepository")
 * @ORM\Table(name="galleries")
 * @ORM\HasLifecycleCallbacks()
 */
class Gallery extends TranslationFilterableEntity implements PointedTypeResource
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="GalleryTranslation", mappedBy="gallery", cascade={"persist", "remove"})
     */
    protected $translations;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\GalleryImage", mappedBy="gallery", cascade={"persist", "remove"})
     */
    protected $images;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $published;

    /**
     * @var string
     * @param \DateTime $created
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Gallery
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getTranslations(): ?Collection
    {
        return $this->translations;
    }

    /**
     * @param Collection $translations
     * @return Gallery
     */
    public function setTranslations(Collection $translations): Gallery
    {
        $this->translations = $translations;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     * @return Gallery
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getImages(): ?Collection
    {
        return $this->images;
    }

    /**
     * @param Collection $images
     * @return Gallery
     */
    public function setImages(Collection $images): Gallery
    {
        $this->images = $images;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPublished(): ?\DateTime
    {
        return $this->published;
    }

    /**
     * @param \DateTime $published
     * @return Gallery
     */
    public function setPublished(\DateTime $published): Gallery
    {
        $this->published = $published;
        return $this;
    }

    // MARK: - Lifecycle

    /**
     * @ORM\PrePersist
     */
    public function onPrePersistSetCreated()
    {
        $this->created = new \DateTime();
        $this->published = $this->published ?? new \DateTime();
    }

}