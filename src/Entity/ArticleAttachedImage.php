<?php

namespace App\Entity;

use Ateszworks\MultilangContentBundle\Entity\ArticleEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class CarouselImage
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\ArticleAttachedImageRepository")
 * @ORM\Table(name="article_images")
 */
class ArticleAttachedImage implements Image
{

    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $image = null;

    /**
     * @var ArticleEntity
     * @ORM\ManyToOne(targetEntity="Ateszworks\MultilangContentBundle\Entity\ArticleEntity")
     */
    private $article;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ArticleAttachedImage
     */
    public function setId(int $id): ArticleAttachedImage
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return ArticleAttachedImage
     */
    public function setImage(string $image): ArticleAttachedImage
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return ArticleEntity
     */
    public function getArticle(): ArticleEntity
    {
        return $this->article;
    }

    /**
     * @param ArticleEntity $article
     * @return ArticleAttachedImage
     */
    public function setArticle(ArticleEntity $article): ArticleAttachedImage
    {
        $this->article = $article;
        return $this;
    }
}