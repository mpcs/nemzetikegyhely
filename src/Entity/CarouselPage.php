<?php

namespace App\Entity;

use Ateszworks\MultilangContentBundle\Entity\TranslationFilterableEntity;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class CarouselImage
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\CarouselPageRepository")
 * @ORM\Table(name="carousel_pages")
 * @ORM\HasLifecycleCallbacks()
 */
class CarouselPage extends TranslationFilterableEntity
{

    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $image = null;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="CarouselPageTranslation", mappedBy="page", cascade={"persist", "remove"})
     */
    protected $translations;

    /**
     * @var string
     * @param \DateTime $created
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $weight = 0;

    /**
     * @var string|null
     * Until I work out something better...
     */
    private $oldImage = null;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage(string $image): void
    {
        $this->image = $image;
    }

    /**
     * @return Collection|null
     */
    public function getTranslations(): ?Collection
    {
        return $this->translations;
    }

    /**
     * @param Collection $translations
     * @return CarouselPage
     */
    public function setTranslations(Collection $translations): CarouselPage
    {
        $this->translations = $translations;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created): void
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     */
    public function setWeight(int $weight): void
    {
        $this->weight = $weight;
    }

    // MARK: - Lifecycle

    /**
     * @ORM\PrePersist
     */
    public function onPrePersistSetCreated()
    {
        $this->created = new \DateTime();
    }

}