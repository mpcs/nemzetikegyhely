<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class GalleryImage
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\GalleryImageRepository")
 * @ORM\Table(name="gallery_images")
 * @ORM\HasLifecycleCallbacks()
 */
class GalleryImage implements Image
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $storedName;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $originalName;

    /**
     * @var Gallery
     * @ORM\ManyToOne(targetEntity="App\Entity\Gallery", inversedBy="images")
     */
    private $gallery;

    /**
     * @var string
     * @param \DateTime $created
     * @ORM\Column(type="datetime")
     */
    protected $uploaded;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return GalleryImage
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Gallery
     */
    public function getGallery(): ?Gallery
    {
        return $this->gallery;
    }

    /**
     * @param Gallery $gallery
     * @return GalleryImage
     */
    public function setGallery(?Gallery $gallery): GalleryImage
    {
        $this->gallery = $gallery;
        return $this;
    }

    /**
     * @return string
     */
    public function getStoredName(): string
    {
        return $this->storedName;
    }

    /**
     * @param string $storedName
     * @return GalleryImage
     */
    public function setStoredName(string $storedName): GalleryImage
    {
        $this->storedName = $storedName;
        return $this;
    }

    /**
     * @return string
     */
    public function getOriginalName(): string
    {
        return $this->originalName;
    }

    /**
     * @param string $originalName
     * @return GalleryImage
     */
    public function setOriginalName(string $originalName): GalleryImage
    {
        $this->originalName = $originalName;
        return $this;
    }

    // MARK: - Lifecycle

    /**
     * @ORM\PrePersist
     */
    public function onPrePersistSetCreated()
    {
        $this->uploaded = new \DateTime();
    }

    // Image

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->getStoredName();
    }
}