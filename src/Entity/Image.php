<?php

namespace App\Entity;


/**
 * Interface Image
 * @package App\Entity
 */
interface Image
{

    /**
     * @return string
     */
    public function getImage(): string;
}