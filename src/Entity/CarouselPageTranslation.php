<?php

namespace App\Entity;

use Ateszworks\MultilangContentBundle\ContentInterface\ContentTranslation;
use Ateszworks\MultilangContentBundle\Entity\LangEntity;
use Ateszworks\MultilangContentBundle\Entity\TranslationFilterableEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class CarouselPageTranslation
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\CarouselPageTranslationRepository")
 * @ORM\Table(name="carousel_page_translations")
 */
class CarouselPageTranslation implements ContentTranslation
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @var LangEntity
     * @ORM\ManyToOne(targetEntity="Ateszworks\MultilangContentBundle\Entity\LangEntity")
     */
    protected $lang;

    /**
     * @var CarouselPage
     * @ORM\ManyToOne(targetEntity="CarouselPage", inversedBy="translations")
     */
    private $page;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return CarouselPageTranslation
     */
    public function setContent(string $content): CarouselPageTranslation
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return LangEntity
     */
    public function getLang(): LangEntity
    {
        return $this->lang;
    }

    /**
     * @param LangEntity $lang
     * @return CarouselPageTranslation
     */
    public function setLang(LangEntity $lang): CarouselPageTranslation
    {
        $this->lang = $lang;
        return $this;
    }

    /**
     * @return CarouselPage
     */
    public function getPage(): CarouselPage
    {
        return $this->page;
    }

    /**
     * @param CarouselPage $page
     */
    public function setPage(CarouselPage $page): void
    {
        $this->page = $page;
    }


}