<?php

namespace App\Entity;

use Ateszworks\MultilangContentBundle\ContentInterface\PointedTypeTranslationInterface;
use Doctrine\ORM\Mapping as ORM;
use Ateszworks\MultilangContentBundle\ContentInterface\ContentTranslation;
use Ateszworks\MultilangContentBundle\Entity\LangEntity;
use Ateszworks\MultilangContentBundle\Entity\TranslationFilterableEntity;

/**
 * Class GalleryTranslation
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\GalleryTranslationRepository")
 * @ORM\Table(name="gallery_translations")
 */
class GalleryTranslation implements ContentTranslation, PointedTypeTranslationInterface
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var LangEntity
     * @ORM\ManyToOne(targetEntity="Ateszworks\MultilangContentBundle\Entity\LangEntity")
     */
    protected $lang;

    /**
     * @var string
     * @ORM\Column(name="url_slug", type="string")
     */
    private $urlSlug;

    /**
     * @var string
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    /**
     * @var Gallery
     * @ORM\ManyToOne(targetEntity="Gallery", inversedBy="translations")
     */
    private $gallery;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return GalleryTranslation
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return GalleryTranslation
     */
    public function setTitle(string $title): GalleryTranslation
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return LangEntity
     */
    public function getLang(): LangEntity
    {
        return $this->lang;
    }

    /**
     * @param LangEntity $lang
     * @return GalleryTranslation
     */
    public function setLang(LangEntity $lang)
    {
        $this->lang = $lang;
        return $this;
    }

    /**
     * @return Gallery
     */
    public function getGallery(): ?Gallery
    {
        return $this->gallery;
    }

    /**
     * @param Gallery $gallery
     * @return GalleryTranslation
     */
    public function setGallery(Gallery $gallery): GalleryTranslation
    {
        $this->gallery = $gallery;
        return $this;
    }

    // ContentTranslation
    public function getContent(): ?string
    {
        return $this->getTitle();
    }

    /**
     * @return string
     */
    public function getUrlSlug(): string
    {
        return $this->urlSlug;
    }

    /**
     * @param string $urlSlug
     * @return GalleryTranslation
     */
    public function setUrlSlug(string $urlSlug): GalleryTranslation
    {
        $this->urlSlug = $urlSlug;
        return $this;
    }

    /**
     * @return string
     * Returns the name of the property the query should look for.
     */
    public static function getFilterableFieldName(): string
    {
        return 'urlSlug';
    }

    /**
     * @return string
     * Returns content of the field that the query was triggered for.
     */
    public function getFilterableFieldContents(): string
    {
        return $this->getTitle();
    }

    /**
     * @return TranslationFilterableEntity The resource, of which this is the translation of.
     */
    public function getTranslationOf(): TranslationFilterableEntity
    {
        return $this->getGallery();
    }

}