<?php

namespace App\DependencyInjection;


use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;

class AppExtension extends Extension
{

    /**
     * Loads a specific configuration.
     *
     * @param array $configs
     * @param ContainerBuilder $container
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('meta.og.image', $config['meta']['og']['image']);
        $container->setParameter('meta.twitter.image', $config['meta']['twitter']['image']);
        $container->setParameter('managers.file.gallery.storage',
            $config['managers']['file']['gallery']['storage']);
    }
}