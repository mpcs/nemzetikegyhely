<?php

namespace App\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    /**
     * Generates the configuration tree builder.
     *
     * @return \Symfony\Component\Config\Definition\Builder\TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        /** @var $treeBuilder TreeBuilder */
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('app');

        $rootNode
            ->children()
                ->arrayNode('meta')
                    ->children()
                        ->arrayNode('og')
                            ->children()
                                ->scalarNode('image')->end()
                            ->end()
                        ->end()
                        ->arrayNode('twitter')
                            ->children()
                                ->scalarNode('image')->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('managers')
                    ->children()
                        ->arrayNode('file')
                            ->children()
                                ->arrayNode('gallery')
                                    ->children()
                                        ->scalarNode('storage')
                                            ->info('The folder for images of the gal.')
                                            ->defaultValue('%kernel.project_dir%/public/gallery')
                                        ->end() // storage_directory
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}