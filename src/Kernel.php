<?php

namespace App;

use App\DependencyInjection\AppExtension;
use App\Manager\MPCSArticleManager;
use App\Provider\MPCSContentProvider;
use App\Transformer\MPCSArticleTransformer;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\RouteCollectionBuilder;

/**
 * Class Kernel
 * @package App
 */
class Kernel extends BaseKernel implements CompilerPassInterface
{
    use MicroKernelTrait;

    /**
     *
     */
    const CONFIG_EXTS = '.{php,xml,yaml,yml}';

//    /**
//     * Mocked this method in here, because this could not have been found. Nice, Symfony test doc writers, nice...
//     * Loud applause...
//     * @param string $string
//     */
//    public static function bootstrapEnv(string $string)
//    {
//        // NOOP.
//        // Dunno wtf to do here.
//    }

    /**
     * @return string
     */
    public function getCacheDir()
    {
        return $this->getProjectDir() . '/var/cache/' . $this->environment;
    }

    /**
     * @return string
     */
    public function getLogDir()
    {
        return $this->getProjectDir() . '/var/log';
    }

    /**
     * @return \Generator|iterable|\Symfony\Component\HttpKernel\Bundle\BundleInterface[]
     */
    public function registerBundles()
    {
        $contents = require $this->getProjectDir() . '/config/bundles.php';
        foreach ($contents as $class => $envs) {
            if (isset($envs['all']) || isset($envs[$this->environment])) {
                yield new $class();
            }
        }
    }

    /**
     * @param ContainerBuilder $container
     * @param LoaderInterface $loader
     * @throws \Exception
     */
    protected function configureContainer(ContainerBuilder $container, LoaderInterface $loader)
    {
        $container->registerExtension(new AppExtension());

        $container->setParameter('container.autowiring.strict_mode', true);
        $container->setParameter('container.dumper.inline_class_loader', true);
        $confDir = $this->getProjectDir() . '/config';
        $loader->load($confDir . '/packages/*' . self::CONFIG_EXTS, 'glob');
        if (is_dir($confDir . '/packages/' . $this->environment)) {
            $loader->load($confDir . '/packages/' . $this->environment . '/**/*' . self::CONFIG_EXTS, 'glob');
        }
        $loader->load($confDir . '/services' . self::CONFIG_EXTS, 'glob');
        $loader->load($confDir . '/services_' . $this->environment . self::CONFIG_EXTS, 'glob');
    }

    /**
     * @param RouteCollectionBuilder $routes
     * @throws \Symfony\Component\Config\Exception\FileLoaderLoadException
     */
    protected function configureRoutes(RouteCollectionBuilder $routes)
    {
        $confDir = $this->getProjectDir() . '/config';
        if (is_dir($confDir . '/routes/')) {
            $routes->import($confDir . '/routes/*' . self::CONFIG_EXTS, '/', 'glob');
        }
        if (is_dir($confDir . '/routes/' . $this->environment)) {
            $routes->import($confDir . '/routes/' . $this->environment . '/**/*' . self::CONFIG_EXTS, '/', 'glob');
        }
        $routes->import($confDir . '/routes' . self::CONFIG_EXTS, '/', 'glob');
    }

    /**
     * You can modify the container here before it is dumped to PHP code.
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        $routerDefiniton = $container->findDefinition('router');
        $fileManagerDefiniton = $container->findDefinition('mpcs.manager.file_manager');
        $loggerDefinition = $container->findDefinition('logger');

        $AWMLCBarticleManagerDefiniton = $container->findDefinition('ateszworks_multilang_content.manager.article_manager');
        $AWMLCBarticleManagerDefiniton->setClass(MPCSArticleManager::class);
        $AWMLCBarticleManagerDefiniton->addMethodCall('setRouter', [$routerDefiniton]);

        $AWMLCBArticleTransformer = $container->findDefinition('ateszworks_multilang_content.transformer.article_transformer');
        $AWMLCBArticleTransformer->setClass(MPCSArticleTransformer::class);
        $AWMLCBArticleTransformer->addMethodCall('setFileManager', [$fileManagerDefiniton]);

        $AWMLCBContentProvider = $container->findDefinition('ateszworks_multilang_content.provider.content_provider');
        $AWMLCBContentProvider->setClass(MPCSContentProvider::class);
        $AWMLCBContentProvider->addMethodCall('setLogger', [$loggerDefinition]);
    }

    /**
     * @return string
     */
    public function getProjectDir(): string
    {
        return __DIR__ . '/..';
    }
}
