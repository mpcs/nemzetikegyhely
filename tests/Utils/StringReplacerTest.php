<?php
declare(strict_types=1);

namespace App\Tests\Utils;

use App\Utils\StringReplacer;
use PHPUnit\Framework\TestCase;

class StringReplacerTest extends TestCase
{

    public function testReplacer()
    {
        $dataInjector = $this->getMockBuilder('\App\Utils\DynamicDataInjector')
            ->disableOriginalConstructor()->getMock();
        $replacer = new StringReplacer($dataInjector);
        $dict = ['sample' => 'SANPLE', 'and' => 'és'];
        $sampleText = 'Naon jo {inject:sample} text. Very {neminjec:tetszik}, {inject:and} nagyon {milyen:jo}.';
        $this->assertEquals('Naon jo SANPLE text. Very {neminjec:tetszik}, és nagyon {milyen:jo}.',
            $replacer->injectData($sampleText, $dict));
    }

}