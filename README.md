# Máriapócs Nemzeti Kegyhely #

Nemzeti Kegyhegy site of Máriapócs' webiste triumvirate.
Written in PHP, based on Symfony.

## About ##
Started as a small project, outgrown everything, became a well-coded, 
uniquely designed website monster, that has a wide range of capabilites.

The backend side of the project is multi-layered, tries to make transitioning from
 layer to layer be as transparent as possible, and principles of the layers are
 well separated. Everything has it's own purpose, and should do no more than intended.

## How do I get set up? ##

The project is very easy to setup, however, is dependent on NodeJS.

 * Pull production branch
 * npm i -g gulp-cli
 * npm i
 * gulp compress:all
 * composer install

After that, it should be running smoothly. Of course, has to be backed up by some 
web server.

## Who do I talk to? ##

* atesztoth@outlook.hu / muchcoffe@gmail.com