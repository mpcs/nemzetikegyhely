'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const uglifycss = require('gulp-uglifycss');

gulp.task('sass:cmp:admin', function () {
    return gulp
        .src(['src/Ateszworks/AdminBundle/Resources/sass/*.scss'])
        .pipe(
            sass({style: 'expanded'})
                .on('error', sass.logError)
        )
        .pipe(gulp.dest('src/Ateszworks/AdminBundle/Resources/public/assets/css'));
});

gulp.task('compress:css:admin', ['sass:cmp:admin'], function () {
    gulp.src('src/Ateszworks/AdminBundle/Resources/public/assets/css/*.css')
        .pipe(uglifycss({
            "maxLineLen": 80,
            "uglyComments": true
        }))
        .pipe(gulp.dest('src/Ateszworks/AdminBundle/Resources/public/assets/css'));
});

gulp.task('sass:cmp:production', function () {
    return gulp
        .src(['templates/sass/*.scss'])
        .pipe(
            sass({style: 'expanded'})
                .on('error', sass.logError)
        )
        .pipe(gulp.dest('public/main/css'));
});

gulp.task('compress:css:production', ['sass:cmp:production'], function () {
    gulp.src('public/main/css/*.css')
        .pipe(uglifycss({
            "maxLineLen": 80,
            "uglyComments": true
        }))
        .pipe(gulp.dest('public/main/css'));
});

gulp.task('compress:all', ['compress:css:admin', 'compress:css:production']);
